package org.mcgi.jp.model;

import java.math.BigDecimal;
import java.util.Date;

public class BalanceSheetEntry implements Comparable<BalanceSheetEntry>  {

        private Date date;
        private String description;
        private BigDecimal credit;
        private BigDecimal debit;
        private BigDecimal balance;

        public BalanceSheetEntry() {
            this.date = null;
            this.description = "";
            this.credit = new BigDecimal(0);
            this.debit = new BigDecimal(0);
            this.balance = new BigDecimal(0);
        }

        public Date getDate() {
            return date;
        }
        public void setDate(Date date) {
            this.date = date;
        }
        public String getDescription() {
            return description;
        }
        public void setDescription(String description) {
            this.description = description;
        }
        public BigDecimal getCredit() {
            return credit;
        }
        public void setCredit(BigDecimal credit) {
            this.credit = credit;
        }
        public BigDecimal getDebit() {
            return debit;
        }
        public void setDebit(BigDecimal debit) {
            this.debit = debit;
        }
        public BigDecimal getBalance() {
            return balance;
        }
        public void setBalance(BigDecimal balance) {
            this.balance = balance;
        }

        @Override
        public int compareTo(BalanceSheetEntry compareObj) {
            if (this.date == null || compareObj == null || compareObj.getDate() == null) {
                return 0;
            }

            Date paramDate = compareObj.getDate();
            /* For Ascending order*/
            return this.date.compareTo(paramDate);
        }
}