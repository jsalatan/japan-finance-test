package org.mcgi.jp.model;

import java.util.Date;
import java.util.function.Predicate;

public class CollectionPredicate {
    public static Predicate<RegularCollection> isBetweenDates(Date startDate, Date endDate) {
        return c -> c.getTransactionDate().after(startDate) && c.getTransactionDate().before(endDate);
    }
}
