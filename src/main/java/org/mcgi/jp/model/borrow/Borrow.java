package org.mcgi.jp.model.borrow;

import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.ExpenseType;
import org.mcgi.jp.model.Transaction;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mysema.query.annotations.QueryEntity;

@QueryEntity
@Document(collection = "borrows")
public class Borrow extends Transaction {

    /**
     * Auto-generated serialVersionUID
     */
    private static final long serialVersionUID = 2325142667355003904L;

    @Id
    private String id;

    private CollectionType borrowFrom;
    private ExpenseType payFor;
    private ExpenseType destExpense;
    private String requestor;
    private String approver;

    public String getApprover() {
        return approver;
    }
    public void setApprover(String approver) {
        this.approver = approver;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public ExpenseType getDestExpense() {
        return destExpense;
    }
    public void setDestExpense(ExpenseType destExpense) {
        this.destExpense = destExpense;
    }
    public String getRequestor() {
        return requestor;
    }
    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }
    public CollectionType getBorrowFrom() {
        return borrowFrom;
    }
    public void setBorrowFrom(CollectionType borrowFrom) {
        this.borrowFrom = borrowFrom;
    }
    public ExpenseType getPayFor() {
        return payFor;
    }
    public void setPayFor(ExpenseType payFor) {
        this.payFor = payFor;
    }
}