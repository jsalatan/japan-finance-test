package org.mcgi.jp.model.borrow;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BorrowRepository extends MongoRepository<Borrow, String> {
//    List<Expense> findByLocaleAccount(LocaleAccount localeAccount);
}