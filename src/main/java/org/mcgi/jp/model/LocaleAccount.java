package org.mcgi.jp.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.mcgi.jp.model.collection.CollectionAccount;
import org.mcgi.jp.model.payable.Payable;
import org.mcgi.jp.model.project.ProjectBook;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mysema.query.annotations.QueryEntity;

/* Refs
 *  http://www.baeldung.com/queries-in-spring-data-mongodb
 *  http://docs.spring.io/spring-data/data-document/docs/current/reference/html/#d0e3327
 */
@QueryEntity
@Document(collection = "locale_accounts")
public class LocaleAccount {

    @Id
    private String id;
    private String name;
    private List<ProjectBook> projectBooks = Collections.synchronizedList(new ArrayList<ProjectBook>());

    private List<CollectionAccount> accounts = Collections.synchronizedList(new ArrayList<CollectionAccount>());
    private List<Payable> payables = Collections.synchronizedList(new ArrayList<Payable>());

    private String division;
    private String district;
    private String countr;
    private String curreny;

    public LocaleAccount() {
        super();
    }
    public LocaleAccount(String id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public List<CollectionAccount> getAccounts() {
        return accounts;
    }
    public void setAccounts(List<CollectionAccount> accounts) {
        this.accounts = accounts;
    }

    @Transient
    public CollectionAccount getAccount(CollectionType collType) {
        return this.getAccount(collType.getDescription());
    }
    @Transient
    public CollectionAccount getAccount(String typeDescription) {
        CollectionAccount collAccount = null;
        for (CollectionAccount account : accounts) {
            String accountType = account.getType();
            if (typeDescription.equals(accountType)) {
                collAccount = account;
                break;
            }
        }
        return collAccount;
    }

    public List<Payable> getPayables() {
        return payables;
    }
    public void setPayables(List<Payable> payables) {
        this.payables = payables;
    }
    public List<ProjectBook> getProjectBooks() {
        return projectBooks;
    }
    public void setProjectBooks(List<ProjectBook> projectBooks) {
        this.projectBooks = projectBooks;
    }
    public ProjectBook getProjectBook(String projDescOrId) {
        ProjectBook targetBook = this.projectBooks.stream().filter(x -> x.getProjectDesc().equals(projDescOrId)).findAny()
                .orElse(null);

        if (targetBook == null) {
            targetBook = this.projectBooks.stream().filter(x -> x.getId().equals(projDescOrId)).findAny()
                    .orElse(null);
        }

        return targetBook;
    }

    public void updateExpense(RegularExpense targetExpense) {
        CollectionAccount accountToUse = this.getAccount(targetExpense.getSourceOfFunds());

        // Update locale balance only when transaction/remittance date is inputted
        if (targetExpense.getTransactionDate() != null) {
            // subtract expense from target collection account balance
            BigDecimal balance = accountToUse.getBalance();
            accountToUse.setBalance(balance.subtract(targetExpense.getAmount()));
        }
    }
    public String getDivision() {
        return division;
    }
    public void setDivision(String division) {
        this.division = division;
    }
    public String getDistrict() {
        return district;
    }
    public void setDistrict(String district) {
        this.district = district;
    }
    public String getCountr() {
        return countr;
    }
    public void setCountr(String countr) {
        this.countr = countr;
    }
    public String getCurreny() {
        return curreny;
    }
    public void setCurreny(String curreny) {
        this.curreny = curreny;
    }

}