package org.mcgi.jp.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mysema.query.annotations.QueryEntity;

@QueryEntity
@Document(collection = "collection_types")
public class CollectionType {

    public static final CollectionType LOCALE_FUND = new CollectionType("Locale Fund");
    public static final CollectionType LOCALE_EMERGENCY_FUND = new CollectionType("Locale Emergency Fund");

    @Id
    private String id;
    private String description;
    private String scope;
    private boolean isRemittable;

    public CollectionType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public boolean isRemittable() {
        return isRemittable;
    }

    public void setRemittable(boolean isRemittable) {
        this.isRemittable = isRemittable;
    }

}