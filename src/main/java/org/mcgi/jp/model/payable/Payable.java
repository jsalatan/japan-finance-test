package org.mcgi.jp.model.payable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Transient;

public class Payable implements Serializable {

    /** Auto-generated serialVersionUID. */
    private static final long serialVersionUID = 7172610638063228047L;

    private ObjectId id;
    private String description;
    private String lender;
    private Date dateBorrowed;
    private BigDecimal total;
    private BigDecimal balance;
    @Transient
    private BigDecimal paymentsTotal;
    private List<Payment> payments = Collections.synchronizedList(new ArrayList<Payment>());

    public Payable() {
        this.id = new ObjectId();
        this.balance = BigDecimal.ZERO;
        this.total = BigDecimal.ZERO;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public String getLender() {
        return lender;
    }

    public void setLender(String lender) {
        this.lender = lender;
    }

    public Date getDateBorrowed() {
        return dateBorrowed;
    }

    public void setDateBorrowed(Date dateBorrowed) {
        this.dateBorrowed = dateBorrowed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ObjectId getId() {
        return id;
    }

    public BigDecimal getPaymentsTotal() {
        return paymentsTotal;
    }

    public void setPaymentsTotal(BigDecimal paymentsTotal) {
        this.paymentsTotal = paymentsTotal;
    }

}