package org.mcgi.jp.model.payable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.bson.types.ObjectId;

public class Payment implements Serializable {

    /** Auto-generated serialVersionUID. */
    private static final long serialVersionUID = 7172610638063228047L;

    private ObjectId id;
    private Date paymentDate;
    private BigDecimal amount;
    private String receivedBy;
    private String paidBy;

    public ObjectId getId() {
        return id;
    }
    public void setId(ObjectId id) {
        this.id = id;
    }
    public Date getPaymentDate() {
        return paymentDate;
    }
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public String getReceivedBy() {
        return receivedBy;
    }
    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }
    public String getPaidBy() {
        return paidBy;
    }
    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

}