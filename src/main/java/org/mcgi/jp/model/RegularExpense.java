package org.mcgi.jp.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mcgi.jp.enumeration.ExpenseStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import com.mysema.query.annotations.QueryEntity;

@QueryEntity
public class RegularExpense extends Transaction {

    /**
     * Auto-generated serialVersionUID
     */
    private static final long serialVersionUID = 9098598866194907237L;

    @Id
    private ObjectId id;

    @DBRef
    private ExpenseType expenseType;
    private CollectionType sourceOfFunds;
    private ExpenseStatus status;
    private String remittedBy;
    private Date dueDate;

    public RegularExpense() {
        this.id = new ObjectId();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ExpenseType getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(ExpenseType expenseType) {
        this.expenseType = expenseType;
    }

    public CollectionType getSourceOfFunds() {
        return sourceOfFunds;
    }

    public void setSourceOfFunds(CollectionType sourceOfFunds) {
        this.sourceOfFunds = sourceOfFunds;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("[Expense]\n");
        sb.append("getId(): ").append(getId()).append("\n");
        sb.append("getAmount(): ").append(getAmount()).append("\n");
        sb.append("getTransactionDate(): ").append(getTransactionDate()).append("\n");
        sb.append("getExpenseType(): ").append(getExpenseType()).append("\n");
        sb.append("getCollectionType(): ").append(getSourceOfFunds()).append("\n");
        sb.append("getStatus(): ").append(getStatus()).append("\n");
        return sb.toString();
    }

    public ExpenseStatus getStatus() {
        return status;
    }

    public void setStatus(ExpenseStatus status) {
        this.status = status;
    }

    public String getRemittedBy() {
        return remittedBy;
    }

    public void setRemittedBy(String remittedBy) {
        this.remittedBy = remittedBy;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

}