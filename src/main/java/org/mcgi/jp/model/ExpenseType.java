package org.mcgi.jp.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mysema.query.annotations.QueryEntity;

@QueryEntity
@Document(collection = "expense_types")
public class ExpenseType {

    @Id
    private String id;

    private String description;
    @DBRef
    private CollectionType defaultSourceOfFund;
    private String appliesToLocale;

    public ExpenseType() {
        super();
    }
    public ExpenseType(String id, String description, CollectionType defaultSourceOfFund, String appliesToLocale) {
        super();
        this.id = id;
        this.description = description;
        this.defaultSourceOfFund = defaultSourceOfFund;
        this.appliesToLocale = appliesToLocale;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public CollectionType getDefaultSourceOfFund() {
        return defaultSourceOfFund;
    }
    public void setDefaultSourceOfFund(CollectionType defaultSourceOfFund) {
        this.defaultSourceOfFund = defaultSourceOfFund;
    }
    public String getAppliesToLocale() {
        return appliesToLocale;
    }
    public void setAppliesToLocale(String appliesToLocale) {
        this.appliesToLocale = appliesToLocale;
    }

}