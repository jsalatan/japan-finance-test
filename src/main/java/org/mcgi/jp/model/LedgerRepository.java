package org.mcgi.jp.model;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LedgerRepository extends MongoRepository<Ledger, String> {
}