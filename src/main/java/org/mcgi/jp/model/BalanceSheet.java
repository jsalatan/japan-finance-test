package org.mcgi.jp.model;

import java.util.Date;
import java.util.List;

public class BalanceSheet  {

    private String locale;
    private Date startCoverage;
    private Date endCoverage;
    private List<BalanceSheetEntry> entries;

    public BalanceSheet(String locale) {
        this.locale = locale;
    }
    public String getLocale() {
        return locale;
    }
    public void setLocale(String locale) {
        this.locale = locale;
    }
    public Date getStartCoverage() {
        return startCoverage;
    }
    public void setStartCoverage(Date startCoverage) {
        this.startCoverage = startCoverage;
    }
    public Date getEndCoverage() {
        return endCoverage;
    }
    public void setEndCoverage(Date endCoverage) {
        this.endCoverage = endCoverage;
    }
    public List<BalanceSheetEntry> getEntries() {
        return entries;
    }
    public void setEntries(List<BalanceSheetEntry> entries) {
        this.entries = entries;
    }


}