package org.mcgi.jp.model.project;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "remittances")
public class Remittance {

    @Id
    private String id;
    private String name;
    private BigDecimal amount;
    private Date remittanceDate;


}