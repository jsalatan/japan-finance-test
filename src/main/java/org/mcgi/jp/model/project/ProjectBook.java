package org.mcgi.jp.model.project;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.RegularCommitment;
import org.mcgi.jp.model.RegularExpense;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

public class ProjectBook {

    @Id
    private ObjectId id;
    private String projectDesc;
    private BigDecimal target;
    @Transient
    private String mode;

    @Transient
    private BigDecimal totalCommitment;

    @Transient
    private BigDecimal totalCollection;

    private List<RegularCollection> regularCollections = Collections.synchronizedList(new ArrayList<RegularCollection>());
    private List<RegularCommitment> regularCommitments = Collections.synchronizedList(new ArrayList<RegularCommitment>());
    private List<RegularExpense> regularExpenses = Collections.synchronizedList(new ArrayList<RegularExpense>());

    public ProjectBook() {
        this.id = new ObjectId();
        this.target = BigDecimal.ZERO;
    }

    public ObjectId getId() {
        return id;
    }
    public void setId(ObjectId id) {
        this.id = id;
    }
    public BigDecimal getTarget() {
        return target;
    }
    public void setTarget(BigDecimal target) {
        this.target = target;
    }
    public String getProjectDesc() {
        return projectDesc;
    }
    public void setProjectDesc(String projectDesc) {
        this.projectDesc = projectDesc;
    }
    public BigDecimal getTotalExpenses(boolean paidOnly) {
        return this.computeTotalExpenses(paidOnly);
    }
    public BigDecimal getTotalUnpaidExpenses() {
        return this.computeTotalUnpaidExpenses(this.regularExpenses);
    }
    public List<RegularExpense> getUnpaidExpenses() {
        return this.regularExpenses.stream().filter(x -> x.getTransactionDate() == null).collect(Collectors.toList());
    }
    
    private BigDecimal computeTotalExpenses(boolean paidOnly) {
        BigDecimal totalExpense = BigDecimal.ZERO;
        
        List<RegularExpense> targetExpenses = this.regularExpenses;
        if (paidOnly) {
            targetExpenses = this.regularExpenses.stream().filter(x -> x.getTransactionDate() != null).collect(Collectors.toList());
        }
        
        for (RegularExpense expense : targetExpenses) {
            totalExpense = totalExpense.add(expense.getAmount());
        }
        return totalExpense;
    }

    private BigDecimal computeTotalUnpaidExpenses(List<RegularExpense> expenses) {
        BigDecimal totalExpense = BigDecimal.ZERO;
        List<RegularExpense> unpaidExpenses = getUnpaidExpenses();
        for (RegularExpense expense : unpaidExpenses) {
            totalExpense = totalExpense.add(expense.getAmount());
        }
        return totalExpense;
    }
    
    public BigDecimal getTotalCommitment() {
        return this.computeTotalCommitment(this.regularCommitments);
    }
    public BigDecimal getTotalCollection() {
        return computeTotalCollection(this.regularCollections);
    }

    private BigDecimal computeTotalCollection(List<RegularCollection> collections) {
        BigDecimal totalCollection = BigDecimal.ZERO;
        for (RegularCollection collection : collections) {
            totalCollection = totalCollection.add(collection.getAmount());
        }
        return totalCollection;
    }

    private BigDecimal computeTotalCommitment(List<RegularCommitment> commitments) {
        BigDecimal totalCommitment = BigDecimal.ZERO;
        for (RegularCommitment commitment : commitments) {
            totalCommitment = totalCommitment.add(commitment.getAmount());
        }
        return totalCommitment;
    }
    public String getMode() {
        return mode;
    }
    public void setMode(String mode) {
        this.mode = mode;
    }

    public List<RegularCollection> getRegularCollections() {
        return regularCollections;
    }
    public void setRegularCollections(List<RegularCollection> collections) {
        this.regularCollections = collections;
    }
    public void addCollection(RegularCollection collection) {
        regularCollections.add(collection);
    }
    public List<RegularCommitment> getRegularCommitments() {
        return regularCommitments;
    }
    public void setRegularCommitments(List<RegularCommitment> regularCommitments) {
        this.regularCommitments = regularCommitments;
    }

    public List<RegularExpense> getRegularExpenses() {
        return regularExpenses;
    }

    public void setRegularExpenses(List<RegularExpense> regularExpenses) {
        this.regularExpenses = regularExpenses;
    }

    public void addRegularExpense(RegularExpense expense) {
        this.regularExpenses.add(expense);
    }

}