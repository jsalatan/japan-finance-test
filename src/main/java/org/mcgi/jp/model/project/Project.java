package org.mcgi.jp.model.project;

import java.math.BigDecimal;
import java.util.Date;

import org.bson.types.ObjectId;
import org.mcgi.jp.enumeration.ProjectMode;
import org.mcgi.jp.enumeration.ProjectScope;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
/**
 * This class determines what projects locales are able to support.
 * There can be projects defined at the locale level (locale-initiated),
 * and then there are district, division, national, and international projects.
 *
 * Most of these projects will be locale, national, and division.
 */
@Document(collection = "projects")
public class Project {

    @Id
    private ObjectId id;
    private String description;
    private ProjectScope scope;
    private ProjectMode mode;
    private BigDecimal targetAmt;
    private Date startDate;
    private Date endDate;

    public Project() {
        this.id = new ObjectId();
    }

    /*
     * NOTE: Collections are recorded/maintained in LocaleAccount
     * + same with commitments
     */
    //private List<Commitment> commitments;
    //private List<ProjectCollection> collections;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public BigDecimal getTargetAmt() {
        return targetAmt;
    }
    public void setTargetAmt(BigDecimal targetAmt) {
        this.targetAmt = targetAmt;
    }
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    /*
     * NOTE: Collections are recorded/maintained in LocaleAccount
     * + same with commitments
     */
    /*
    public List<Commitment> getCommitments() {
        return commitments;
    }
    public void setCommitments(List<Commitment> commitments) {
        this.commitments = commitments;
    }
    */

    public ProjectMode getMode() {
        return mode;
    }
    public void setMode(ProjectMode mode) {
        this.mode = mode;
    }
    /*
     * NOTE: Collections are recorded/maintained in LocaleAccount
     * + same with commitments
     */
    /*
    public List<ProjectCollection> getCollections() {
        return collections;
    }
    public void setCollections(List<ProjectCollection> collections) {
        this.collections = collections;
    }
    */
    public ObjectId getId() {
        return id;
    }
    public void setId(ObjectId id) {
        this.id = id;
    }

    public ProjectScope getScope() {
        return scope;
    }

    public void setScope(ProjectScope scope) {
        this.scope = scope;
    }

}