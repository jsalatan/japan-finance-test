package org.mcgi.jp.model.admin;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class JapanFinanceUser {

    @Id
    private String username;
    private String firstName;
    private String lastName;
    private String locale;
    private String password;
    private UserPreference preference;

    @DBRef
    private List<Role> roles;

    public JapanFinanceUser() {
        this.preference = new UserPreference();
    }
    public JapanFinanceUser(String username, String firstName, String lastName, String locale, String password, List<Role> roles) {
        super();
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.locale = locale;
        this.password = password;
        this.roles = roles;
        this.preference = new UserPreference();
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getLocale() {
        return locale;
    }
    public void setLocale(String locale) {
        this.locale = locale;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public List<Role> getRoles() {
        return roles;
    }
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    public UserPreference getPreference() {
        return preference;
    }
    public void setPreference(UserPreference preference) {
        this.preference = preference;
    }

}