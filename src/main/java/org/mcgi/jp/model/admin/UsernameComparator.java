package org.mcgi.jp.model.admin;

import java.util.Comparator;

public class UsernameComparator implements Comparator<JapanFinanceUser> {
    @Override
    public int compare(JapanFinanceUser x, JapanFinanceUser y) {
        if (x == null || y == null) {
            return 0;
        }
        return x.getUsername().compareTo(y.getUsername());
    }
}