package org.mcgi.jp.model.admin;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<JapanFinanceUser, String> {

    List<JapanFinanceUser> findByLocale(String localeId);
    List<JapanFinanceUser> findByLocaleOrderByUsername(String localeId);
}