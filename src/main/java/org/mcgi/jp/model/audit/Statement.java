package org.mcgi.jp.model.audit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.RegularExpense;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "statements")
@CompoundIndexes({
    @CompoundIndex(name = "localeId_month", def = "{'localeId' : 1, 'statementYm': 1}", unique = true)
})
public class Statement implements Serializable {

    /**
     * Auto-generated.
     */
    private static final long serialVersionUID = 6657398856529679545L;

    @Id
    private String id;

    @NotNull
    @Indexed
    private String localeId;

    @NotNull
    @Indexed
    private String statementYm;

    private List<RegularCollection> collections;
    private List<RegularExpense> expenses;
    private BigDecimal startingBalance;
    private BigDecimal endingBalance;
    public Statement(String localeId, String statementYm) {
        super();
        this.localeId = localeId;
        this.statementYm = statementYm;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getLocaleId() {
        return localeId;
    }
    public void setLocaleId(String localeId) {
        this.localeId = localeId;
    }
    public String getStatementYm() {
        return statementYm;
    }
    public void setStatementYm(String statementYm) {
        this.statementYm = statementYm;
    }
    public List<RegularCollection> getCollections() {
        return collections;
    }
    public void setCollections(List<RegularCollection> collections) {
        this.collections = collections;
    }
    public BigDecimal getStartingBalance() {
        return startingBalance;
    }
    public void setStartingBalance(BigDecimal startingBalance) {
        this.startingBalance = startingBalance;
    }
    public BigDecimal getEndingBalance() {
        return endingBalance;
    }
    public void setEndingBalance(BigDecimal endingBalance) {
        this.endingBalance = endingBalance;
    }
    public List<RegularExpense> getExpenses() {
        return expenses;
    }
    public void setExpenses(List<RegularExpense> expenses) {
        this.expenses = expenses;
    }

}