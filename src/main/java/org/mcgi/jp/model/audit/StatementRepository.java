package org.mcgi.jp.model.audit;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatementRepository extends MongoRepository<Statement, String> {
    Statement findByLocaleIdAndStatementYm(String localeId, String statementYm);
    List<Statement> findByLocaleId(String localeId);
}