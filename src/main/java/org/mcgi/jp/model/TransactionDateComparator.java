package org.mcgi.jp.model;

import java.util.Comparator;

public class TransactionDateComparator implements Comparator<Transaction> {
    @Override
    public int compare(Transaction x, Transaction y) {
        if (x == null || y == null
            || x.getTransactionDate() == null || y.getTransactionDate() == null) {
            return 0;
        }
        return x.getTransactionDate().compareTo(y.getTransactionDate());
    }
}