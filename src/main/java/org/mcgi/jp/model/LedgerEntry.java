package org.mcgi.jp.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ledger_entries")
public class LedgerEntry implements Serializable, Comparable<LedgerEntry> {

    private static final long serialVersionUID = 7204960868038869426L;

    @Id
    private String id;

    private String debitOrCredit;
    private Date transactionDate;
    private BigDecimal amount;
    private BigDecimal balance;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDebitOrCredit() {
		return debitOrCredit;
	}
	public void setDebitOrCredit(String debitOrCredit) {
		this.debitOrCredit = debitOrCredit;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	@Override
	public int compareTo(LedgerEntry compareObj) {
		Date transactionDate = compareObj.getTransactionDate();
        /* For Ascending order*/
        return this.transactionDate.compareTo(transactionDate);
    }
    
}