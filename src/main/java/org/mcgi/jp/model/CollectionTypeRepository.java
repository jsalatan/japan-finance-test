package org.mcgi.jp.model;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CollectionTypeRepository extends MongoRepository<CollectionType, String> {
    CollectionType findByDescription(String string);
}