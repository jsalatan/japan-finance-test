package org.mcgi.jp.model.report;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "collection_reports")
public class CollectionReport {

    @Id
    private String id;
    private Date dateGenerated;
    private String reporter;
    private List<CollectionRecord> records;

    public Date getDateGenerated() {
        return dateGenerated;
    }
    public void setDateGenerated(Date dateGenerated) {
        this.dateGenerated = dateGenerated;
    }
    public String getReporter() {
        return reporter;
    }
    public void setReporter(String reporter) {
        this.reporter = reporter;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public List<CollectionRecord> getRecords() {
        return records;
    }
    public void setRecords(List<CollectionRecord> records) {
        this.records = records;
    }

    @Transient
    private String nameOfLocal;
    @Transient
    private String collectionType;
    @Transient
    private Date collectionDate;
    @Transient
    private String baseCurrency;
    @Transient
    private BigDecimal amount;
    @Transient
    private BigDecimal exchangeRate;
    @Transient
    private String toCurrency;

    public String getCollectionType() {
        return collectionType;
    }
    public void setCollectionType(String collectionType) {
        this.collectionType = collectionType;
    }
    public Date getCollectionDate() {
        return collectionDate;
    }
    public void setCollectionDate(Date collectionDate) {
        this.collectionDate = collectionDate;
    }
    public String getBaseCurrency() {
        return baseCurrency;
    }
    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }
    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
    public String getToCurrency() {
        return toCurrency;
    }
    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }
    public String getNameOfLocal() {
        return nameOfLocal;
    }
    public void setNameOfLocal(String nameOfLocal) {
        this.nameOfLocal = nameOfLocal;
    }

}