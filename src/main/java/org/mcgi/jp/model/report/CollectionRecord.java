package org.mcgi.jp.model.report;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@CompoundIndex(name = "collection_record_idx",
                def = "{'locale' : 1, 'collectionType' : 1, 'collectionDate' : 1, 'baseCurrency' : 1}")
@Document(collection = "collection_records")
public class CollectionRecord {

    private String locale;
    private String collectionType;
    private Date collectionDate;
    private String baseCurrency;
    private BigDecimal amount;

    private BigDecimal exchangeRate;
    private String toCurrency;
    public String getLocale() {
        return locale;
    }
    public void setLocale(String locale) {
        this.locale = locale;
    }
    public String getCollectionType() {
        return collectionType;
    }
    public void setCollectionType(String collectionType) {
        this.collectionType = collectionType;
    }
    public Date getCollectionDate() {
        return collectionDate;
    }
    public void setCollectionDate(Date collectionDate) {
        this.collectionDate = collectionDate;
    }
    public String getBaseCurrency() {
        return baseCurrency;
    }
    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }
    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
    public String getToCurrency() {
        return toCurrency;
    }
    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

}
