package org.mcgi.jp.model.report;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LiquidationReportRepository extends MongoRepository<LiquidationReport, String> {
    LiquidationReport findByLocaleAndYearMonth(String localeName, String yearMonth);
    List<LiquidationReport> findByLocale(String localeName);
}