package org.mcgi.jp.model.report;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mysema.query.annotations.QueryEntity;

@QueryEntity
@Document(collection = "liquidation_reports")
public class LiquidationReport {

    @Id
    private String id;
    private String division;
    private String district;
    private String locale;

    private String yearMonth;
    private BigDecimal beginningCashOnHand;
    private BigDecimal currentCollection;
    private List<Map<String, BigDecimal>> localeExpenses;

    @Transient
    private BigDecimal totalCollection;

    @Transient
    private BigDecimal totalExpenses;

    @Transient
    private BigDecimal totalCashRemaining;


    public LiquidationReport(String division, String district, String locale, String yearMonth) {
        super();
        this.division = division;
        this.district = district;
        this.locale = locale;
        this.yearMonth = yearMonth;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getDivision() {
        return division;
    }


    public void setDivision(String division) {
        this.division = division;
    }


    public String getDistrict() {
        return district;
    }


    public void setDistrict(String district) {
        this.district = district;
    }


    public String getLocale() {
        return locale;
    }


    public void setLocale(String locale) {
        this.locale = locale;
    }


    public String getYearMonth() {
        return yearMonth;
    }


    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }


    public BigDecimal getBeginningCashOnHand() {
        return beginningCashOnHand;
    }


    public void setBeginningCashOnHand(BigDecimal beginningCashOnHand) {
        this.beginningCashOnHand = beginningCashOnHand;
    }


    public BigDecimal getCurrentCollection() {
        return currentCollection;
    }


    public void setCurrentCollection(BigDecimal currentCollection) {
        this.currentCollection = currentCollection;
    }


    public List<Map<String, BigDecimal>> getLocaleExpenses() {
        return localeExpenses;
    }


    public void setLocaleExpenses(List<Map<String, BigDecimal>> localeExpenses) {
        this.localeExpenses = localeExpenses;
    }


    public BigDecimal getTotalCollection() {
        return totalCollection;
    }


    public void setTotalCollection(BigDecimal totalCollection) {
        this.totalCollection = totalCollection;
    }


    public BigDecimal getTotalExpenses() {
        return totalExpenses;
    }


    public void setTotalExpenses(BigDecimal totalExpenses) {
        this.totalExpenses = totalExpenses;
    }


    public BigDecimal getTotalCashRemaining() {
        return totalCashRemaining;
    }


    public void setTotalCashRemaining(BigDecimal totalCashRemaining) {
        this.totalCashRemaining = totalCashRemaining;
    }



}