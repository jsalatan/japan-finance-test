package org.mcgi.jp.model.report;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CollectionReportRepository extends MongoRepository<CollectionReport, String> {
}