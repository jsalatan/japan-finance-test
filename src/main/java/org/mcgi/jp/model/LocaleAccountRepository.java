package org.mcgi.jp.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LocaleAccountRepository extends MongoRepository<LocaleAccount, String> {

    LocaleAccount findByName(String name);

    // ref: http://stackoverflow.com/questions/12730370/spring-data-mongodb-findby-method-for-nested-objects
    @Query(value = "{ 'collections.transactionDate' : { '$gte' : ?1, '$lte' : ?2 }, '_id' : ?0 }", fields = "{ 'collections' : 1}")
    List<LocaleAccount> findByCollectionDateBetween(String localeId, Date startDate, Date endDate);
}