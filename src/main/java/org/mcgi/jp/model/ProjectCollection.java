package org.mcgi.jp.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.bson.types.ObjectId;

public class ProjectCollection extends Transaction implements Serializable {

    private static final long serialVersionUID = 5838347437268836464L;

    private ObjectId id;
    private String givenBy;

    public ProjectCollection() {
        super();
    }

    public ProjectCollection(ObjectId id, String givenBy, BigDecimal amount, Date collectionDate) {
        super();
        this.id = id;
        this.givenBy = givenBy;
        super.setAmount(amount);
        super.setTransactionDate(collectionDate);
    }
    public void setId(ObjectId id) {
        this.id = id;
    }
    public ObjectId getId() {
        return id;
    }

    public String getGivenBy() {
        return givenBy;
    }

    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }

}