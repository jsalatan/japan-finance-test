package org.mcgi.jp.model;

import java.math.BigDecimal;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Commitment {

    @Id
    private ObjectId id;

    private String committedBy;
    private BigDecimal amount;
    private Date commitmentDate;
    private String commitmentFor;

    public ObjectId getId() {
        return id;
    }
    public void setId(ObjectId id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public Date getCommitmentDate() {
        return commitmentDate;
    }
    public void setCommitmentDate(Date commitmentDate) {
        this.commitmentDate = commitmentDate;
    }
    public String getCommittedBy() {
        return committedBy;
    }
    public void setCommittedBy(String committedBy) {
        this.committedBy = committedBy;
    }
    public String getCommitmentFor() {
        return commitmentFor;
    }
    public void setCommitmentFor(String commitmentFor) {
        this.commitmentFor = commitmentFor;
    }

}