package org.mcgi.jp.model;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpenseTypeRepository extends MongoRepository<ExpenseType, String> {
    ExpenseType findByDescription(String description);
    List<ExpenseType> findByAppliesToLocale(String localeId);
}