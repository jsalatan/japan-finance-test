package org.mcgi.jp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ledger")
public class Ledger implements Serializable {

    private static final long serialVersionUID = 7204960868038869426L;

    @Id
    private String id;

    @DBRef
    private List<LedgerEntry> entries = new ArrayList<LedgerEntry>();

    public Ledger() {
        super();
    }

    public Ledger(List<LedgerEntry> entries) {
        super();
        this.setEntries(entries);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

	public List<LedgerEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<LedgerEntry> entries) {
		this.entries = entries;
	}

}