package org.mcgi.jp.model.bpm;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessProcessRepository extends MongoRepository<BusinessProcess, String> {
}