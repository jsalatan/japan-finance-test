package org.mcgi.jp.model.bpm;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "business_processes")
public class BusinessProcess implements Serializable {

    /** auto-generated serialVersionUID */
    private static final long serialVersionUID = 4012250061808169526L;

    @Id
    private String id;
    private String name;
    private String controlUrl;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getControlUrl() {
        return controlUrl;
    }
    public void setControlUrl(String controlUrl) {
        this.controlUrl = controlUrl;
    }

}