package org.mcgi.jp.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AccountingLedger {

    private LocaleAccount localeAccount;
    private List<LedgerEntry> entries = Collections.synchronizedList(new ArrayList<LedgerEntry>());

    public AccountingLedger(LocaleAccount localeAccount) {
        super();
        this.localeAccount = localeAccount;
    }

    public void generateLedger() {
        List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
        for (RegularCollection collection : collections) {
            LedgerEntry entry = new LedgerEntry();
            entry.setDebitOrCredit("C");
            entry.setAmount(collection.getAmount());
            entry.setTransactionDate(collection.getTransactionDate());
            getEntries().add(entry);
        }

        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
        for (RegularExpense expense : expenses) {
            LedgerEntry entry = new LedgerEntry();
            entry.setDebitOrCredit("D");
            entry.setAmount(expense.getAmount());
            entry.setTransactionDate(expense.getTransactionDate());
            getEntries().add(entry);
        }
        Collections.sort(getEntries());

        BigDecimal sum = new BigDecimal(0);
        for (int i = 0; i < getEntries().size(); i++) {
            LedgerEntry ledgerEntry = getEntries().get(i);
            String debitOrCredit = ledgerEntry.getDebitOrCredit();

            BigDecimal amount = ledgerEntry.getAmount();
            if (debitOrCredit.equals("C")) {
                sum = sum.add(amount);
            } else {
                sum = sum.subtract(amount);
            }

            ledgerEntry.setBalance(sum);
        }
    }

    public List<LedgerEntry> getEntries() {
        return entries;
    }

}