package org.mcgi.jp.model;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

public class RegularCollection extends Transaction implements Serializable {

    private static final long serialVersionUID = 5838347437268836464L;

    @Id
    private ObjectId id;

    @DBRef
    private CollectionType type;
    private String givenBy;

    public RegularCollection() {
        super();
        this.id = new ObjectId();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }
    public ObjectId getId() {
        return id;
    }
    public CollectionType getType() {
        return type;
    }
    public void setType(CollectionType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("[Collection]\n");
        sb.append("getId(): ").append(getId()).append("\n");
        sb.append("getAmount(): ").append(getAmount()).append("\n");
        sb.append("getTransactionDate(): ").append(getTransactionDate()).append("\n");
        sb.append("getType(): ").append(getType()).append("\n");
        sb.append("givenBy(): ").append(getGivenBy()).append("\n");
        return sb.toString();
    }

    public String getGivenBy() {
        return givenBy;
    }

    public void setGivenBy(String givenBy) {
        this.givenBy = givenBy;
    }

}