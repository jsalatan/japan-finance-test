package org.mcgi.jp.model.collection;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mysema.query.annotations.QueryEntity;

@QueryEntity
@Document(collection = "collection_accounts")
public class CollectionAccount implements Serializable {

    private static final long serialVersionUID = -8380219647586939480L;

    @Id
    private String type;
    private BigDecimal balance;

    public CollectionAccount(String type) {
        this.type = type;
        this.balance = new BigDecimal(0);
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public BigDecimal getBalance() {
        return balance;
    }
    // TODO: Remove setter for balance
    /*
     * No setter for balance; balance can only be set by adding collections or subtracting expenses (transactions);
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

}