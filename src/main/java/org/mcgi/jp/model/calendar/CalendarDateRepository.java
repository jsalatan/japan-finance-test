package org.mcgi.jp.model.calendar;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalendarDateRepository extends MongoRepository<CalendarDate, String> {
}