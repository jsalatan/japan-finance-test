package org.mcgi.jp.model.calendar;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "calendar_dates")
public class CalendarDate implements Serializable {

    /** auto-generated serialVersionUID */
    private static final long serialVersionUID = 1973828179333464488L;

    @Id
    private String id;
    @Indexed(unique=true)
    private Date theDate;
    /** Audit Day (Y/N) */
    private String auditDay;
    private Integer systemDayNo;
    @Transient
    private Boolean checked;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Date getTheDate() {
        return theDate;
    }
    public void setTheDate(Date theDate) {
        this.theDate = theDate;
    }
    public String getAuditDay() {
        return auditDay;
    }
    public void setAuditDay(String auditDay) {
        this.auditDay = auditDay;
    }
    public Integer getSystemDayNo() {
        return systemDayNo;
    }
    public void setSystemDayNo(Integer systemDayNo) {
        this.systemDayNo = systemDayNo;
    }
    public Boolean getChecked() {
        return checked;
    }
    public void setChecked(Boolean checked) {
        this.checked = checked;
    }


}