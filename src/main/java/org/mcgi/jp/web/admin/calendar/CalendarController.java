package org.mcgi.jp.web.admin.calendar;

import java.util.ArrayList;
import java.util.List;

import org.mcgi.jp.model.calendar.CalendarDate;
import org.mcgi.jp.model.calendar.CalendarDateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CalendarController {

    private final Logger logger = LoggerFactory.getLogger(CalendarController.class);

    @Autowired
    private CalendarDateRepository calendarRepo;

    @RequestMapping("/admin/calendar")
    public String index(ModelMap model) {

        List<CalendarDate> dates = calendarRepo.findAll();
        if (dates == null || dates.isEmpty()) {
            dates = new ArrayList<CalendarDate>();
            int i = 0;
            while (i < 1) {
                dates.add(new CalendarDate());
                i++;
            }
        }
        model.put("dateEntries", dates);
        CalendarForm form = new CalendarForm();
        form.setDateEntries(dates);
        model.put("calendarForm", form);

        return "admin/calendar/index.html";
    }

    @RequestMapping(value="/admin/calendar", method=RequestMethod.POST)
    public String submit(@ModelAttribute("calendarForm") CalendarForm calendarForm, ModelMap model) {
        calendarRepo.save(calendarForm.getDateEntries());
        return "redirect:/admin/calendar";
    }
}