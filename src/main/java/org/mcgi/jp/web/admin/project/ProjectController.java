package org.mcgi.jp.web.admin.project;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.enumeration.ProjectMode;
import org.mcgi.jp.enumeration.ProjectScope;
import org.mcgi.jp.model.project.Project;
import org.mcgi.jp.model.project.ProjectRepository;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProjectController {

    private final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    private ProjectRepository repo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = {"/project", "/project/index" }, method = RequestMethod.GET)
    public String index(Map<String, Object> model) {
        List<Project> projects = repo.findAll();
        model.put("projects", projects);

        return "project/index";
    }

    @RequestMapping(value = "/project/new", method = RequestMethod.GET)
    public String newInstance(Model model) {
        Project project = new Project();
        model.addAttribute("project", project);
        model.addAttribute("scopes", ProjectScope.values());
        model.addAttribute("modes", ProjectMode.values());

        return "project/new";

    }

    @RequestMapping(value = "/project/new", method = RequestMethod.POST)
    public String register(
            @RequestParam String description,
            @RequestParam String scope,
            @RequestParam BigDecimal targetAmt,
            @RequestParam String mode,
            @RequestParam Date startDate,
            @RequestParam Date endDate,
            ModelMap model) {

        Project project = new Project();
        project.setDescription(description);
        project.setScope(ProjectScope.valueOf(scope));
        project.setTargetAmt(targetAmt);
        project.setMode(ProjectMode.valueOf(mode));
        project.setStartDate(startDate);
        project.setEndDate(endDate);

        repo.insert(project);
        return "redirect:/project";
    }

    @RequestMapping(value = "/project/{projectId}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String projectId, Model model) {
        Project project = repo.findOne(projectId);
        model.addAttribute("project", project);

        model.addAttribute("scopes", ProjectScope.values());
        model.addAttribute("modes", ProjectMode.values());

        return "project/edit";

    }

    @RequestMapping(value = "/project/{projectId}/edit", method = RequestMethod.POST)
    public String update(
            @PathVariable String projectId,
            @RequestParam String description,
            @RequestParam String scope,
            @RequestParam BigDecimal targetAmt,
            @RequestParam String mode,
            @RequestParam Date startDate,
            @RequestParam Date endDate,
            ModelMap model) {

        Project project = repo.findOne(projectId);
        project.setDescription(description);
        project.setScope(ProjectScope.valueOf(scope));
        project.setTargetAmt(targetAmt);
        project.setMode(ProjectMode.valueOf(mode));
        project.setStartDate(startDate);
        project.setEndDate(endDate);

        repo.save(project);
        return "redirect:/project";
    }

}