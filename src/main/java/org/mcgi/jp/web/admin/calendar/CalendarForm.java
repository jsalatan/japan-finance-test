package org.mcgi.jp.web.admin.calendar;

import java.util.List;

import org.mcgi.jp.model.calendar.CalendarDate;

public class CalendarForm {

    private List<CalendarDate> dateEntries;

    public List<CalendarDate> getDateEntries() {
        return dateEntries;
    }

    public void setDateEntries(List<CalendarDate> dateEntries) {
        this.dateEntries = dateEntries;
    }


}
