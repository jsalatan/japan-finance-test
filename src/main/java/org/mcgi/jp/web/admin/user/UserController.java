package org.mcgi.jp.web.admin.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.Role;
import org.mcgi.jp.model.admin.RoleRepository;
import org.mcgi.jp.model.admin.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private LocaleAccountRepository localeRepo;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/user", method = RequestMethod.GET)
    public String index(ModelMap model) {

        List<JapanFinanceUser> users = userRepo.findAll();
        model.put("users", users);

        return "admin/user/index";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/user/new", method = RequestMethod.GET)
    public String newUser(Model model) {
        model.addAttribute("userForm", new JapanFinanceUser());

        List<Role> roles = roleRepo.findAll();
        Map<String, String> rolesMap = new HashMap<String, String>();
        for (Role role : roles) {
            rolesMap.put(role.getId(), role.getId());
        }
        Map<String, String> treeMap = new TreeMap<String, String>(rolesMap);
        model.addAttribute("roles", treeMap);

        List<LocaleAccount> locales = localeRepo.findAll();
        model.addAttribute("locales", locales);

        return "admin/user/new";
    }

    @RequestMapping(value = "/admin/user/new", method = RequestMethod.POST)
    public String create(@RequestParam String username, @RequestParam String firstName, @RequestParam String lastName,
            @RequestParam String locale, @RequestParam String password, @RequestParam(name="roles") String[] roleIds, ModelMap model) {

        List<Role> roles = new ArrayList<Role>();
        for (int i = 0 ; i < roleIds.length; i++) {
            roles.add(roleRepo.findOne(roleIds[i]));
        }

        JapanFinanceUser user = new JapanFinanceUser(username, firstName, lastName, locale, password, roles);
        userRepo.save(user);

        return "redirect:/admin/user";
    }

    @RequestMapping(value = "/admin/user/{userId}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String userId, Model model) {
        List<Role> roles = roleRepo.findAll();
        model.addAttribute("roles", roles);

        List<LocaleAccount> locales = localeRepo.findAll();
        model.addAttribute("locales", locales);

        JapanFinanceUser target = userRepo.findOne(userId);
        model.addAttribute("user", target);

        return "admin/user/edit";
    }

    @RequestMapping(value = "/admin/user/{userId}/edit", method = RequestMethod.POST)
    public String update(@RequestParam String username, @RequestParam String firstName, @RequestParam String lastName,
            @RequestParam String locale, @RequestParam String password, @RequestParam(name="roles") String[] roleIds, ModelMap model) {

        List<Role> roles = new ArrayList<Role>();
        for (int i = 0 ; i < roleIds.length; i++) {
            roles.add(roleRepo.findOne(roleIds[i]));
        }

        JapanFinanceUser user = new JapanFinanceUser(username, firstName, lastName, locale, password, roles);
        userRepo.save(user);

        return "redirect:/admin/user";
    }
}