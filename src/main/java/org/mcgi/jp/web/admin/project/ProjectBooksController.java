package org.mcgi.jp.web.admin.project;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import org.bson.types.ObjectId;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.project.Project;
import org.mcgi.jp.model.project.ProjectBook;
import org.mcgi.jp.model.project.ProjectRepository;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProjectBooksController {

    private final Logger logger = LoggerFactory.getLogger(ProjectBooksController.class);

    @Autowired
    private ProjectRepository projectRepo;

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/project_book", method = RequestMethod.GET)
    public String localeIndex(Map<String, Object> model) {
        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        if (localeAccounts == null || localeAccounts.isEmpty()) {
            model.put("msg", "No locales regisered.");
        }

        return "admin/project_book/locale_index";
    }

    @RequestMapping(value = "/locale_account/{localeId}/project_book", method = RequestMethod.GET)
    public String index(HttpSession session, @PathVariable String localeId, Map<String, Object> model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.put("localeAccount", localeAccount);

        List<ProjectBook> projectBooks = localeAccount.getProjectBooks();
        model.put("projectBooks", projectBooks);

        Config.set( session, Config.FMT_LOCALE, new java.util.Locale("ja","JP") );

        return "admin/project_book/index";
    }

    @RequestMapping(value = "/locale_account/{localeId}/project_book/new", method = RequestMethod.GET)
    public String newInstance(@PathVariable String localeId, Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("localeAccount", localeAccount);

        List<Project> projects = projectRepo.findAll();
        model.addAttribute("projects", projects);

        ProjectBook projectBook = new ProjectBook();
        model.addAttribute("projectBook", projectBook);
        return "admin/project_book/new";

    }

    @RequestMapping(value = "/locale_account/{localeId}/project_book/new", method = RequestMethod.POST)
    public String register(
            @PathVariable String localeId,
            @RequestParam String projectDesc,
            @RequestParam BigDecimal target,
            ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        ProjectBook newProjBook = new ProjectBook();
        newProjBook.setId(new ObjectId());
        newProjBook.setProjectDesc(projectDesc);
        newProjBook.setTarget(target);

        localeAccount.getProjectBooks().add(newProjBook);
        localeAccountRepo.save(localeAccount);

        return "redirect:/locale_account/" + localeId + "/project_book";
    }

    @RequestMapping(value = "/locale_account/{localeId}/project_book/{projectBookId}/edit", method = RequestMethod.GET)
    public String edit(
            @PathVariable String localeId,
            @PathVariable String projectBookId,
            Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("localeAccount", localeAccount);

        ProjectBook projectBook = localeAccount.getProjectBook(projectBookId);
        model.addAttribute("projectBook", projectBook);

        return "admin/project_book/edit";

    }

    @RequestMapping(value = "/locale_account/{localeId}/project_book/{projectBookId}/edit", method = RequestMethod.POST)
    public String update(
            @PathVariable String localeId,
            @PathVariable String projectBookId,
            @RequestParam String projectDesc,
            @RequestParam BigDecimal target,
            ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        ProjectBook projectBook = localeAccount.getProjectBook(projectBookId);
        projectBook.setProjectDesc(projectDesc);
        projectBook.setTarget(target);

        localeAccountRepo.save(localeAccount);

        return "redirect:/locale_account/" + localeId + "/project_book";
    }

}