package org.mcgi.jp.web.admin.bpm;

import java.util.List;

import org.mcgi.jp.model.bpm.BusinessProcess;
import org.mcgi.jp.model.bpm.BusinessProcessRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProcessManagementController {

    private final Logger logger = LoggerFactory.getLogger(ProcessManagementController.class);

    @Autowired
    private BusinessProcessRepository repo;

    @RequestMapping("/admin/bpm")
    public String index(ModelMap model) {
        List<BusinessProcess> processes = repo.findAll();
        model.addAttribute("processes", processes);
        return "admin/bpm/index";
    }

    @RequestMapping(value = "/admin/bpm/new", method = RequestMethod.GET)
    public String newModel(Model model) {
        BusinessProcess process = new BusinessProcess();
        model.addAttribute("formObj", process);
        return "admin/bpm/new";
    }

    @RequestMapping(value = "/admin/bpm/new", method = RequestMethod.POST)
    public String create(@ModelAttribute("formObj") BusinessProcess formObj, Model model) {
        repo.save(formObj);
        return "redirect:/admin/bpm";
    }
}