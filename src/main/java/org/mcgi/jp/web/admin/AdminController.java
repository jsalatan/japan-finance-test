package org.mcgi.jp.web.admin;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AdminController {

    private final Logger logger = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String index(Model model) {

        LocaleAccount locale = null;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
            String localeStr = customUser.getLocale();

            if (localeStr != null) {
                locale = localeAccountRepo.findByName(localeStr);

                if (locale == null) {
                    locale = localeAccountRepo.findOne(localeStr);
                }
            }

        }
        model.addAttribute("localeAccount", locale);

        return "admin/index";
    }

}