package org.mcgi.jp.web.locale;

import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LocaleAccountController {

    private final Logger logger = LoggerFactory.getLogger(LocaleAccountController.class);

    @Autowired
    private LocaleAccountRepository repo;

    @RequestMapping(value = "/locale_account", method = RequestMethod.GET)
    public String index(Map<String, Object> model) {
        List<LocaleAccount> accounts = repo.findAll();
        model.put("accounts", accounts);

        return "locale_account/index";
    }

    @RequestMapping(value = "/locale_account/new", method = RequestMethod.GET)
    public String newExpense(Model model) {

        LocaleAccount account = new LocaleAccount();
        model.addAttribute("account", account);

        return "locale_account/new";

    }

    @RequestMapping(value = "/locale_account/new", method = RequestMethod.POST)
    public String registerExpense(@ModelAttribute("SpringWeb") LocaleAccount account, ModelMap model) {
        repo.insert(account);
        return "redirect:/locale_account";
    }

}