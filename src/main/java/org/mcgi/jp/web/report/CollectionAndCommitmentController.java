package org.mcgi.jp.web.report;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.project.ProjectBook;
import org.mcgi.jp.service.CollectionAndCommitmentService;
import org.mcgi.jp.service.FinanceUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CollectionAndCommitmentController {

    private final Logger logger = LoggerFactory.getLogger(CollectionAndCommitmentController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private FinanceUserService userSvc;

    @Autowired
    private CollectionAndCommitmentService service;

    @RequestMapping(value = "/reports/collection_and_commitment", method = RequestMethod.GET)
    public String localeIndex(Map<String, Object> model) {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        boolean hasLocale = userSvc.hasLocale();

        String page = "";
        if (hasLocale) {
            LocaleAccount locale = userSvc.getLocale();
            page = "redirect:/reports/collection_and_commitment/" + locale.getId();
        } else {
            page = "reports/collection_and_commitment/locale_index";
        }

        return page;
    }

    @RequestMapping(value = "/reports/collection_and_commitment/{localeId}", method = RequestMethod.GET)
    public String projectIndex(@PathVariable String localeId, Model model) {

        /*
         * The displayed page will differ depending on user's role:
         *   1) If GS or LC, display weekly table with member's list
         *   2) If National, display weekly table with list of locales
         */

        /*
         * Basic flow:
         * 1) Get weekend labels (for columns)
         * 2) Get the list of locale members (for rows)
         * 3) Output data per user, per week (table details)
         */
        // Get weekend labels for columns
        List<Date> weekEnds = service.getWeekendDatesofCurrentMonth();
        model.addAttribute("weekEnds", weekEnds);

        // Get report rows (member list and collection details)
        List<MonthlyReportRecord> records = service.getMonthlyReport(localeId);
        model.addAttribute("reportRows", records);

        return "reports/collection_and_commitment/monthly";
    }

    @RequestMapping(value = "/reports/collection_and_commitment/{localeId}/{projectBookId}", method = RequestMethod.GET)
    public String main(@PathVariable String localeId, @PathVariable String projectBookId, Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        List<ProjectBook> projectBooks = localeAccount.getProjectBooks();
        ProjectBook targetBook = projectBooks.stream().filter(x -> x.getId().equals(projectBookId)).findAny()
                .orElse(null);

        model.addAttribute("project", targetBook);

        return "reports/collection_and_commitment/project_index";
    }

}