package org.mcgi.jp.web.report;

import java.math.BigDecimal;

public class MonthlyReportRecord {

    private String memberId;
    private BigDecimal commitment;
    private BigDecimal collectionWeek1;
    private BigDecimal collectionWeek2;
    private BigDecimal collectionWeek3;
    private BigDecimal collectionWeek4;
    private BigDecimal collectionWeek5;
    private BigDecimal totalCollection;
    public String getMemberId() {
        return memberId;
    }
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
    public BigDecimal getCommitment() {
        return commitment;
    }
    public void setCommitment(BigDecimal commitment) {
        this.commitment = commitment;
    }
    public BigDecimal getCollectionWeek1() {
        return collectionWeek1;
    }
    public void setCollectionWeek1(BigDecimal collectionWeek1) {
        this.collectionWeek1 = collectionWeek1;
    }
    public BigDecimal getCollectionWeek2() {
        return collectionWeek2;
    }
    public void setCollectionWeek2(BigDecimal collectionWeek2) {
        this.collectionWeek2 = collectionWeek2;
    }
    public BigDecimal getCollectionWeek3() {
        return collectionWeek3;
    }
    public void setCollectionWeek3(BigDecimal collectionWeek3) {
        this.collectionWeek3 = collectionWeek3;
    }
    public BigDecimal getCollectionWeek4() {
        return collectionWeek4;
    }
    public void setCollectionWeek4(BigDecimal collectionWeek4) {
        this.collectionWeek4 = collectionWeek4;
    }
    public BigDecimal getCollectionWeek5() {
        return collectionWeek5;
    }
    public void setCollectionWeek5(BigDecimal collectionWeek5) {
        this.collectionWeek5 = collectionWeek5;
    }
    public BigDecimal getTotalCollection() {
        return totalCollection;
    }
    public void setTotalCollection(BigDecimal totalCollection) {
        this.totalCollection = totalCollection;
    }

}

