package org.mcgi.jp.web.report;

import static org.apache.commons.io.FileUtils.readFileToString;

import java.io.File;
import java.io.IOException;

import org.mcgi.jp.web.report.helper.WebAppReportXmlReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haulmont.yarg.formatters.factory.DefaultFormatterFactory;
import com.haulmont.yarg.loaders.factory.DefaultLoaderFactory;
import com.haulmont.yarg.loaders.impl.GroovyDataLoader;
import com.haulmont.yarg.reporting.ReportOutputDocument;
import com.haulmont.yarg.reporting.Reporting;
import com.haulmont.yarg.reporting.RunParams;
import com.haulmont.yarg.structure.Report;
import com.haulmont.yarg.util.groovy.DefaultScriptingImpl;

@Controller
public class ReportController {

    private final Logger logger = LoggerFactory.getLogger(ReportController.class);

    @RequestMapping(value = "/reports", method = RequestMethod.GET)
    public String listExpense(Model model) {
        return "reports/index";
    }

    /*
    @RequestMapping(value = "helloReport1", method = RequestMethod.GET)
    @ResponseBody
    public void getRpt1(HttpServletResponse response) throws JRException, IOException {
      InputStream jasperStream = this.getClass().getResourceAsStream("/jasperreports/HelloWorld1.jasper");
      Map<String,Object> params = new HashMap<>();
      JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());

      response.setContentType("application/x-pdf");
      response.setHeader("Content-disposition", "inline; filename=helloWorldReport.pdf");

      final OutputStream outStream = response.getOutputStream();
      JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
    }
    */

    @RequestMapping(value="/reports/getpdf", method=RequestMethod.GET)
    public @ResponseBody ResponseEntity<byte[]> getPDF() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("incomes.xml").getFile());
        Report report = new WebAppReportXmlReader().parseXml(readFileToString(file));

        Reporting reporting = new Reporting();
        reporting.setFormatterFactory(new DefaultFormatterFactory());
        reporting.setLoaderFactory(
                new DefaultLoaderFactory()
                        .setGroovyDataLoader(new GroovyDataLoader(new DefaultScriptingImpl())));

        ReportOutputDocument reportOutputDocument = reporting.runReport(new RunParams(report));

        byte[] contents = reportOutputDocument.getContent();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        String filename = "output.xlsx";
//        headers.setContentDispositionFormData(filename, filename);
        headers.set("Content-Disposition", "attachment; filename=" + filename);
        headers.setContentLength(contents.length);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
        return response;
    }

}