package org.mcgi.jp.web.report;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.report.LiquidationReport;
import org.mcgi.jp.model.report.LiquidationReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.jasperreports.JasperReportsViewResolver;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@Controller
public class LiquidationReportController {

    private final Logger logger = LoggerFactory.getLogger(YargReportController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private LiquidationReportRepository liquiReportRepo;

    @RequestMapping(value = "/reports/liquidation", method = RequestMethod.GET)
    public String localeIndex(Map<String, Object> model) {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        if (localeAccounts == null || localeAccounts.isEmpty()) {
            model.put("msg", "No locales regisered.");
        }

        return "reports/liquidation/locale_index";
    }

    @RequestMapping(value = "/reports/liquidation/{localeId}", method = RequestMethod.GET)
    public String monthIndex(@PathVariable String localeId, Model model) {
        // Output current month's liquidation report
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        /*
         * Get last month's ending balance/forwarded end cash
         *   Separate collections into previous ending
         *   and current collection
         */

        // FIXME: Get previous month's ending cash on hand (ending balance)

        // Get current collection (start of this month until today)
        // Dependendent on Calendar table

        List<LiquidationReport> liquiReports = liquiReportRepo.findByLocale(localeAccount.getName());

        List<String> months = new ArrayList<String>();
        for (LiquidationReport liquiReport : liquiReports) {
            months.add(liquiReport.getYearMonth());
        }

        model.addAttribute("months", months);
        model.addAttribute("localeId", localeId);
        return "reports/liquidation/month_index";
    }

    @RequestMapping(value = "/reports/liquidation/{localeId}/{month}", method = RequestMethod.GET)
    public @ResponseBody void monthIndex(@PathVariable String localeId, @PathVariable String month,
            HttpServletRequest request, HttpServletResponse response)  throws JRException, IOException {

        InputStream jasperStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("jasper/JREmp1.jasper");
        Map<String, Object> params = new HashMap<>();
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);

        Map parameters = new HashMap();
        parameters.put("Title", "List of Contacts");
        parameters.put("noy", 10);

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());

        response.setContentType("application/x-pdf");
        response.setHeader("Content-disposition", "inline; filename=helloWorldReport.pdf");

        final OutputStream outStream = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

        JasperReportsViewResolver rn = new JasperReportsViewResolver();
    }

}