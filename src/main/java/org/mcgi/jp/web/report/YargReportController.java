package org.mcgi.jp.web.report;

import static org.apache.commons.io.FileUtils.readFileToString;

import java.io.File;
import java.io.IOException;

import org.mcgi.jp.web.report.helper.WebAppReportTemplateBuilder;
import org.mcgi.jp.web.report.helper.WebAppReportXmlReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.haulmont.yarg.formatters.factory.DefaultFormatterFactory;
import com.haulmont.yarg.loaders.factory.DefaultLoaderFactory;
import com.haulmont.yarg.loaders.impl.GroovyDataLoader;
import com.haulmont.yarg.reporting.ReportOutputDocument;
import com.haulmont.yarg.reporting.Reporting;
import com.haulmont.yarg.reporting.RunParams;
import com.haulmont.yarg.structure.Report;
import com.haulmont.yarg.structure.ReportBand;
import com.haulmont.yarg.structure.ReportOutputType;
import com.haulmont.yarg.structure.impl.BandBuilder;
import com.haulmont.yarg.structure.impl.ReportBuilder;
import com.haulmont.yarg.util.groovy.DefaultScriptingImpl;

@Controller
public class YargReportController {

    private final Logger logger = LoggerFactory.getLogger(YargReportController.class);

    @RequestMapping(value = "/reports/yarg/{localeId}/{month}/xml", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<byte[]> monthReportXml(
            @PathVariable String localeId, @PathVariable String month, Model model) throws IOException {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("incomes.xml").getFile());
        Report report = new WebAppReportXmlReader().parseXml(readFileToString(file));

        Reporting reporting = new Reporting();
        reporting.setFormatterFactory(new DefaultFormatterFactory());
        reporting.setLoaderFactory(
                new DefaultLoaderFactory()
                        .setGroovyDataLoader(new GroovyDataLoader(new DefaultScriptingImpl())));

        ReportOutputDocument reportOutputDocument = reporting.runReport(new RunParams(report));
        byte[] contents = reportOutputDocument.getContent();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        String filename = "liquidation_report.xlsx";
        headers.set("Content-Disposition", "attachment; filename=" + filename);
        headers.setContentLength(contents.length);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
        return response;
    }

    @RequestMapping(value = "/reports/yarg/{localeId}/{month}/xlsx", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<byte[]> monthReportXlsx(
            @PathVariable String localeId, @PathVariable String month, Model model) throws IOException {

        ReportBuilder reportBuilder = new ReportBuilder();
        //* OPTION1
        WebAppReportTemplateBuilder reportTemplateBuilder = new WebAppReportTemplateBuilder()
                .documentPath("liquidationReport1stPage.xlsx")
                .documentName("liquidationReport1stPage.xlsx")
                .outputType(ReportOutputType.xlsx)
                .readFromPath();
        reportBuilder.template(reportTemplateBuilder.build());
        /* */

        /* OPTION2
        ReportTemplateImpl repotTempImpl = new ReportTemplateImpl(ReportTemplate.DEFAULT_TEMPLATE_CODE,
                "liquidationReport1stPage.xlsx", "liquidationReport1stPage.xlsx", documentContent, ReportOutputType.xlsx);
        reportBuilder.template(repotTempImpl);
        /* */

        BandBuilder bandBuilder = new BandBuilder();
        ReportBand main= bandBuilder.name("Main").query("Main", "return [\n" +
                "                              [\n" +
                "                               'district':  'District 1',\n" +
                "                               'locale' : 'Chiba',\n" +
                "                               'forTheMonthOf' : 'For the Month of May 2016'\n" +
                "                            ]]", "groovy").build();
        reportBuilder.band(main);

        /*
        bandBuilder = new BandBuilder();
        ReportBand collections = bandBuilder.name("Collection").query("Collection", "return [\n" +
                "                              [\n" +
                "                               'forwardCOH':  100000,\n" +
                "                               'currentCollection' : 180000\n" +
                "                            ]]", "groovy").build();

        bandBuilder = new BandBuilder();
        ReportBand expenses = bandBuilder.name("Expense").query("Expense", "return [\n" +
                "                              [\n" +
                "                               'localeRent':  120000,\n" +
                "                               'hotelRental' : 1,\n" +
                "                               'electricBill' : 2,\n" +
                "                               'waterBill' : 3,\n" +
                "                               'telephoneBill' : 4,\n" +
                "                               'internet' : 5,\n" +
                "                               'transportation' : 6,\n" +
                "                               'foodExpense' : 7,\n" +
                "                               'medicines' : 8\n" +
                "                            ]]", "groovy").build();

        reportBuilder.band(collections);
        reportBuilder.band(expenses);
        */

        Report report = reportBuilder.build();

        Reporting reporting = new Reporting();
        reporting.setFormatterFactory(new DefaultFormatterFactory());
        reporting.setLoaderFactory(
                new DefaultLoaderFactory()
                        .setGroovyDataLoader(new GroovyDataLoader(new DefaultScriptingImpl())));

        ReportOutputDocument reportOutputDocument = reporting.runReport(new RunParams(report));
        byte[] contents = reportOutputDocument.getContent();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
        String filename = "liquidation_report.xlsx";
        headers.set("Content-Disposition", "attachment; filename=" + filename);
        headers.setContentLength(contents.length);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
        return response;
    }
}