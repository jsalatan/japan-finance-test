package org.mcgi.jp.web.report.helper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.google.common.base.Preconditions;
import com.haulmont.yarg.formatters.CustomReport;
import com.haulmont.yarg.structure.ReportOutputType;
import com.haulmont.yarg.structure.ReportTemplate;

public class WebAppReportTemplateBuilder {

    private WebAppReportTemplateImpl reportTemplate;

    public WebAppReportTemplateBuilder() {
        reportTemplate = new WebAppReportTemplateImpl();
    }

    public WebAppReportTemplateBuilder code(String code) {
        Preconditions.checkNotNull(code, "\"code\" parameter can not be null");
        reportTemplate.code = code;
        return this;
    }

    public WebAppReportTemplateBuilder documentName(String documentName) {
        Preconditions.checkNotNull(documentName, "\"documentName\" parameter can not be null");
        reportTemplate.documentName = documentName;
        return this;
    }

    public WebAppReportTemplateBuilder documentPath(String documentPath) {
        reportTemplate.documentPath = documentPath;
        return this;
    }

    public WebAppReportTemplateBuilder readFromPath() throws IOException {
        Preconditions.checkNotNull(reportTemplate.documentPath, "\"documentPath\" parameter is null. Can not load data from null path");
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(reportTemplate.documentPath).getFile());
        reportTemplate.documentContent = FileUtils.readFileToByteArray(file);
        return this;
    }

    public WebAppReportTemplateBuilder documentContent(byte[] documentContent) {
        Preconditions.checkNotNull(documentContent, "\"documentContent\" parameter can not be null");
        reportTemplate.documentContent = documentContent;
        return this;
    }

    public WebAppReportTemplateBuilder documentContent(InputStream documentContent) throws IOException {
        Preconditions.checkNotNull(documentContent, "\"documentContent\" parameter can not be null");
        reportTemplate.documentContent = IOUtils.toByteArray(documentContent);
        return this;
    }

    public WebAppReportTemplateBuilder outputType(ReportOutputType outputType) {
        Preconditions.checkNotNull(outputType, "\"outputType\" parameter can not be null");
        reportTemplate.reportOutputType = outputType;
        return this;
    }

    public WebAppReportTemplateBuilder outputNamePattern(String outputNamePattern) {
        reportTemplate.outputNamePattern = outputNamePattern;
        return this;
    }

    public WebAppReportTemplateBuilder custom(CustomReport customReport) {
        Preconditions.checkNotNull(customReport, "\"customReport\" parameter can not be null");
        reportTemplate.custom = true;
        reportTemplate.customReport = customReport;
        return this;
    }

    public ReportTemplate build() {
        reportTemplate.validate();
        WebAppReportTemplateImpl result = reportTemplate;
        reportTemplate = new WebAppReportTemplateImpl();
        return result;
    }
}
