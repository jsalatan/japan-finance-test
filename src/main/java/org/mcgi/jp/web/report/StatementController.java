package org.mcgi.jp.web.report;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.audit.Statement;
import org.mcgi.jp.model.audit.StatementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StatementController {

    private final Logger logger = LoggerFactory.getLogger(StatementController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private StatementRepository statementRepo;

    @RequestMapping(value = "/reports/statement", method = RequestMethod.GET)
    public String localeIndex(Map<String, Object> model) {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        if (localeAccounts == null || localeAccounts.isEmpty()) {
            model.put("msg", "No locales regisered.");
        }

        return "reports/statement/locale_index";
    }

    @RequestMapping(value = "/reports/statement/{localeId}", method = RequestMethod.GET)
    public String monthIndex(@PathVariable String localeId, Model model) {
        List<Statement> satementReports = statementRepo.findByLocaleId(localeId);

        List<String> months = new ArrayList<String>();
        for (Statement statement : satementReports) {
            months.add(statement.getStatementYm());
        }

        model.addAttribute("months", months);
        model.addAttribute("localeId", localeId);
        return "reports/statement/month_index";
    }

    @RequestMapping(value = "/reports/statement/{localeId}/{month}", method = RequestMethod.GET)
    public String statementMonth(@PathVariable String localeId, @PathVariable String month, Model model)  {

        LocaleAccount locale = localeAccountRepo.findOne(localeId);
        Statement satementReport = statementRepo.findByLocaleIdAndStatementYm(localeId, month);

        model.addAttribute("statement", satementReport);
        model.addAttribute("locale", locale);
        return "reports/statement/main";

    }

}