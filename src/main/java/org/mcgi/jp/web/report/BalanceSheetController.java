package org.mcgi.jp.web.report;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.BalanceSheet;
import org.mcgi.jp.model.BalanceSheetEntry;
import org.mcgi.jp.model.ExpenseType;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BalanceSheetController {

    private final Logger logger = LoggerFactory.getLogger(BalanceSheetController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/reports/balance_sheet", method = RequestMethod.GET)
    public String localeIndex(Map<String, Object> model) {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        return "reports/balance_sheet/locale_index";
    }

    @RequestMapping(value = "/reports/balance_sheet/{localeId}", method = RequestMethod.GET)
    public String listExpense(@PathVariable String localeId, Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        BalanceSheet balanceSheet = new BalanceSheet(localeAccount.getName());
        List<BalanceSheetEntry> entries = new ArrayList<BalanceSheetEntry>();
        balanceSheet.setEntries(entries);

        List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
        for (RegularCollection collection : collections) {
            BalanceSheetEntry entry = new BalanceSheetEntry();
            entry.setCredit(collection.getAmount());
            entry.setDate(collection.getTransactionDate());
            entry.setDescription(collection.getType().getDescription());
            entry.setDate(collection.getTransactionDate());
            entries.add(entry);
        }

        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
        for (RegularExpense expense : expenses) {
            BalanceSheetEntry entry = new BalanceSheetEntry();
            entry.setDebit(expense.getAmount());
            entry.setDate(expense.getTransactionDate());

            ExpenseType expenseType = expense.getExpenseType();
            if (expenseType != null) {
                entry.setDescription(expenseType.getDescription());
            }
            entry.setDate(expense.getTransactionDate());
            entries.add(entry);
        }
        Collections.sort(entries);

        BigDecimal runningBalance = new BigDecimal(0);
        BigDecimal sumCredit = new BigDecimal(0);
        BigDecimal sumDebit = new BigDecimal(0);
        for (int i = 0; i < entries.size(); i++) {
            BalanceSheetEntry entry = entries.get(i);

            runningBalance = runningBalance.add(entry.getCredit());
            runningBalance = runningBalance.subtract(entry.getDebit());
            entry.setBalance(runningBalance);

            sumCredit = sumCredit.add(entry.getCredit());
            sumDebit = sumDebit.add(entry.getDebit());
        }

        BalanceSheetEntry sumEntry = new BalanceSheetEntry();
        sumEntry.setDescription("Total:");
        sumEntry.setCredit(sumCredit);
        sumEntry.setDebit(sumDebit);
        sumEntry.setBalance(sumCredit.subtract(sumDebit));
        entries.add(sumEntry);

        model.addAttribute("balancesheet", balanceSheet);
        return "reports/balance_sheet/main";
    }

}