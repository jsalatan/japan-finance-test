package org.mcgi.jp.web.report;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.TransactionDateComparator;
import org.mcgi.jp.web.util.CollectionHelper;
import org.mcgi.jp.web.util.ExpenseHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AccountBalanceController {

    private final Logger logger = LoggerFactory.getLogger(AccountBalanceController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/reports/accounts", method = RequestMethod.GET)
    public String localeIndex(Map<String, Object> model) {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        return "reports/accounts/locale_index";
    }

    @RequestMapping(value = "/reports/accounts/{localeId}", method = RequestMethod.GET)
    public String listExpense(@PathVariable String localeId, Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        // Get collections
        List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
        // Sort
        Collections.sort(collections, new TransactionDateComparator());
        // get first and last element to get date range
        model.addAttribute("start", collections.get(0).getTransactionDate());
        model.addAttribute("end", collections.get(collections.size() - 1).getTransactionDate());

        Map<String, BigDecimal> accounts = new LinkedHashMap<String, BigDecimal>();
        Map<String, List<RegularCollection>> collectionsByTypeMap = CollectionHelper.separateByType(collections);
        for (String collType : collectionsByTypeMap.keySet()) {
            List<RegularCollection> collectionsOfType = collectionsByTypeMap.get(collType);

            BigDecimal total = BigDecimal.ZERO;
            for (RegularCollection collection : collectionsOfType) {
                total = total.add(collection.getAmount());
            }

            accounts.put(collType, total);
        }

        // Get expenses
        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
        Map<String, List<RegularExpense>> expensesByCollTypeMap = ExpenseHelper.separateByCollectionType(expenses);
        for (String expenseCollType : expensesByCollTypeMap.keySet()) {
            // Get total collection of type x
            BigDecimal totalCollection = accounts.get(expenseCollType);

            List<RegularExpense> expensesOfType = expensesByCollTypeMap.get(expenseCollType);
            BigDecimal totalExpenses = BigDecimal.ZERO;
            for (RegularExpense expense : expensesOfType) {
                totalExpenses = totalExpenses.add(expense.getAmount());
            }

            accounts.put(expenseCollType, totalCollection.subtract(totalExpenses));
        }

        // Add sum row
        BigDecimal grandTotal = BigDecimal.ZERO;
        for (String key : accounts.keySet()) {
            BigDecimal accountTotalBalance = accounts.get(key);
            grandTotal = grandTotal.add(accountTotalBalance);
        }
        accounts.put("Total", grandTotal);

        model.addAttribute("localeAccount", localeAccount);
        model.addAttribute("accounts", accounts);

        return "reports/accounts/main";
    }

}