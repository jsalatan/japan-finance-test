package org.mcgi.jp.web.report.helper;

import java.io.FileNotFoundException;
import java.io.InputStream;

import com.haulmont.yarg.structure.xml.impl.DefaultXmlReader;

public class WebAppReportXmlReader extends DefaultXmlReader {
    protected InputStream getDocumentContent(String documentPath) throws FileNotFoundException {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(documentPath);
 }
}
