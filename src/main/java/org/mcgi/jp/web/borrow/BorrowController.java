package org.mcgi.jp.web.borrow;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.ExpenseType;
import org.mcgi.jp.model.ExpenseTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.borrow.Borrow;
import org.mcgi.jp.model.borrow.BorrowRepository;
import org.mcgi.jp.model.collection.CollectionAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BorrowController {

    private final Logger logger = LoggerFactory.getLogger(BorrowController.class);

    @Autowired
    private BorrowRepository repo;

    @Autowired
    private ExpenseTypeRepository expenseTypeRepo;

    @Autowired
    private CollectionTypeRepository collTypeRepo;

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/borrow", method = RequestMethod.GET)
    public String localeIndex(Model model) {
        List<ExpenseType> expenseTypesList = expenseTypeRepo.findAll();
        model.addAttribute("expenseTypes", expenseTypesList);

        List<CollectionType> collTypes = collTypeRepo.findAll();
        model.addAttribute("collTypes", collTypes);

        Borrow borrow = new Borrow();
        model.addAttribute("borrow", borrow);
        return "borrow/new";
    }

    @RequestMapping(value = "/locale_account/{localeId}/borrow/new", method = RequestMethod.GET)
    public String newBorrow(@PathVariable String localeId, Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        List<ExpenseType> expenseTypesList = expenseTypeRepo.findAll();
        /*
        Map<String, String> expenseTypesMap = new HashMap<String, String>();
        for (ExpenseType expenseType : expenseTypesList) {
            expenseTypesMap.put(expenseType.getValue(), expenseType.getDescription());
        }
        model.addAttribute("expenseTypes", expenseTypesMap);
        /* */
        model.addAttribute("expenseTypes", expenseTypesList);

        List<CollectionAccount> accounts = localeAccount.getAccounts();
        model.addAttribute("fundSources", accounts);

        Borrow borrow = new Borrow();
        model.addAttribute("formObj", borrow);

        return "borrow/new";

    }

    @RequestMapping(value = "/locale_account/{localeId}/borrow/new", method = RequestMethod.POST)
    public String create(@PathVariable String localeId,
            @RequestParam(name="payFor") String expenseTypeDescription,
            @RequestParam BigDecimal amount, @RequestParam(name="borrowFrom") String typeDescription,
            @RequestParam Date transactionDate, ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        // create new expense and save to expenses table
        Borrow borrow = new Borrow();
        borrow.setPayFor(expenseTypeRepo.findByDescription(expenseTypeDescription));
        borrow.setAmount(amount);
        borrow.setBorrowFrom(collTypeRepo.findByDescription(typeDescription));
        borrow.setTransactionDate(transactionDate);
        Borrow registeredBorrow = repo.insert(borrow);

        // add to locale account's borrows and save locale account
        /* TODO: For deletion (use payables not borrows)
        List<Borrow> borrows = localeAccount.getBorrows();
        borrows.add(registeredBorrow);
        localeAccountRepo.save(localeAccount);
        /* */

        return "redirect:/locale_account/" + localeId + "/expense";
    }

}