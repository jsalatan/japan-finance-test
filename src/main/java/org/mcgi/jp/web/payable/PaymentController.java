package org.mcgi.jp.web.payable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.payable.Payable;
import org.mcgi.jp.model.payable.Payment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PaymentController {

    private final Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private UserRepository userRepo;

    @RequestMapping(value = "/locale_account/{localeId}/payable/{payableId}/payment", method = RequestMethod.GET)
    public String index(
            @PathVariable String localeId,
            @PathVariable String payableId,
            Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        List<Payable> payables = localeAccount.getPayables();
        Payable targetPayable = getTargetPayable(payables, payableId);
        List<Payment> payments = targetPayable.getPayments();

        BigDecimal total = targetPayable.getTotal();
        BigDecimal paymentsTotal = BigDecimal.ZERO;
        for (Payment payment : payments) {
            paymentsTotal = paymentsTotal.add(payment.getAmount());
        }
        targetPayable.setBalance(total.subtract(paymentsTotal));
        targetPayable.setPaymentsTotal(paymentsTotal);

        model.addAttribute("payable", targetPayable);
        return "payable/payment/index";
    }

    private Payable getTargetPayable(List<Payable> payables, String payableId) {
        Payable targetPayable = null;
        for (Payable payable : payables) {
            ObjectId id = payable.getId();
            if (id.toString().equals(payableId)) {
                targetPayable = payable;
                break;
            }
        }

        return targetPayable;
    }

    @RequestMapping(value = "/locale_account/{localeId}/payable/{payableId}/payment/new", method = RequestMethod.GET)
    public String newObj(
            @PathVariable String localeId,
            @PathVariable String payableId,
            Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        Payable targetPayable = getTargetPayable(localeAccount.getPayables(), payableId);
        model.addAttribute("payable", targetPayable);

        Payment payment = new Payment();
        model.addAttribute("formObj", payment);

        List<JapanFinanceUser> users = userRepo.findAll();
        model.addAttribute("users", users);

        return "payable/payment/new";

    }

    @RequestMapping(value = "/locale_account/{localeId}/payable/{payableId}/payment/new", method = RequestMethod.POST)
    public String create(
            @PathVariable String localeId,
            @PathVariable String payableId,
            @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date paymentDate,
            @RequestParam BigDecimal amount,
            @RequestParam String receivedBy,
            @RequestParam String paidBy,
            ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        Payment newPayment = new Payment();
        newPayment.setPaymentDate(paymentDate);
        newPayment.setAmount(amount);
        newPayment.setReceivedBy(receivedBy);
        newPayment.setPaidBy(paidBy);

        Payable targetPayable = getTargetPayable(localeAccount.getPayables(), payableId);
        List<Payment> payments = targetPayable.getPayments();
        payments.add(newPayment);

        BigDecimal newBalance = BigDecimal.ZERO;
        newBalance = targetPayable.getBalance().subtract(newPayment.getAmount());
        targetPayable.setBalance(newBalance);

        localeAccountRepo.save(localeAccount);

        return "redirect:/locale_account/" + localeId + "/payable/" + payableId + "/payment";
    }

}