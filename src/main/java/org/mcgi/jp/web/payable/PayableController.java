package org.mcgi.jp.web.payable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.mcgi.jp.model.ExpenseTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.payable.Payable;
import org.mcgi.jp.model.payable.Payment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PayableController {

    private final Logger logger = LoggerFactory.getLogger(PayableController.class);

    @Autowired
    private ExpenseTypeRepository expenseTypeRepo;

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private UserRepository userRepo;

    @RequestMapping(value = "/payable", method = RequestMethod.GET)
    public String localeIndex(Model model) {
        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.addAttribute("localeAccounts", localeAccounts);

        if (localeAccounts == null || localeAccounts.isEmpty()) {
            model.addAttribute("msg", "No locales regisered.");
        }

        return "payable/locale_index";
    }
    @RequestMapping(value = "/locale_account/{localeId}/payable", method = RequestMethod.GET)
    public String index(@PathVariable String localeId, Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        List<Payable> payables = localeAccount.getPayables();

        for (Payable payable : payables) {
            List<Payment> payments = payable.getPayments();

            BigDecimal total = payable.getTotal();
            BigDecimal balance = payable.getBalance();
            if (balance == null) {
                balance = total;
            }
            for (Payment payment : payments) {
                balance = balance.subtract(payment.getAmount());
            }
            payable.setBalance(balance);
        }

        model.addAttribute("payables", payables);
        return "payable/index";
    }

    @RequestMapping(value = "/locale_account/{localeId}/payable/new", method = RequestMethod.GET)
    public String newObj(@PathVariable String localeId, Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

//        List<ExpenseType> expenseTypesList = expenseTypeRepo.findAll();
//        model.addAttribute("expenseTypes", expenseTypesList);
//
//        List<CollectionAccount> accounts = localeAccount.getAccounts();
//        model.addAttribute("fundSources", accounts);

        Payable payable = new Payable();
        model.addAttribute("formObj", payable);

        List<JapanFinanceUser> users = userRepo.findAll();
        model.addAttribute("users", users);

        return "payable/new";

    }

    @RequestMapping(value = "/locale_account/{localeId}/payable/new", method = RequestMethod.POST)
    public String create(@PathVariable String localeId,
            @RequestParam String description,
            @RequestParam String lender,
            @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date dateBorrowed,
            @RequestParam BigDecimal total,
            ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        // create new expense and save to expenses table
        Payable payable = new Payable();
        payable.setDescription(description);
        payable.setLender(lender);
        payable.setDateBorrowed(dateBorrowed);
        payable.setTotal(total);
        payable.setBalance(total);

        List<Payable> payables = localeAccount.getPayables();
        payables.add(payable);

        localeAccountRepo.save(localeAccount);

        return "redirect:/locale_account/" + localeId + "/payable";
    }

}