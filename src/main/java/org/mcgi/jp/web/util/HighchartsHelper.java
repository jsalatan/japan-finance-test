package org.mcgi.jp.web.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.LocaleAccount;

public class HighchartsHelper {

    public static List<Map<String,Object>> getPieSeriesData(List<LocaleAccount> localeAccounts) {

        /*
         * 1. Get total
         * 2. Compute for percentage
         * 3. create map
         */

        // Get total
        List<Object[]> collectionsByLocale = new ArrayList<Object[]>();
        BigDecimal allCollections = new BigDecimal(0);
        for (LocaleAccount localeAccount : localeAccounts) {
            List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();

            BigDecimal totalCollectionsByLocale = new BigDecimal(0);
            for (RegularCollection collection : collections) {
                totalCollectionsByLocale = totalCollectionsByLocale.add(collection.getAmount());
            }

            collectionsByLocale.add(new Object[] {localeAccount.getName(), totalCollectionsByLocale, localeAccount.getId()});
            allCollections = allCollections.add(totalCollectionsByLocale);
        }

        // Convert value to percentage
        /*
         * seriesValues[0] = name
         * seriesValues[1] = y
         * seriesValues[2] = drilldown
         */
        /*
        for (Object[] seriesValues : collectionsByLocale) {
            BigDecimal totalCollectionsByLocale = (BigDecimal) seriesValues[1];
            BigDecimal decimalPercentage = totalCollectionsByLocale.divide(allCollections, 1, RoundingMode.HALF_UP);
            seriesValues[1] =  decimalPercentage.multiply(new BigDecimal(100));
        }
        /* */

        // Create map
        List<Map<String, Object>> seriesData = new ArrayList<Map<String, Object>>();
        for (Object[] seriesValues : collectionsByLocale) {
            Map<String, Object> seriesDatum = new HashMap<String, Object>();
            seriesDatum.put("name", seriesValues[0]);
            seriesDatum.put("y", seriesValues[1]);
            seriesDatum.put("drilldown", true);

            seriesData.add(seriesDatum);
        }

        return seriesData;
    }


//  StringBuffer sb = new StringBuffer();
//  sb.append(" { ");
//  sb.append("     'Chiba': { ");
//  sb.append("         name: 'Chiba', ");
//  sb.append("         data: [ ");
//  sb.append("             ['LF', 30,000], ");
//  sb.append("             ['LEF', 180,000] ");
//  sb.append("         ] ");
//  sb.append("     }, ");
//  sb.append("     'Tokyo': { ");
//  sb.append("         name: 'Tokyo', ");
//  sb.append("         data: [ ");
//  sb.append("             ['LF', 200,000] ");
//  sb.append("         ] ");
//  sb.append("     } ");
//  sb.append(" } ");
//
//  return sb.toString();
    public static Map<String, Map<String, Object>> getPieDrilldownDataForAsync(List<LocaleAccount> localeAccounts) {
        /*
         * Map
         *   Map
         *     List
         */
        Map<String, Map<String, Object>> drilldownData = new HashMap<String, Map<String, Object>>();
        for (LocaleAccount localeAccount : localeAccounts) {

            Map<String, Object> drilldownDatum = new HashMap<String, Object>();

            drilldownDatum.put("name", localeAccount.getName());
            // 1. Separate collections by type
            List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
            Map<String, BigDecimal> collectionsByType = new HashMap<String, BigDecimal>();
            for (RegularCollection collection : collections) {
                String collTypeDescription = collection.getType().getDescription();

                BigDecimal accumulativeCollection = collectionsByType.get(collTypeDescription);
                if (accumulativeCollection == null) {
                    accumulativeCollection = collection.getAmount();
                } else {
                    accumulativeCollection = accumulativeCollection.add(collection.getAmount());
                }
                collectionsByType.put(collTypeDescription, accumulativeCollection);
            }

            // 2. Convert Map to List
            List<Object[]> data = new ArrayList<Object[]>();
            for (String collectionType : collectionsByType.keySet()) {
                BigDecimal totalCollectionByType = collectionsByType.get(collectionType);
                data.add(new Object[] {collectionType, totalCollectionByType});
            }
            drilldownDatum.put("data", data);

            drilldownData.put(localeAccount.getName(), drilldownDatum);
        }

        return drilldownData;
    }

}
