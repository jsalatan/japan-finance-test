package org.mcgi.jp.web.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.RegularExpense;

public class ExpenseHelper {

    // Prevent this from being instantiated
    private ExpenseHelper() {
    }

    public static Map<String, List<RegularExpense>> separateByCollectionType(List<RegularExpense> expenses) {

        Map<String, List<RegularExpense>> expensesByCollType = new HashMap<String, List<RegularExpense>>();
        for (RegularExpense expense : expenses) {
            String collTypeDescAsKey = expense.getSourceOfFunds().getDescription();
            List<RegularExpense> list = expensesByCollType.get(collTypeDescAsKey);

            if (list == null) {
                list = new ArrayList<RegularExpense>();
                list.add(expense);
                expensesByCollType.put(collTypeDescAsKey, list);
            } else {
                list.add(expense);
            }
        }

        return expensesByCollType;
    }

}
