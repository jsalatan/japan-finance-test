package org.mcgi.jp.web.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.RegularCollection;

public class CollectionHelper {

    // Prevent this from being instantiated
    private CollectionHelper() {
    }

    public static Map<String, List<RegularCollection>> separateByType(List<RegularCollection> collections) {
        Map<String, List<RegularCollection>> collectionsByType = new HashMap<String, List<RegularCollection>>();
        for (RegularCollection collection : collections) {
            String key = collection.getType().getDescription();
            List<RegularCollection> list = collectionsByType.get(key);

            if (list == null) {
                list = new ArrayList<RegularCollection>();
                list.add(collection);
                collectionsByType.put(key, list);
            } else {
                list.add(collection);
            }
        }

        return collectionsByType;
    }

}
