package org.mcgi.jp.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.mcgi.jp.enumeration.ProjectScope;
import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.ExpenseType;
import org.mcgi.jp.model.ExpenseTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.Role;
import org.mcgi.jp.model.admin.RoleRepository;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.project.Project;
import org.mcgi.jp.model.project.ProjectBook;
import org.mcgi.jp.model.project.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    private final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private LocaleAccountRepository localeRepo;

    @Autowired
    private CollectionTypeRepository collTypRepo;

    @Autowired
    private ExpenseTypeRepository expenseTypRepo;

    @Autowired
    private ProjectRepository projectRepo;

    private static final Map<String, List<String[]>> USERS_MAP = new HashMap<String, List<String[]>>();
    static {
        List<String[]> localeUsers = new ArrayList<String[]>();
        localeUsers.add(new String[] {"dela.cruz.jose.jr", "Jose Jr.", "Dela Cruz", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"dela.pena.romano", "Romano", "Dela Pena", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"mabini.irvin", "Irvin", "Mabini", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"mabini.bencil", "Bencil", "Mabini", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"pc.tech.dave", "Dave", "PC Tech", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"sarmiento.ben", "Ben", "Sarmiento", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"macam.malou", "Macam", "Malou", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"madel.airish", "Airish", "Madel", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"maeda.gemma", "Gemma", "Maeda", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"roxas.gemma", "Gemma", "Roxas", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"tsuruya.marilyn", "Marilyn", "Tsuruya", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"sugiwara.grace", "Grace", "Sugiwara", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"kubota.violy", "Violy", "Kubota", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"sunga.arlan", "Arlan", "Sunga", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"okumura.henrietta", "Henrietta", "Okumura", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"quezon.ruel", "Ruel", "Quezon", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"astorga.betty", "Betty", "Astorga", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"yamamoto.emilie", "Emilie", "Yamamoto", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"de.guzman.rommel", "Rommel", "De Guzman", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"kawashima.linda", "Linda", "Kawashima", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"aramachi.raquel", "Raquel", "Aramachi", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"sakurai.susana", "Susana", "Sakurai", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"yabut.ronnie", "Ronnie", "Yabut", "1234", "ROLE_MEMBER"});
        localeUsers.add(new String[] {"yoshimura.cecillelyn", "Cecille Lyn", "Yoshimura", "1234", "ROLE_MEMBER"});

        localeUsers.add(new String[] {"kinjou.cherry", "Cherry", "Kinjou", "1234", "ROLE_MEMBER"});

        localeUsers.add(new String[] {"aveno.angie", "Angie", "Aveno", "1234", "ROLE_GROUP_TREASURER"});

        localeUsers.add(new String[] {"petil.jess", "Jess", "Petil", "1234", "ROLE_GROUP_SECRETARY"});
        localeUsers.add(new String[] {"lugares.ej", "EJ", "Lugares", "1234", "ROLE_GROUP_SECRETARY"});

        localeUsers.add(new String[] {"naraoka.marilyn", "Marilyn", "Naraoka", "1234", "ROLE_LOCALE_SECRETARY"});
        localeUsers.add(new String[] {"gundayao.jen", "Jen", "Gundayao", "1234", "ROLE_LOCALE_SECRETARY"});

        localeUsers.add(new String[] {"yamamoto.jinky", "Jinky", "Yamamoto", "1234", "ROLE_LOCALE_TREASURER"});

        localeUsers.add(new String[] {"sekiguchi.zeny", "Zeny", "Sekiguchi", "1234", "ROLE_LOCALE_AUDITOR"});

        localeUsers.add(new String[] {"suzuki.ryuiji", "Ryuiji", "Suzuki", "1234", "ROLE_GROUP_SERVANT"});
        localeUsers.add(new String[] {"petil.bryan", "Bryan", "Petil", "1234", "ROLE_GROUP_SERVANT"});

        localeUsers.add(new String[] {"lugares.mariano.jr", "Mariano Jr.", "Lugares", "1234", "ROLE_LOCALE_COORDINATOR"});
        localeUsers.add(new String[] {"salatan.jonathan", "Jonathan Eastmael", "Salatan", "1234", "ROLE_LOCALE_COORDINATOR"});

        USERS_MAP.put("CHB", localeUsers);

        List<String[]> usersLocaleOfTokyo = new ArrayList<String[]>();
        usersLocaleOfTokyo.add(new String[] {"bongcac.joyce", "Joyce", "Bongcac", "1234", "ROLE_NATIONAL_TREASURER"});
        usersLocaleOfTokyo.add(new String[] {"menor.robert", "Robert", "Menor", "1234", "ROLE_DISTRICT_COORDINATOR"});

        USERS_MAP.put("TOK", usersLocaleOfTokyo);
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Map<String, Object> model) {

        createRoles();
        createAdmin();
        createLocales();
        createProjects();

        List<LocaleAccount> locales = localeRepo.findAll();
        for (LocaleAccount localeAccount : locales) {
            createLocaleFundProject(localeAccount.getId());
        }

        createCollectionTypes();
        createExpenseTypes("CHB");
        createUsers("CHB");
        createUsers("TOK");

        return "redirect:/dashboard";
    }

    private void createProjects() {
        Project localeFundProj = projectRepo.findByDescription("Locale Fund");
        if (localeFundProj == null) {
            Project project = new Project();
            project.setDescription("Locale Fund");
            project.setScope(ProjectScope.LOCALE);
            projectRepo.save(project);
        }
    }

    private void createLocaleFundProject(String localeId) {
        LocaleAccount localeAccount = localeRepo.findOne(localeId);
        Project localeFundProj = projectRepo.findByDescription("Locale Fund");
        ProjectBook lf = localeAccount.getProjectBook(localeFundProj.getDescription());
        if (lf == null) {
            lf = new ProjectBook();
            lf.setProjectDesc(localeFundProj.getDescription());
            localeAccount.getProjectBooks().add(lf);
            localeRepo.save(localeAccount);
        }
    }

    private void createUsers(String localeId) {

        List<String[]> usersToRegister = USERS_MAP.get(localeId);
        List<JapanFinanceUser> usersByLocale = userRepo.findByLocale(localeId);
        if (usersByLocale.size() > 0 && usersByLocale.size() != usersToRegister.size()) {
            return;
        }

        for (String[] userInfo : usersToRegister) {
            List<Role> role = new ArrayList<Role>();
            role.add(roleRepo.findOne(userInfo[4]));

            String id = userInfo[0];
            if (!userRepo.exists(id)) {
                JapanFinanceUser newUser = new JapanFinanceUser(id, userInfo[1], userInfo[2], localeId, userInfo[3], role);
                userRepo.save(newUser);
            }
        }

    }

    private void createExpenseTypes(String localeId) {
        List<String[]> expenseTypesToReg = new ArrayList<String[]>();
        expenseTypesToReg.add(new String[] {"Locale Rent", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Remittance Fee", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"NTT", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Yahoo BB", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Water 1st floor", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Water 2nd floor", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Electricity 1st floor", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Electricity 2nd floor", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Gas", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Locale Equipment Upgrade", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Cellphone", "Locale Fund"});
        expenseTypesToReg.add(new String[] {"Health Insurance", "Locale Fund"});

        for (String[] expenseTypeToReg : expenseTypesToReg) {

            String desc = expenseTypeToReg[0];
            ExpenseType existingExpenseType = expenseTypRepo.findByDescription(desc);

            if (existingExpenseType == null) {
                ExpenseType newExpenseType = new ExpenseType();
                newExpenseType.setId(ObjectId.get().toString());
                newExpenseType.setDescription(expenseTypeToReg[0]);
                newExpenseType.setDefaultSourceOfFund(collTypRepo.findByDescription(expenseTypeToReg[1]));
                newExpenseType.setAppliesToLocale(localeId);
                expenseTypRepo.insert(newExpenseType);
            }
        }
    }

    private void createCollectionTypes() {
        List<CollectionType> collTypes = collTypRepo.findAll();
        if (collTypes == null || collTypes.isEmpty()) {
            collTypRepo.insert(CollectionType.LOCALE_FUND);
            collTypRepo.insert(CollectionType.LOCALE_EMERGENCY_FUND);
        }
    }

    private void createLocales() {
        List<LocaleAccount> locales = localeRepo.findAll();
        if (locales == null || locales.isEmpty()) {
            localeRepo.insert(new LocaleAccount("CHB", "Chiba"));
            localeRepo.insert(new LocaleAccount("TOK", "Tokyo"));
        }
    }

    private void createAdmin() {
        JapanFinanceUser admin = userRepo.findOne("admin");
        if (admin == null) {
            List<Role> adminRole = new ArrayList<Role>();
            adminRole.add(roleRepo.findOne("ROLE_ADMIN"));
            JapanFinanceUser user = new JapanFinanceUser("admin", "Admin", "Admin", null, "admin", adminRole);
            userRepo.save(user);
        }
    }

    private void createRoles() {
        List<Role> roles = roleRepo.findAll();
        if (roles == null || roles.isEmpty()) {
            roles.add(roleRepo.save(new Role("ROLE_ADMIN", "Admin")));

            roles.add(roleRepo.save(new Role("ROLE_MEMBER", "Locale Member")));

            roles.add(roleRepo.save(new Role("ROLE_GROUP_SECRETARY", "Locale Secretary")));
            roles.add(roleRepo.save(new Role("ROLE_GROUP_TREASURER", "Locale Treasurer")));
            roles.add(roleRepo.save(new Role("ROLE_GROUP_SERVANT", "Locale Secretary")));

            roles.add(roleRepo.save(new Role("ROLE_LOCALE_SECRETARY", "Locale Secretary")));
            roles.add(roleRepo.save(new Role("ROLE_LOCALE_AUDITOR", "Locale Auditor")));
            roles.add(roleRepo.save(new Role("ROLE_LOCALE_TREASURER", "Locale Treasurer")));
            roles.add(roleRepo.save(new Role("ROLE_LOCALE_COORDINATOR", "Locale Coordinator")));

            roles.add(roleRepo.save(new Role("ROLE_NATIONAL_SECRETARY", "National Secretary")));
            roles.add(roleRepo.save(new Role("ROLE_NATIONAL_AUDITOR", "National Auditor")));
            roles.add(roleRepo.save(new Role("ROLE_NATIONAL_TREASURER", "National Treasurer")));

            roles.add(roleRepo.save(new Role("ROLE_DISTRICT_COORDINATOR", "District Coordinator")));
            roles.add(roleRepo.save(new Role("ROLE_DISTRICT_SERVANT", "District Servant")));
        }
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home() {
        return "home";
    }

}