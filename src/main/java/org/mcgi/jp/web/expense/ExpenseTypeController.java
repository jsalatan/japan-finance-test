package org.mcgi.jp.web.expense;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.ExpenseType;
import org.mcgi.jp.model.ExpenseTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ExpenseTypeController {

    private final Logger logger = LoggerFactory.getLogger(ExpenseTypeController.class);

    @Autowired
    private ExpenseTypeRepository expenseTypeRepo;

    @Autowired
    private CollectionTypeRepository collectionTypeRepo;

    @RequestMapping(value = {"/expense_type", "/expense_type/index" }, method = RequestMethod.GET)
    public String index(Map<String, Object> model) {
        List<ExpenseType> expenseTypes = expenseTypeRepo.findAll();
        model.put("expenseTypes", expenseTypes);

        return "expense_type/index";
    }

    @RequestMapping(value = "/expense_type/new", method = RequestMethod.GET)
    public String newExpense(Model model) {

        List<CollectionType> collectionTypesList = collectionTypeRepo.findAll();
        Map<String, String> collectionTypesMap = new HashMap<String, String>();
        for (CollectionType item : collectionTypesList) {
            collectionTypesMap.put(item.getDescription(), item.getDescription());
        }
        model.addAttribute("collectionTypes", collectionTypesMap);

        ExpenseType expenseType = new ExpenseType();
        model.addAttribute("expenseType", expenseType);

        return "expense_type/new";

    }

//    @RequestMapping(value = "/expense_type/new", method = RequestMethod.POST)
//    public String registerExpense(@ModelAttribute("SpringWeb") ExpenseType expenseType, ModelMap model) {
//        expenseTypeRepo.insert(expenseType);
//        return "redirect:/expense_type";
//    }

    @RequestMapping(value = "/expense_type/new", method = RequestMethod.POST)
    public String createExpenseType(@RequestParam String description,
            @RequestParam(name="defaultSourceOfFund") String sourceTypeDescription, ModelMap model) {

        ExpenseType expenseType = new ExpenseType();
        expenseType.setDescription(description);
        expenseType.setDefaultSourceOfFund(collectionTypeRepo.findByDescription(sourceTypeDescription));

        expenseTypeRepo.insert(expenseType);
        return "redirect:/expense_type";
    }

}