package org.mcgi.jp.web.expense;

import java.util.Map;

import org.mcgi.jp.model.AccountingLedger;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LedgerController {

    private final Logger logger = LoggerFactory.getLogger(LedgerController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/ledger", method = RequestMethod.GET)
    public String index(Map<String, Object> model) {
        return "ledger/index";
    }

    @RequestMapping(value = "/ledger/generate/{localeId}", method = RequestMethod.GET)
    public String generateLedger(@PathVariable String localeId, Map<String, Object> model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        AccountingLedger ledger = new AccountingLedger(localeAccount);

        model.put("entries", ledger.getEntries());
        return "ledger/index";
    }

}