package org.mcgi.jp.web.expense;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.ExpenseType;
import org.mcgi.jp.model.ExpenseTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.TransactionDateComparator;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.collection.CollectionAccount;
import org.mcgi.jp.web.CsvFileUploadForm;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ExpenseController {

    private final Logger logger = LoggerFactory.getLogger(ExpenseController.class);

    @Autowired
    private ExpenseTypeRepository expenseTypeRepo;

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private CollectionTypeRepository collTypeRepo;

    @Autowired
    private UserRepository userRepo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/expense", method = RequestMethod.GET)
    public String localeIndex(Map<String, Object> model) {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        if (localeAccounts == null || localeAccounts.isEmpty()) {
            model.put("msg", "No locales regisered.");
        }

        return "expense/locale_index";
    }

    @RequestMapping(value = "/locale_account/{localeId}/expense", method = RequestMethod.GET)
    public String listExpense(@PathVariable String localeId, Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
        Collections.sort(expenses, new TransactionDateComparator());
        model.addAttribute("expenses", expenses);
        model.addAttribute("fileuploadForm", new CsvFileUploadForm());
        return "expense/index";
    }

    @RequestMapping(value = "/locale_account/{localeId}/expense/new", method = RequestMethod.GET)
    public String newExpense(@PathVariable String localeId, Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        List<ExpenseType> expenseTypesList = expenseTypeRepo.findByAppliesToLocale("CHB");
        model.addAttribute("expenseTypes", expenseTypesList);

        List<CollectionAccount> accounts = localeAccount.getAccounts();
        model.addAttribute("fundSources", accounts);

        RegularExpense expense = new RegularExpense();
        model.addAttribute("expenseForm", expense);

        List<JapanFinanceUser> users = userRepo.findAll();
        model.addAttribute("users", users);

        return "expense/new";

    }

    @RequestMapping(value = "/locale_account/{localeId}/expense/new", method = RequestMethod.POST)
    public String createExpense(@PathVariable String localeId,
            @RequestParam(name="expenseType") String expenseTypeDescription,
            @RequestParam BigDecimal amount,
            @RequestParam(name="sourceOfFunds") String typeDescription,
            @RequestParam(name="transactionDate") Date paymentDate,
            @RequestParam Date dueDate,
            ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        // create new expense and save as locale expense
        RegularExpense newExpense = new RegularExpense();
        newExpense.setExpenseType(expenseTypeRepo.findByDescription(expenseTypeDescription));
        newExpense.setAmount(amount);
        newExpense.setSourceOfFunds(collTypeRepo.findByDescription(typeDescription));
        newExpense.setTransactionDate(paymentDate);
        newExpense.setDueDate(dueDate);

        // Get user info and set as registered by
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JapanFinanceUser user = ((CustomUserDetails)principal).getUser();
        newExpense.setRegisteredBy(user.getUsername());

        // add to locale account's expenses and save locale account
        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
        expenses.add(newExpense);
        localeAccountRepo.save(localeAccount);

        // save/persist update locale account
        localeAccountRepo.save(localeAccount);
        return "redirect:/locale_account/" + localeId + "/expense";
    }

    @RequestMapping(value = "/locale_account/{localeId}/expense/check_balance", method = RequestMethod.GET)
    public @ResponseBody String checkBalance(@PathVariable String localeId, @RequestParam String expenseType,
            @RequestParam BigDecimal amount, @RequestParam String sourceOfFunds,
            @RequestParam(name="transactionDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date paymentDate,
            ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        // Get target collection account and debit expense
        CollectionAccount accountToUse = localeAccount.getAccount(sourceOfFunds);
        if (accountToUse == null) {
            accountToUse = new CollectionAccount(sourceOfFunds);
            localeAccount.getAccounts().add(accountToUse);
            localeAccountRepo.save(localeAccount);
        }
        // subtract expense from target collection account balance
        BigDecimal balance = accountToUse.getBalance();

        return String.valueOf(balance.toPlainString());
    }

    @RequestMapping(value = "/locale_account/{localeId}/expense/{expenseId}/edit", method = RequestMethod.GET)
    public String editExpense(@PathVariable String localeId, @PathVariable String expenseId, Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        List<ExpenseType> expenseTypesList = expenseTypeRepo.findAll();
        Map<String, String> expenseTypesMap = new HashMap<String, String>();
        for (ExpenseType expenseType : expenseTypesList) {
            expenseTypesMap.put(expenseType.getDescription(), expenseType.getDescription());
        }
        model.addAttribute("expenseTypes", expenseTypesMap);

        List<CollectionAccount> accounts = localeAccount.getAccounts();
        model.addAttribute("fundSources", accounts);

        RegularExpense targetExpense = null;
        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
        for (RegularExpense expense : expenses) {
            if(expenseId.equals(expense.getId())) {
                targetExpense = expense;
                break;
            }
        }
        model.addAttribute("expenseForm", targetExpense);

        return "expense/edit";

    }

    @RequestMapping(value = "/locale_account/{localeId}/expense/{expenseId}/edit", method = RequestMethod.POST)
    public String updateExpense(@PathVariable String localeId, @PathVariable String expenseId,
            @RequestParam(name="expenseType") String expenseTypeDescription, @RequestParam BigDecimal amount,
            @RequestParam(name="sourceOfFunds") String typeDescription,
            @RequestParam(name="dueDate") Date dueDate,
            @RequestParam(name="transactionDate") Date paymentDate,
            ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        // create new expense and save to expenses table
        RegularExpense targetExpense = null;
        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
        for (RegularExpense expense : expenses) {
            if(expenseId.equals(expense.getId())) {
                targetExpense = expense;
                break;
            }
        }

        // Save updated expense
        targetExpense.setExpenseType(expenseTypeRepo.findByDescription(expenseTypeDescription));
        targetExpense.setAmount(amount);
        targetExpense.setSourceOfFunds(collTypeRepo.findByDescription(typeDescription));
        targetExpense.setDueDate(dueDate);
        targetExpense.setTransactionDate(paymentDate);

        localeAccount.updateExpense(targetExpense);

        // save/persist update locale account
        localeAccountRepo.save(localeAccount);
        return "redirect:/locale_account/" + localeId + "/expense";
    }

    @RequestMapping(value = "/locale_account/{localeId}/expense/{expenseId}/delete", method = RequestMethod.GET)
    public String delete(
            @PathVariable final String localeId,
            @PathVariable final String expenseId,
            Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
        for (int i = 0; i < expenses.size(); i++) {
            RegularExpense expense = expenses.get(i);
            ObjectId id = expense.getId();
            if (id != null && expenseId.equals(id.toString())) {
                expenses.remove(i);
            }
        }
        localeAccountRepo.save(localeAccount);

        return "redirect:/locale_account/" + localeId + "/expense";
    }

}