package org.mcgi.jp.web.expense;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.ExpenseTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.collection.CollectionAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ExpenseUploadController {

    private final Logger logger = LoggerFactory.getLogger(ExpenseUploadController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private ExpenseTypeRepository expenseTypeRepo;

    @Autowired
    private CollectionTypeRepository collTypeRepo;

    @RequestMapping(value = "/locale_account/{localeId}/expense/upload", method = RequestMethod.POST)
    public String csvUpload(@PathVariable String localeId, @RequestParam MultipartFile file) throws Exception {

        XSSFWorkbook myWorkBook = null;
        try{
            myWorkBook = new XSSFWorkbook (file.getInputStream());
            // Return first sheet from the XLSX workbook
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);
            // Get iterator to all the rows in current sheet
            Iterator<Row> rowIterator = mySheet.iterator();

            LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

            // Traversing over each row of XLSX file
            boolean isFirstRow = true;
            while (rowIterator.hasNext()) {
                // Skip first row (table header)
                if (isFirstRow) {
                    rowIterator.next();
                    isFirstRow = false;
                    continue;
                }

                Row row = rowIterator.next();
                // For each row, iterate through each columns
                RegularExpense expense = new RegularExpense();

                int cellIdx = 0;

                String typeDesc = row.getCell(cellIdx++).getStringCellValue();
                expense.setExpenseType(expenseTypeRepo.findByDescription(typeDesc));

                double amount = row.getCell(cellIdx++).getNumericCellValue();
                expense.setAmount(new BigDecimal(amount));

                String remittedBy = row.getCell(cellIdx++).getStringCellValue();
                expense.setRemittedBy(remittedBy);

                // Get user info and set as registered by
                Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                JapanFinanceUser user = ((CustomUserDetails)principal).getUser();
                expense.setRegisteredBy(user.getUsername());

                Date paymentDate = row.getCell(cellIdx++).getDateCellValue();
                expense.setTransactionDate(paymentDate);

                String srcOfFundDesc = row.getCell(cellIdx++).getStringCellValue();
                CollectionType srcOfFundCollType = collTypeRepo.findByDescription(srcOfFundDesc);
                expense.setSourceOfFunds(srcOfFundCollType);

                localeAccount.getProjectBook("Locale Fund").addRegularExpense(expense);
                localeAccountRepo.save(localeAccount);

                // get target collection account and subtract expense
                CollectionAccount collAccount = localeAccount.getAccount(srcOfFundDesc);
                if (collAccount == null) {
                    collAccount = new CollectionAccount(srcOfFundDesc);
                    localeAccount.getAccounts().add(collAccount);
                }
                BigDecimal balance = collAccount.getBalance();
                collAccount.setBalance(balance.subtract(expense.getAmount()));
                localeAccountRepo.save(localeAccount);
            }

        }catch(Exception e){
            logger.info(e.getMessage()+" "+e.getCause());
            throw e;
        } finally {
            try {
                if (myWorkBook != null) {
                    myWorkBook.close();
                }
            } catch (IOException e) {
            }
        }

        return "redirect:/locale_account/" + localeId + "/expense";
    }

}