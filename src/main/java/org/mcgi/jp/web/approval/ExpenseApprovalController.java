package org.mcgi.jp.web.approval;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.enumeration.ExpenseStatus;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.TransactionDateComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ExpenseApprovalController {

    private final Logger logger = LoggerFactory.getLogger(ExpenseApprovalController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/approval/expense", method = RequestMethod.GET)
    public String approveExpenses(Map<String, Object> model) {
        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();

        for (LocaleAccount localeAccount : localeAccounts) {
            Collections.sort(localeAccount.getProjectBook("Locale Fund").getRegularExpenses(), new TransactionDateComparator());
        }

        model.put("localeAccounts", localeAccounts);
        return "approval/expense";
    }

    @RequestMapping(value = "/approval/expense/{localeName}/{yearMonth}", method = RequestMethod.GET)
    public String approveExpensesForMonth(@PathVariable String localeName,
            @PathVariable(value="yearMonth") String monthForApproval,
            Map<String, Object> model) {

        LocaleAccount localeAccount = localeAccountRepo.findByName(localeName);
        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();

        DateFormat df = new SimpleDateFormat("yyyyMM");
        Date tempMonthForApprovalDate = null;
        try {
            tempMonthForApprovalDate = df.parse(monthForApproval);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
        }

        LocalDate monthForApprovalDate = tempMonthForApprovalDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Date firstOfMonth = java.sql.Date.valueOf(monthForApprovalDate.with(TemporalAdjusters.firstDayOfMonth()));
        Date lastOfMonth = java.sql.Date.valueOf(monthForApprovalDate.with(TemporalAdjusters.lastDayOfMonth()));

        expenses.removeIf(s -> s.getTransactionDate().before(firstOfMonth)
                                || s.getTransactionDate().after(lastOfMonth));

        List<LocaleAccount> localeAccounts = new ArrayList<LocaleAccount>();
        localeAccounts.add(localeAccount);
        model.put("localeAccounts", localeAccounts);

        return "approval/expense";
    }

    @RequestMapping(value = "/approval/expense/{localeId}/{expenseId}", method = RequestMethod.POST)
    public @ResponseBody String approveMonth(@PathVariable String localeId,
            @PathVariable(value="expenseId") String expenseId, @RequestParam String status, ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        ExpenseStatus currStatus = null;
        if (status == null || "".equals(status)) {
            currStatus = ExpenseStatus.PENDING;
        } else {
            currStatus = ExpenseStatus.valueOf(status);
        }

        ExpenseStatus newStatus = null;
        switch (currStatus) {
        case PENDING:
            newStatus = ExpenseStatus.APPROVED;
            break;
        case APPROVED:
            newStatus = ExpenseStatus.PENDING;
            break;
        default:
            newStatus = ExpenseStatus.PENDING;
            break;
        }

        for (RegularExpense expense : localeAccount.getProjectBook("Locale Fund").getRegularExpenses()) {
            if (expense.getId().equals(expenseId)) {
                expense.setStatus(newStatus);
            }
        }
        localeAccountRepo.save(localeAccount);

        return "success";
    }

}