package org.mcgi.jp.web.approval;

import java.util.Map;

import org.mcgi.jp.model.LocaleAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ApprovalController {

    private final Logger logger = LoggerFactory.getLogger(ApprovalController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/approval", method = RequestMethod.GET)
    public String index(Map<String, Object> model) {
        return "approval/index";
    }

}