package org.mcgi.jp.web.approval;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mcgi.jp.enumeration.ExpenseStatus;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.TransactionDateComparator;
import org.mcgi.jp.model.report.LiquidationReport;
import org.mcgi.jp.model.report.LiquidationReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LiquidationApprovalController {

    private final Logger logger = LoggerFactory.getLogger(LiquidationApprovalController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private LiquidationReportRepository liqReportRepo;

    @RequestMapping(value = "/approval/liquidation", method = RequestMethod.GET)
    public String approveLiquidation(Map<String, Object> model) {

        /*
         * TODO: Approve liquidation
         * 1) Get all expenses for month
         * 2) All expenses paid?
         * 3) All expenses approved?
         * 4) Enable / display approval to generate liquidation report
         */
        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();

        for (LocaleAccount localeAccount : localeAccounts) {
            Collections.sort(localeAccount.getProjectBook("Locale Fund").getRegularExpenses(), new TransactionDateComparator());
        }

        // Split expenses by Month
        /*
         * Locale[0]       |       Month[1]                   | Status[2]
         * Chiba        |       <link>January</link>    | Approved
         * Tokyo        |       <link>January</link>    | Approved
         * Chiba        |       <link>February</link>   | Approved
         * Tokyo        |       <link>February</link>   | Pending
         * ...
         */
        for (LocaleAccount localeAccount : localeAccounts) {
            Collections.sort(localeAccount.getProjectBook("Locale Fund").getRegularExpenses(), new TransactionDateComparator());
        }

        List<String[]> expenseApprovalsByMonth = new ArrayList<String[]>();
        for (LocaleAccount localeAccount : localeAccounts) {
            List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
            List<String[]> expensesByMonthForLocale = splitExpensesByMonth(localeAccount.getName(), expenses);
            expenseApprovalsByMonth.addAll(expensesByMonthForLocale);
        }

        model.put("liquidationReports", expenseApprovalsByMonth);
        return "approval/liquidation";
    }

    @RequestMapping(value = "/approval/liquidation/{localeName}/{yearMonth}", method = RequestMethod.POST)
    public @ResponseBody String approveMonth(@PathVariable String localeName,
            @PathVariable(value="yearMonth") String monthForApprovalStr, ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findByName(localeName);
        List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();

        DateFormat df = new SimpleDateFormat("yyyyMM");
        Date tempMonthForApprovalDate = null;
        try {
            tempMonthForApprovalDate = df.parse(monthForApprovalStr);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
        }
        LocalDate monthForApprovalDate = tempMonthForApprovalDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate firstOfMonth = monthForApprovalDate.with(TemporalAdjusters.firstDayOfMonth());
        LocalDate lastOfMonth = monthForApprovalDate.with(TemporalAdjusters.lastDayOfMonth());

        for (RegularExpense expense : expenses) {
            Date tempDate = expense.getTransactionDate();
            LocalDate transactionDate = tempDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            if (transactionDate.compareTo(firstOfMonth) >= 0
                    && transactionDate.compareTo(lastOfMonth) <= 0) {
                expense.setStatus(ExpenseStatus.APPROVED);
            }
        }
        localeAccountRepo.save(localeAccount);


        // Generate initial liquidation report
        YearMonth monthForApprovalYM = YearMonth.from(monthForApprovalDate);
        LiquidationReport newLiqReport = new LiquidationReport("ASIA & OCEANIA DIVISION", "DISTRICT 1", localeName,
                monthForApprovalYM.toString());

        // get previous month's ending cash and set as this month's beginning cash on hand
        LiquidationReport prevLiqReport = getPrevLiqReport(localeName, monthForApprovalDate);
        if (prevLiqReport != null) {
            newLiqReport.setBeginningCashOnHand(prevLiqReport.getTotalCashRemaining());
        } else {
            newLiqReport.setBeginningCashOnHand(new BigDecimal(0));
        }

        liqReportRepo.save(newLiqReport);

        return "success";
    }

    private LiquidationReport getPrevLiqReport(String localeName, LocalDate monthForApproval) {

        LocalDate prevMonth = monthForApproval.minusMonths(1);
        YearMonth prevMonthYM = YearMonth.from(prevMonth);

        return liqReportRepo.findByLocaleAndYearMonth(localeName, prevMonthYM.toString());
    }

    private List<String[]> splitExpensesByMonth(String localeName, List<RegularExpense> expenses) {

        List<String[]> monthsForApprovalForLocale = new ArrayList<String[]>();

        List<String> monthsForApproval = getMonthsForApproval(expenses);
        List<String> monthsForApprovalStatus = getMonthsForApprovalStatus(monthsForApproval, expenses);

        for (int i = 0; i < monthsForApproval.size(); i++) {
            String monthForApproval = monthsForApproval.get(i);
            String status = monthsForApprovalStatus.get(i);

            String[] localeMonthStatus = new String[3];
            localeMonthStatus[0] = localeName;
            localeMonthStatus[1] = monthForApproval;
            localeMonthStatus[2] = status;

            monthsForApprovalForLocale.add(localeMonthStatus);
        }

        return monthsForApprovalForLocale;
    }

    private List<String> getMonthsForApprovalStatus(List<String> monthsForApproval, List<RegularExpense> expenses) {

        List<String> statusPerMonth = new ArrayList<String>();
        DateFormat df = new SimpleDateFormat("yyyyMM");
        for (String monthForApproval : monthsForApproval) {

            Date tempMonthForApprovalDate = null;
            try {
                tempMonthForApprovalDate = df.parse(monthForApproval);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
            }
            LocalDate monthForApprovalDate = tempMonthForApprovalDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate firstOfMonth = monthForApprovalDate.with(TemporalAdjusters.firstDayOfMonth());
            LocalDate lastOfMonth = monthForApprovalDate.with(TemporalAdjusters.lastDayOfMonth());

            boolean isAllApproved = false;
            for (RegularExpense expense : expenses) {
                Date tempDate = expense.getTransactionDate();
                LocalDate transactionDate = tempDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                if (transactionDate.compareTo(firstOfMonth) >= 0
                        && transactionDate.compareTo(lastOfMonth) <= 0) {

                    if (expense.getStatus() == ExpenseStatus.APPROVED) {
                        isAllApproved = true;
                    } else {
                        isAllApproved = false;
                        break;
                    }
                }
            }

            if (isAllApproved) {
                statusPerMonth.add(ExpenseStatus.APPROVED.name());
            } else {
                statusPerMonth.add(ExpenseStatus.PENDING.name());
            }
        }

        return statusPerMonth;
    }

    private List<String> getMonthsForApproval(List<RegularExpense> expenses) {

        Set<String> monthsForApprovalSet = new HashSet<String>();
        DateFormat df = new SimpleDateFormat("yyyyMM");
        for (RegularExpense expense : expenses) {
            Date transactionDate = expense.getTransactionDate();
            monthsForApprovalSet.add(df.format(transactionDate));
        }

        List<String> sortedList = new ArrayList<String>(monthsForApprovalSet);
        Collections.sort(sortedList);

        return sortedList;
    }

}