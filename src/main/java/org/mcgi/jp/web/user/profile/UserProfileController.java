package org.mcgi.jp.web.user.profile;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserPreference;
import org.mcgi.jp.model.admin.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserProfileController {

    private final Logger logger = LoggerFactory.getLogger(UserProfileController.class);

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/user/{userId}/profile", method = RequestMethod.GET)
    public String profile(@PathVariable String userId, Map<String, Object> model, Principal principal) {
        JapanFinanceUser user = userRepo.findOne(userId);
        model.put("user", user);

        String localeStr = user.getLocale();
        LocaleAccount locale = localeAccountRepo.findByName(localeStr);

        if (locale == null) {
            locale = localeAccountRepo.findOne(localeStr);
        }
        model.put("locale", locale);

        Map<String, String> dashboardTypes = new LinkedHashMap<String,String>();
        dashboardTypes.put("basic_column", "Basic Column");
        dashboardTypes.put("column_with_table", "Column With Table");
        dashboardTypes.put("project_monitoring", "Project Monitoring");
        model.put("dashboardTypes", dashboardTypes);

        return "user/profile";
    }

    @RequestMapping(value = "/user/{userId}/profile", method = RequestMethod.POST)
    public String udpate(@PathVariable String userId,
            @RequestParam(name="preference.dashboard") String dashboardPreference,
            Map<String, Object> model, Principal principal) {
        JapanFinanceUser user = userRepo.findOne(userId);
        UserPreference preference = user.getPreference();
        preference.setDashboard(dashboardPreference);
        userRepo.save(user);
        model.put("user", user);

        return "redirect:/logout";
    }

}