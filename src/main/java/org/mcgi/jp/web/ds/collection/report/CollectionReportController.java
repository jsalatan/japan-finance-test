package org.mcgi.jp.web.ds.collection.report;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.report.CollectionRecord;
import org.mcgi.jp.model.report.CollectionReport;
import org.mcgi.jp.model.report.CollectionReportRepository;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CollectionReportController {

    private final Logger logger = LoggerFactory.getLogger(CollectionReportController.class);


    @Autowired
    private CollectionReportRepository collReportRepo;

    @Autowired
    private CollectionTypeRepository collTypeRepo;

    @Autowired
    private LocaleAccountRepository localeRepo;


    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/collection_report", method = RequestMethod.GET)
    public String collection(Model model) {
        List<CollectionReport> collectionReports = collReportRepo.findAll();
        model.addAttribute("reports", collectionReports);
        return "ds/collection_report/index";
    }

    @RequestMapping(value = "/collection_report/new", method = RequestMethod.GET)
    public String createReport(Model model) {
        CollectionReport newReport = new CollectionReport();
        newReport.setDateGenerated(new Date());

        // Get user info and set as registered by
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JapanFinanceUser user = ((CustomUserDetails)principal).getUser();
        newReport.setReporter(user.getLastName() + ", " + user.getFirstName());

        List<CollectionRecord> records = createRecords(10);
        newReport.setRecords(records);

        CollectionReport savedReport = collReportRepo.save(newReport);
        return "redirect:/collection_report/" + savedReport.getId() + "/collections";
    }

    @RequestMapping(value = "/collection_report/{reportId}/collections", method = RequestMethod.GET)
    public String newReport(@PathVariable String reportId, Model model) {
        CollectionReport report = collReportRepo.findOne(reportId);
        model.addAttribute("reportForm", report);
        model.addAttribute("record", new CollectionRecord());

        List<CollectionType> collTypes = collTypeRepo.findAll();
        model.addAttribute("collTypes", collTypes);

        List<LocaleAccount> allLocales = localeRepo.findAll();
        model.addAttribute("locals", allLocales);

        return "ds/collection_report/collection/index_and_new";
    }

    private List<CollectionRecord> createRecords(int count) {

        List<CollectionRecord> rows = new ArrayList<CollectionRecord>();
        for (int i = 0; i < count; i++) {
            CollectionRecord record = new CollectionRecord();
            rows.add(record);
        }

        return rows;
    }

}