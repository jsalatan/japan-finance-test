package org.mcgi.jp.web.ds.collection.report;

import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class CollectionsReportCsvUploadController {

    private final Logger logger = LoggerFactory.getLogger(CollectionsReportCsvUploadController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private CollectionTypeRepository collTypeRepo;

    @RequestMapping(value = "/collection_report/csv_upload", method = RequestMethod.POST)
    public String csvUpload(@PathVariable String localeId, @RequestParam MultipartFile file) throws Exception {

        XSSFWorkbook myWorkBook = null;
        try{
            myWorkBook = new XSSFWorkbook (file.getInputStream());
            // Return first sheet from the XLSX workbook
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);
            // Get iterator to all the rows in current sheet
            Iterator<Row> rowIterator = mySheet.iterator();

            LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

            // Traversing over each row of XLSX file
            boolean isFirstRow = true;
            while (rowIterator.hasNext()) {
                // Skip first row (table header)
                if (isFirstRow) {
                    rowIterator.next();
                    isFirstRow = false;
                    continue;
                }

                /* FIXME: Map Uploaded CSV to CollectionRecord
                Row row = rowIterator.next();
                // For each row, iterate through each columns
                RegularCollection collection = new RegularCollection();


                int cellIdx = 0;

                String typeDesc = row.getCell(cellIdx++).getStringCellValue();
                collection.setType(collTypeRepo.findByDescription(typeDesc));

                double amount = row.getCell(cellIdx++).getNumericCellValue();
                collection.setAmount(new BigDecimal(amount));

                String givenBy = row.getCell(cellIdx++).getStringCellValue();
                collection.setGivenBy(givenBy);

                Date collectionDate = row.getCell(cellIdx++).getDateCellValue();
                collection.setTransactionDate(collectionDate);

                ProjectBook projectBook = localeAccount.getProjectBook("Locale Fund");
                projectBook.addCollection(collection);
                localeAccountRepo.save(localeAccount);

                // get target collection account and credit collection
                String typeDescription = collection.getType().getDescription();
                CollectionAccount collAccount = localeAccount.getAccount(typeDescription);
                if (collAccount == null) {
                    collAccount = new CollectionAccount(typeDescription);
                    localeAccount.getAccounts().add(collAccount);
                }
                BigDecimal balance = collAccount.getBalance();
                collAccount.setBalance(balance.add(collection.getAmount()));
                localeAccountRepo.save(localeAccount);
                /* */
            }

        }catch(Exception e){
            logger.info(e.getMessage()+" "+e.getCause());
            throw e;
        } finally {
            try {
                if (myWorkBook != null) {
                    myWorkBook.close();
                }
            } catch (IOException e) {
            }
        }

        return "redirect:/collection_report/";
    }

}