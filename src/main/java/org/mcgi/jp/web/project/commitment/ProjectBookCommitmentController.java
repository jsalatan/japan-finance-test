package org.mcgi.jp.web.project.commitment;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCommitment;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.admin.UsernameComparator;
import org.mcgi.jp.model.project.ProjectBook;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProjectBookCommitmentController {

    private final Logger logger = LoggerFactory.getLogger(ProjectBookCommitmentController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private UserRepository userRepo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/locale_account/{localeId}/project_book/{projectBookId}/commitments", method = RequestMethod.GET)
    public String commitments(
            @PathVariable String localeId,
            @PathVariable String projectBookId,
            Map<String, Object> model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.put("localeAccount", localeAccount);

        ProjectBook targetProjectBook = localeAccount.getProjectBook(projectBookId);
        model.put("projectBook", targetProjectBook);

        return "project/commitment/commitments_index";
    }

    @RequestMapping(
            value = {
                    "/locale_account/{localeId}/commitment/project/{projectBookId}/new",
                    "/locale_account/{localeId}/project/{projectBookId}/commitment/new"
                    },
            method = RequestMethod.GET)
    public String newInstance(
            @PathVariable String localeId,
            @PathVariable String projectBookId,
            Model model) {

        // Set member username automatically as value
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JapanFinanceUser customUser = null;
        if (principal instanceof CustomUserDetails) {
            customUser = ((CustomUserDetails)principal).getUser();
        }
        model.addAttribute("member", customUser);

        List<JapanFinanceUser> users = userRepo.findAll();
        Collections.sort(users, new UsernameComparator());
        model.addAttribute("users", users);

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("localeAccount", localeAccount);

        ProjectBook targetProjectBook = localeAccount.getProjectBook(projectBookId);
        model.addAttribute("projectBook", targetProjectBook);

        RegularCommitment commitment = new RegularCommitment();
        model.addAttribute("commitment", commitment);
        return "project/commitment/new";

    }

    @RequestMapping(
            value = {
                    "/locale_account/{localeId}/commitment/project/{projectBookId}/new",
                    "/locale_account/{localeId}/project/{projectBookId}/commitment/new"
                    },
            method = RequestMethod.POST)
    public String register(
            @PathVariable String localeId,
            @PathVariable String projectBookId,
            @RequestParam(name="committedBy") String memberId,
            @RequestParam BigDecimal amount,
            @RequestParam Date commitmentDate,
            ModelMap model) {

        RegularCommitment newCommitment = new RegularCommitment();
        newCommitment.setId(new ObjectId());
        newCommitment.setCommittedBy(memberId);
        newCommitment.setAmount(amount);
        newCommitment.setCommitmentDate(commitmentDate);

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        ProjectBook targetProjectBook = localeAccount.getProjectBook(projectBookId);
        targetProjectBook.getRegularCommitments().add(newCommitment);

        localeAccountRepo.save(localeAccount);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        boolean isCoordinator = authorities.contains(new SimpleGrantedAuthority("ROLE_LOCALE_COORDINATOR"));

        String redirect = "";
        if (isCoordinator) {
            redirect = "redirect:/locale_account/" + localeId + "/project_book";
        } else {
            redirect = "redirect:/locale_account/" + localeId + "/commitment/project";
        }
        return redirect;
    }

    @RequestMapping(
            value = {
                    "/locale_account/{localeId}/commitment/project/{projectBookId}/{commitmentId}/edit",
                    "/locale_account/{localeId}/project/{projectBookId}/commitment/{commitmentId}/edit"
                    },
            method = RequestMethod.POST)
    public String update(
            @PathVariable String localeId,
            @PathVariable String projectBookId,
            @PathVariable String commitmentId,
            @RequestParam(name="committedBy") String memberId,
            @RequestParam BigDecimal amount,
            @RequestParam Date commitmentDate,
            ModelMap model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        ProjectBook targetProjectBook = localeAccount.getProjectBook(projectBookId);

        List<RegularCommitment> regularCommitments = targetProjectBook.getRegularCommitments();
        RegularCommitment targetCommitment = regularCommitments.stream().filter(x -> x.getId().equals(commitmentId))
                .findAny().orElse(null);
        targetCommitment.setCommittedBy(memberId);
        targetCommitment.setAmount(amount);
        targetCommitment.setCommitmentDate(commitmentDate);

        localeAccountRepo.save(localeAccount);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        boolean isCoordinator = authorities.contains(new SimpleGrantedAuthority("ROLE_LOCALE_COORDINATOR"));

        String redirect = "";
        if (isCoordinator) {
            redirect = "redirect:/locale_account/" + localeId + "/project_book";
        } else {
            redirect = "redirect:/locale_account/" + localeId + "/commitment/project";
        }
        return redirect;
    }

}