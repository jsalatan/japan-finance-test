package org.mcgi.jp.web.project.commitment;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.Commitment;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCommitment;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.project.ProjectBook;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProjectCommitmentController {

    private final Logger logger = LoggerFactory.getLogger(ProjectCommitmentController.class);

    @Autowired
    private LocaleAccountRepository localeRepo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/locale_account/{localeId}/commitment/project", method = RequestMethod.GET)
    public String index(@PathVariable String localeId, Map<String, Object> model) {
        LocaleAccount localeAccount = localeRepo.findOne(localeId);
        model.put("localeAccount", localeAccount);

        List<ProjectBook> projectBooks = localeAccount.getProjectBooks();
        model.put("projectBooks", projectBooks);

        return "project/commitment/project_index";
    }

}