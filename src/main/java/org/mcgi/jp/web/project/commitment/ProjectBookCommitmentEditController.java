package org.mcgi.jp.web.project.commitment;

import java.util.List;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCommitment;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.project.ProjectBook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProjectBookCommitmentEditController {

    private final Logger logger = LoggerFactory.getLogger(ProjectBookCommitmentEditController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private UserRepository userRepo;

    @RequestMapping(
            value = {
                    "/locale_account/{localeId}/commitment/project/{projectBookId}/{commitmentId}/edit",
                    "/locale_account/{localeId}/project/{projectBookId}/commitment/{commitmentId}/edit"
                    },
            method = RequestMethod.GET)
    public String edit(
            @PathVariable String localeId,
            @PathVariable String projectBookId,
            @PathVariable String commitmentId,
            Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("localeAccount", localeAccount);

        // Set member username automatically as value
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JapanFinanceUser customUser = null;
        if (principal instanceof CustomUserDetails) {
            customUser = ((CustomUserDetails)principal).getUser();
        }
        model.addAttribute("member", customUser);

        List<JapanFinanceUser> users = userRepo.findAll();
        model.addAttribute("users", users);

        ProjectBook targetProjectBook = localeAccount.getProjectBook(projectBookId);
        model.addAttribute("projectBook", targetProjectBook);

        List<RegularCommitment> regularCommitments = targetProjectBook.getRegularCommitments();
        RegularCommitment targetCommitment = regularCommitments.stream().filter(x -> x.getId().equals(commitmentId))
                .findAny().orElse(null);
        model.addAttribute("commitment", targetCommitment);

        return "project/commitment/edit";

    }

}