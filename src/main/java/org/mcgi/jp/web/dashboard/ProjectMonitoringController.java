package org.mcgi.jp.web.dashboard;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
public class ProjectMonitoringController {

    private final Logger logger = LoggerFactory.getLogger(ProjectMonitoringController.class);

    enum TransactionMonth {
        PREV_MONTH, CURRENT_MONTH, NEXT_MONTH
    }

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/dashboard/{localeId}/project_monitoring", method = { RequestMethod.GET, RequestMethod.POST })
    public String chart(@PathVariable String localeId, Model model) throws JsonProcessingException {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        List<RegularCollection> allCollections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JapanFinanceUser user = null;
        if (principal instanceof CustomUserDetails) {
            user = ((CustomUserDetails)principal).getUser();
        }

        final String userId = user.getUsername();
        if (user != null) {
            List<RegularCollection> filteredCollections = allCollections.stream()
                    .filter(x -> x.getGivenBy().equals(userId)).collect(Collectors.toList());
            model.addAttribute("contributions", filteredCollections);

            BigDecimal totalContribution = BigDecimal.ZERO;
            for (RegularCollection regularCollection : filteredCollections) {
                totalContribution = totalContribution.add(regularCollection.getAmount());
            }
            model.addAttribute("total", totalContribution);
        }
        return "dashboard/contribution";
    }

}

