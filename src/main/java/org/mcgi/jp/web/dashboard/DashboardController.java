package org.mcgi.jp.web.dashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserPreference;
import org.mcgi.jp.web.util.HighchartsHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class DashboardController {

    private final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    private static final List<String> PREFERRED_DASHBOARD_NEEDS_LOCALE_ID = Arrays.asList(
            new String[] {
                    "contribution"
                    ,"basic_column"
                    ,"column_with_table"
                    ,"project_monitoring"
                });

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String dashboard(Map<String, Object> model,
            SecurityContextHolderAwareRequestWrapper securityRequest) {
        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();

        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (LocaleAccount localeAccount : localeAccounts) {
            Map<String, Object> object = new HashMap<String, Object>();
            object.put("name", localeAccount.getName());
            object.put("y", 50);
            mapList.add(object);
        }
        model.put("localeAccounts", mapList);

        String dashboardViewOrRedirect = null;
        /*
         * Dashboard priority:
         * 1) Fixed (i.e., Admin)
         * 2) User preference ( "redirect:/${userpreference}" )
         * 3) Default (when blank)
         */
        if (securityRequest.isUserInRole("ROLE_ADMIN")) {
            dashboardViewOrRedirect = "redirect:/admin";

        } else if (securityRequest.isUserInRole("ROLE_DISTRICT_SERVANT")) {
            dashboardViewOrRedirect = "ds/collection_report/index";

        } else {
            /*
             * Steps:
             * 0. Check if user is assigned to a locale or not.
             * 1. Get user preference
             * 2. Check if preferred dashboard needs locale id
             * 3. Generate appropriate redirect URL
             * 4. If null, return default
             */
            LocaleAccount locale = getUserLocale();

            /*
             * If expecting locale id, but user is not yet assigned to any locale,
             * Then redirect it to default dashboard index.
             */
            if (locale == null) {
                return "dashboard/index";

            } else {
                String preferredDashboard = getUserPreferredDashboard();

                /*
                 *  If preferred dashboard not set, but user is already assigned to a locale,
                 *    then ithould redirect to member's contribution dashboard.
                 */
                if (preferredDashboard == null) {
                    preferredDashboard = "contribution";
                }

                // If preferred dashboard is in the list of dashboards that needs a locale id, generate redirect URL
                if (PREFERRED_DASHBOARD_NEEDS_LOCALE_ID.contains(preferredDashboard)) {
                    // Generate redirect URL: "redirect:dashboard/" + ${locale_id} + "/" + ${preferred_dashboard};
                    model.put("locale", locale);
                    dashboardViewOrRedirect = "redirect:dashboard/" + locale.getId() + "/" + preferredDashboard;

                /*
                 *  Else (locale id not needed), but has preferred dashboard set, then append it to URL redirect.
                 *    i.e.,  "redirect:dasbhoard/" + ${preferred_dashboard}
                 */
                } else {
                        dashboardViewOrRedirect = "redirect:dashboard/" + preferredDashboard;
                }
            }

        }

        return dashboardViewOrRedirect;
    }

    private LocaleAccount getUserLocale() {
        LocaleAccount locale = null;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
            String localeStr = customUser.getLocale();

            if (localeStr != null && !"".equals(localeStr)) {
                locale = localeAccountRepo.findByName(localeStr);

                if (locale == null) {
                    locale = localeAccountRepo.findOne(localeStr);
                }
            }
        }

        return locale;
    }

    private String getUserPreferredDashboard() {
        String preferredDashboard = null;

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser user = ((CustomUserDetails)principal).getUser();
            UserPreference userPreference = user.getPreference();
            if (userPreference != null) {
                preferredDashboard = userPreference.getDashboard();
            }
        }

        return preferredDashboard;
    }


    private List<String> getXCategories() {
        List<String> labels = new ArrayList<String>();

        Calendar prevMonth = Calendar.getInstance();
        prevMonth.add(Calendar.MONTH, -1);
        labels.add(prevMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));

        Calendar currMonth = Calendar.getInstance();
        labels.add(currMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));

        Calendar nextMonth = Calendar.getInstance();
        nextMonth.add(Calendar.MONTH, 1);
        labels.add(nextMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));

        return labels;
    }

    @RequestMapping(value = "/dashboard/get_drilldown_data_async", method = RequestMethod.GET)
    public @ResponseBody String getDrilldownAsync() throws JsonProcessingException {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        Map<String, Map<String,Object>> pieDrilldownData = HighchartsHelper.getPieDrilldownDataForAsync(localeAccounts);

        return new ObjectMapper().writeValueAsString(pieDrilldownData);
    }

}