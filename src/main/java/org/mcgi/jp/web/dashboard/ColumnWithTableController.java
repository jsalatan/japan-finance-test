package org.mcgi.jp.web.dashboard;

import java.math.BigDecimal;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.Transaction;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
public class ColumnWithTableController {

    private final Logger logger = LoggerFactory.getLogger(ColumnWithTableController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/dashboard/{localeId}/column_with_table", method = { RequestMethod.GET, RequestMethod.POST })
    public String chart(@PathVariable String localeId,
            @RequestParam(required=false) Date startDate,
            @RequestParam(required=false) Date endDate,
            @RequestParam(required=false) Integer numOfMonths,
            Principal principal,
            Model model) throws JsonProcessingException {

        LocaleAccount userLocale = null;
        if ("".equals(localeId)) {
            if (principal instanceof CustomUserDetails) {
                JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
                String localeStr = customUser.getLocale();
                userLocale = localeAccountRepo.findByName(localeStr);

                if (userLocale == null) {
                    userLocale = localeAccountRepo.findOne(localeStr);
                }
            }
        } else {
            userLocale = localeAccountRepo.findOne(localeId);
        }
        model.addAttribute("locale", userLocale);

        // Example data
        /*          |   Collection  | Expenses
         * Jan 2016 |   180,000     | 120,000
         * Feb 2016 |   190,000     | 130,000
         * Mar 2016 |   170,000     | 250,000
         */
        List<String> tblHeader = Arrays.asList(new String[] { "Collection", "Expenses"});
        model.addAttribute("columnHeaders", tblHeader);

        // Populate months (start date & months)
        List<Date> rowMonths = populateMonths(startDate, numOfMonths);

        List<RegularCollection> collections = userLocale.getProjectBook("Locale Fund").getRegularCollections();
        List<RegularExpense> expenses = userLocale.getProjectBook("Locale Fund").getRegularExpenses();
        List<ColumnWithTableBean> monthlyBeans = separateByMonth(collections, expenses, rowMonths);

        model.addAttribute("monthlyBeans", monthlyBeans);
        model.addAttribute("dashboardForm", new DashboardForm());

        return "dashboard/column_with_table";
    }

    private List<ColumnWithTableBean> separateByMonth(List<RegularCollection> collections, List<RegularExpense> expenses,
            List<Date> rowMonths) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        List<ColumnWithTableBean> result = new ArrayList<ColumnWithTableBean>();
        for (Date rowMonth : rowMonths) {
            ColumnWithTableBean bean = new ColumnWithTableBean();

            List<Transaction> collectionsByMonth = separateByMonth(collections, rowMonth);
            BigDecimal totalCollection = computeTotal(collectionsByMonth);

            List<Transaction> expensesByMonth = separateByMonth(expenses, rowMonth);
            BigDecimal totalExpense = computeTotal(expensesByMonth);

            bean.setDescription(sdf.format(rowMonth));
            bean.setCollectionAmt(totalCollection);
            bean.setExpenseAmt(totalExpense);
            result.add(bean);
        }

        return result;
    }

    private BigDecimal computeTotal(List<? extends Transaction> collectionsByMonth) {
        BigDecimal total = BigDecimal.ZERO;


        for (Transaction transaction : collectionsByMonth) {
            total = total.add(transaction.getAmount());
        }

        return total;
    }

    private List<Transaction> separateByMonth(List<? extends Transaction> transactions, Date startDate) {
        Date[] dateRange = getTargetRange(startDate);

        List<Transaction> transactionsByMonth = new ArrayList<Transaction>();
        for (Transaction transaction : transactions) {
            Date date = transaction.getTransactionDate();
            if (date == null) {
                continue;
            }
            if (date.compareTo(dateRange[0]) >= 0
                    && date.compareTo(dateRange[1]) <= 0) {
                transactionsByMonth.add(transaction);
            }
        }

        return transactionsByMonth;
    }

    private List<Date> populateMonths(Date startDate, Integer numOfMonths) {

        if (numOfMonths == null) {
            numOfMonths = new Integer(3);
        }

        List<Date> rowLabels = new ArrayList<Date>();

        for (int i = 0; i < numOfMonths; i++) {
            Calendar aCalendar = Calendar.getInstance();
            if (startDate != null) {
                aCalendar.setTime(startDate);
            }

            // set DATE to 1, so first date of month
            aCalendar.add(Calendar.MONTH, i);
            aCalendar.set(Calendar.DATE, 1);
            aCalendar.set(Calendar.HOUR_OF_DAY, 0);
            aCalendar.set(Calendar.MINUTE, 0);
            aCalendar.set(Calendar.SECOND, 0);
            aCalendar.set(Calendar.MILLISECOND, 0);
            rowLabels.add(aCalendar.getTime());
        }

        return rowLabels;
    }

    private Date[] getTargetRange(Date startDate) {
        Date[] dateRange = new Date[2];

        //   get date today
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.setTime(startDate);

        // set DATE to 1, so first date of month
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.set(Calendar.HOUR_OF_DAY, 0);
        aCalendar.set(Calendar.MINUTE, 0);
        aCalendar.set(Calendar.SECOND, 0);
        aCalendar.set(Calendar.MILLISECOND, 0);
        dateRange[0] = aCalendar.getTime();

        // set maximum date of month
        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        dateRange[1] = aCalendar.getTime();

        return dateRange;
    }

}

