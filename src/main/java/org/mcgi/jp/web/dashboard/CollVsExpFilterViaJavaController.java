package org.mcgi.jp.web.dashboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.Transaction;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class CollVsExpFilterViaJavaController {

    private final Logger logger = LoggerFactory.getLogger(CollVsExpFilterViaJavaController.class);

    enum TransactionMonth {
        PREV_MONTH, CURRENT_MONTH, NEXT_MONTH
    }

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/dashboard/{localeId}/collection_vs_expense/get_series_data", method = RequestMethod.GET)
    public @ResponseBody String localeIndex(@PathVariable String localeId,
            @RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date startDate,
            @RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date endDate) throws JsonProcessingException {

        /*
         * STEPS:
         * 1. Get User locale
         * 2. Get collections
         * 3. Divide/split depending on date
         *   a. Previous month
         *   b. Current month
         * 4. Get expenses
         * 5. Divide/split depending on date
         *   a. Previous month
         *   b. Current month
         * 6. Return response
         */

        // Generate Collections vs Expense from DB
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        LocaleAccount userLocale = null;
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
            String localeStr = customUser.getLocale();
            userLocale = localeAccountRepo.findOne(localeStr);

            if (userLocale == null) {
                userLocale = localeAccountRepo.findByName(localeStr);
            }
        }

        // Final Highcharts data
        List<Map<String, Object>> seriesData = new ArrayList<Map<String, Object>>();

        // Collections
        List<RegularCollection> collections = userLocale.getProjectBook("Locale Fund").getRegularCollections();
        List<Map<String, Object>> collectionsForChart = separateForChart(collections, "collection");
        seriesData.addAll(collectionsForChart);

        // Expenses
        List<RegularExpense> expenses = userLocale.getProjectBook("Locale Fund").getRegularExpenses();
        List<Map<String, Object>> expensesForChart = separateForChart(expenses, "expense");
        seriesData.addAll(expensesForChart);

        Map<String, Object> response = new HashMap<String, Object>();
        response.put("data", seriesData);

        return new ObjectMapper().writeValueAsString(response);
    }

    private List<Map<String, Object>> separateForChart(List<? extends Transaction> transactions, String transType) {
        // Separate collections by month
        List<Transaction> prevMonthColls = separateByMonth(transactions, TransactionMonth.PREV_MONTH);
        List<Transaction> currMonthColls = separateByMonth(transactions, TransactionMonth.CURRENT_MONTH);

        // Separate collections by type
        Map<String, List<Transaction>> collectionsByTypePrevMonth = separateByType(prevMonthColls);
        Map<String, List<Transaction>> collectionsByTypeCurrMonth = separateByType(currMonthColls);

        Map<String, BigDecimal[]> chartDataByType = new HashMap<String, BigDecimal[]>();
        for (String collType : collectionsByTypePrevMonth.keySet()) {
            List<Transaction> collectionsByType = collectionsByTypePrevMonth.get(collType);
            for (Transaction collection : collectionsByType) {
                BigDecimal[] chartData = chartDataByType.get(collType);
                if (chartData == null) {
                    chartData = new BigDecimal[2];
                    chartData[0] = new BigDecimal(0);
                    chartData[1] = new BigDecimal(0);
                    chartDataByType.put(collType, chartData);
                }
                chartData[0] = chartData[0].add(collection.getAmount());
            }
        }

        for (String collType : collectionsByTypeCurrMonth.keySet()) {
            List<Transaction> collectionsByType = collectionsByTypeCurrMonth.get(collType);
            for (Transaction collection : collectionsByType) {
                BigDecimal[] chartData = chartDataByType.get(collType);
                if (chartData == null) {
                    chartData = new BigDecimal[2];
                    chartData[0] = new BigDecimal(0);
                    chartData[1] = new BigDecimal(0);
                    chartDataByType.put(collType, chartData);
                }
                chartData[1] = chartData[1].add(collection.getAmount());
            }
        }

        List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();

        for (String collType : chartDataByType.keySet()) {
            Map<String, Object> seriesDatum = new HashMap<String, Object>();
            seriesDatum.put("name", collType);
            seriesDatum.put("data", chartDataByType.get(collType));
            seriesDatum.put("stack", transType);
            returnList.add(seriesDatum);
        }

        return returnList;
    }

    private List<Transaction> separateByMonth(List<? extends Transaction> transactions, TransactionMonth targetMonth) {
        Date[] dateRange = getTargetRange(targetMonth);

        List<Transaction> transactionsByMonth = new ArrayList<Transaction>();
        for (Transaction transaction : transactions) {
            Date date = transaction.getTransactionDate();
            if (date.compareTo(dateRange[0]) >= 0
                    && date.compareTo(dateRange[1]) <= 0) {
                transactionsByMonth.add(transaction);
            }
        }

        return transactionsByMonth;
    }

    private Date[] getTargetRange(TransactionMonth targetMonth) {
        Date[] dateRange = new Date[2];

        //   get date today
        Calendar aCalendar = Calendar.getInstance();

        switch (targetMonth) {
            case PREV_MONTH:
                // add -1 month
                aCalendar.add(Calendar.MONTH, -1);
                break;
            case NEXT_MONTH:
                // add 1 month
                aCalendar.add(Calendar.MONTH, -1);
                break;
            case CURRENT_MONTH:
                // do nothing
                break;
            default:
                // do nothing
                break;
        }

        // set DATE to 1, so first date of month
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.set(Calendar.HOUR_OF_DAY, 0);
        aCalendar.set(Calendar.MINUTE, 0);
        aCalendar.set(Calendar.SECOND, 0);
        aCalendar.set(Calendar.MILLISECOND, 0);
        dateRange[0] = aCalendar.getTime();

        // set maximum date of month
        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        dateRange[1] = aCalendar.getTime();

        return dateRange;
    }

    private Map<String, List<Transaction>> separateByType(List<? extends Transaction> transactions) {

        Map<String, List<Transaction>> transactionsByType = new HashMap<String, List<Transaction>>();
        for (Transaction transaction : transactions) {
            String key = null;
            if (transaction instanceof RegularCollection) {
                key = ((RegularCollection) transaction).getType().getDescription();
            } else {
                key = ((RegularExpense) transaction).getExpenseType().getDescription();
            }

            List<Transaction> list = transactionsByType.get(key);

            if (list == null) {
                list = new ArrayList<Transaction>();
                list.add(transaction);
                transactionsByType.put(key, list);
            } else {
                list.add(transaction);
            }
        }

        return transactionsByType;
    }

}

