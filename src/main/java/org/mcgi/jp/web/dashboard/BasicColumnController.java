package org.mcgi.jp.web.dashboard;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.Transaction;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class BasicColumnController {

    private final Logger logger = LoggerFactory.getLogger(BasicColumnController.class);

    enum TransactionMonth {
        PREV_MONTH, CURRENT_MONTH, NEXT_MONTH
    }

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/dashboard/{localeId}/basic_column", method = { RequestMethod.GET, RequestMethod.POST })
    public String chart(@PathVariable String localeId, Model model) throws JsonProcessingException {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        model.addAttribute("months", getXCategories());
        model.addAttribute("dashboardForm", new DashboardForm());
        return "dashboard/basic_column";
    }

    private List<String> getXCategories() {
        List<String> labels = new ArrayList<String>();

        Calendar prevMonth = Calendar.getInstance();
        prevMonth.add(Calendar.MONTH, -1);
        labels.add(prevMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));

        Calendar currMonth = Calendar.getInstance();
        labels.add(currMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));

        Calendar nextMonth = Calendar.getInstance();
        nextMonth.add(Calendar.MONTH, 1);
        labels.add(nextMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));

        return labels;
    }

    @RequestMapping(value = "/dashboard/{localeId}/basic_column/get_series_data", method = RequestMethod.GET)
    public @ResponseBody String localeIndex(@PathVariable String localeId,
            @RequestParam(required=false) Date startDate,
            @RequestParam(required=false) Date endDate) throws JsonProcessingException {

        // Generate Collections vs Expense from DB
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        LocaleAccount userLocale = null;
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
            String localeStr = customUser.getLocale();
            userLocale = localeAccountRepo.findOne(localeStr);

            if (userLocale == null) {
                userLocale = localeAccountRepo.findByName(localeStr);
            }
        }

        // Final Highcharts data
        List<Map<String, Object>> seriesData = new ArrayList<Map<String, Object>>();

        // Target
        Map<String, Object> seriesDatumTrgt = new HashMap<String, Object>();
        seriesDatumTrgt.put("name", "Target");
        seriesDatumTrgt.put("data", new Integer[] {200000, 200000, 200000});
        seriesData.add(seriesDatumTrgt);

        // Collections
        List<RegularCollection> collections = userLocale.getProjectBook("Locale Fund").getRegularCollections();
        Map<String, Object> seriesDatumColl = separateForChart(collections, "Collection", startDate);
        seriesData.add(seriesDatumColl);

        // Expenses
        List<RegularExpense> expenses = userLocale.getProjectBook("Locale Fund").getRegularExpenses();
        Map<String, Object> seriesDatumExp = separateForChart(expenses, "Expense", startDate);
        seriesData.add(seriesDatumExp);

        Map<String, Object> response = new HashMap<String, Object>();
        response.put("content", seriesData);

        return new ObjectMapper().writeValueAsString(response);
    }

    private Map<String, Object> separateForChart(List<? extends Transaction> transactions, String transType,
            Date startDate) {
        // Separate collections by month
        List<Transaction> firstMonthTrans = separateByMonth(transactions, TransactionMonth.PREV_MONTH, startDate);
        List<Transaction> secondMonthTrans = separateByMonth(transactions, TransactionMonth.CURRENT_MONTH, startDate);
        List<Transaction> thirdMonthTrans = separateByMonth(transactions, TransactionMonth.NEXT_MONTH, startDate);

        BigDecimal prevMonthTotal = BigDecimal.ZERO;
        for (Transaction transaction : firstMonthTrans) {
            prevMonthTotal = prevMonthTotal.add(transaction.getAmount());
        }

        BigDecimal currMonthTotal = BigDecimal.ZERO;
        for (Transaction transaction : secondMonthTrans) {
            currMonthTotal = currMonthTotal.add(transaction.getAmount());
        }

        BigDecimal nextMonthTotal = BigDecimal.ZERO;
        for (Transaction transaction : thirdMonthTrans) {
            nextMonthTotal = nextMonthTotal.add(transaction.getAmount());
        }

        Map<String, Object> seriesDatum = new HashMap<String, Object>();
        seriesDatum.put("name", transType);
        seriesDatum.put("data", new Integer[] {prevMonthTotal.intValue(), currMonthTotal.intValue(), nextMonthTotal.intValue()});

        return seriesDatum;
    }

    private List<Transaction> separateByMonth(List<? extends Transaction> transactions, TransactionMonth targetMonth,
            Date startDate) {
        Date[] dateRange = null;
        if (startDate == null) {
            dateRange = getTargetRange(targetMonth);
        } else {
            dateRange = getTargetRange(targetMonth, startDate);
        }

        List<Transaction> transactionsByMonth = new ArrayList<Transaction>();
        for (Transaction transaction : transactions) {
            Date date = transaction.getTransactionDate();
            if (date.compareTo(dateRange[0]) >= 0
                    && date.compareTo(dateRange[1]) <= 0) {
                transactionsByMonth.add(transaction);
            }
        }

        return transactionsByMonth;
    }

    private Date[] getTargetRange(TransactionMonth targetMonth) {
        return getTargetRange(targetMonth, null);
    }

    private Date[] getTargetRange(TransactionMonth targetMonth, Date startDate) {
        Date[] dateRange = new Date[2];

        //   get date today
        Calendar aCalendar = Calendar.getInstance();
        if (startDate != null) {
            aCalendar.setTime(startDate);
        }

        switch (targetMonth) {
            case PREV_MONTH:
                // add -1 month
                aCalendar.add(Calendar.MONTH, -1);
                break;
            case NEXT_MONTH:
                // add 1 month
                aCalendar.add(Calendar.MONTH, 1);
                break;
            case CURRENT_MONTH:
                // do nothing
                break;
            default:
                // do nothing
                break;
        }

        // set DATE to 1, so first date of month
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.set(Calendar.HOUR_OF_DAY, 0);
        aCalendar.set(Calendar.MINUTE, 0);
        aCalendar.set(Calendar.SECOND, 0);
        aCalendar.set(Calendar.MILLISECOND, 0);
        dateRange[0] = aCalendar.getTime();

        // set maximum date of month
        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        dateRange[1] = aCalendar.getTime();

        return dateRange;
    }

    @RequestMapping(value = "/dashboard/get_months", method = RequestMethod.GET)
    public @ResponseBody String getMonths(
            @RequestParam(required=false) Date startDate) throws JsonProcessingException {

        List<String> labels = new ArrayList<String>();

        Calendar prevMonth = Calendar.getInstance();
        if (startDate != null) {
            prevMonth.setTime(startDate);
        }
        prevMonth.add(Calendar.MONTH, -1);
        labels.add(prevMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US)
                + "/" + prevMonth.get(Calendar.YEAR));

        Calendar currMonth = Calendar.getInstance();
        if (startDate != null) {
            currMonth.setTime(startDate);
        }
        labels.add(currMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US)
                + "/" + currMonth.get(Calendar.YEAR));

        Calendar nextMonth = Calendar.getInstance();
        if (startDate != null) {
            nextMonth.setTime(startDate);
        }
        nextMonth.add(Calendar.MONTH, 1);
        labels.add(nextMonth.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US)
                + "/" + nextMonth.get(Calendar.YEAR));

        Map<String, Object> response = new HashMap<String, Object>();
        response.put("months", labels);

        return new ObjectMapper().writeValueAsString(response);

    }

}

