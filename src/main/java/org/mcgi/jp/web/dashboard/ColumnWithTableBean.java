package org.mcgi.jp.web.dashboard;

import java.math.BigDecimal;

public class ColumnWithTableBean {
    private String description;
    private BigDecimal collectionAmt;
    private BigDecimal expenseAmt;

    public BigDecimal getCollectionAmt() {
        return collectionAmt;
    }
    public void setCollectionAmt(BigDecimal collectionAmt) {
        this.collectionAmt = collectionAmt;
    }
    public BigDecimal getExpenseAmt() {
        return expenseAmt;
    }
    public void setExpenseAmt(BigDecimal expenseAmt) {
        this.expenseAmt = expenseAmt;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

}