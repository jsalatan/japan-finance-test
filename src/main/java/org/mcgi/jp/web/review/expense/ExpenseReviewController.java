package org.mcgi.jp.web.review.expense;

import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ExpenseReviewController {

    private final Logger logger = LoggerFactory.getLogger(ExpenseReviewController.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    /**
     * List all expenses to be reviewed.
     * @param localeId
     * @param model
     * @return
     */
    @RequestMapping(value = "/review/expense", method = RequestMethod.GET)
    public String localeIndex(Map<String, Object> model) {
        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);
        return "review/expense/locale_index";
    }


    @RequestMapping(value = "/review/expense/{localeId}", method = RequestMethod.GET)
    public String listExpense(@PathVariable String localeId, Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);
        model.addAttribute("expenses", localeAccount.getProjectBook("Locale Fund").getRegularExpenses());
        return "review/expense/index";
    }
}