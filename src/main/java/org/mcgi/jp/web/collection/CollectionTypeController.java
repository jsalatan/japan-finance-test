package org.mcgi.jp.web.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CollectionTypeController {

    private final Logger logger = LoggerFactory.getLogger(CollectionTypeController.class);

    @Autowired
    private CollectionTypeRepository repo;

    @RequestMapping(value = {"/collection_type", "/collection_type/index" }, method = RequestMethod.GET)
    public String index(Map<String, Object> model) {
        List<CollectionType> all = repo.findAll();
        model.put("collectionTypes", all);

        return "collection_type/index";
    }

    @RequestMapping(value = "/collection_type/new", method = RequestMethod.GET)
    public String newExpense(Model model) {

        CollectionType source = new CollectionType("");
        model.addAttribute("source", source);

        List<String> scopes = new ArrayList<String>();
        scopes.add("Locale");
        scopes.add("National");
        scopes.add("District");
        scopes.add("Division");
        model.addAttribute("scopes", scopes);

        return "collection_type/new";

    }

    @RequestMapping(value = "/collection_type/new", method = RequestMethod.POST)
    public String registerExpense(@ModelAttribute("SpringWeb") CollectionType source, ModelMap model) {
        repo.insert(source);
        return "redirect:/collection_type";
    }

}