package org.mcgi.jp.web.collection;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.TransactionDateComparator;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.collection.CollectionAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CollectionsIFController {

    private final Logger logger = LoggerFactory.getLogger(CollectionsIFController.class);

    @Autowired
    private CollectionTypeRepository collTypeRepo;

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/collection", method = RequestMethod.GET)
    public String index(Map<String, Object> model) {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        if (localeAccounts == null || localeAccounts.isEmpty()) {
            model.put("msg", "No locales regisered.");
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String url = "collection/locale_index";

        LocaleAccount locale = null;
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
            String localeStr = customUser.getLocale();
            locale = localeAccountRepo.findByName(localeStr);

            if (locale == null) {
                locale = localeAccountRepo.findOne(localeStr);
            }

            if (locale != null) {
                url = "redirect:/locale_account/" + locale.getId() + "/collection";
            }
        }

        return url;
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/new_row", method = RequestMethod.GET)
    public String newRow(@PathVariable String localeId, Model model) {
        RegularCollection collection = new RegularCollection();
        model.addAttribute("collection", collection);

        List<CollectionType> collTypes = collTypeRepo.findAll();
        Map<String, String> collTypeMap = new HashMap<String, String>();
        for (CollectionType collType : collTypes) {
            collTypeMap.put(collType.getDescription(), collType.getDescription());
        }
        model.addAttribute("collTypes", collTypeMap);

        return "collection/new_row";
    }

    // IMPORTANT: http://www.baeldung.com/cascading-with-dbref-and-lifecycle-events-in-spring-data-mongodb
    @RequestMapping(value = "/locale_account/{localeId}/collection/submit_collection", method = RequestMethod.POST)
    public @ResponseBody String insert(@PathVariable String localeId,
            @RequestParam(name="type") String typeDescription,
            @RequestParam BigDecimal amount,
            @RequestParam String givenBy,
            @RequestParam(name="transactionDate") Date collectionDate, Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        /*
         * TODO: When we add a collection to a locale account, it should trigger a creation of a transaction
         *   and add to a CollectionAccount
         */
        // create new collection and save to collections table
        CollectionType collType = collTypeRepo.findByDescription(typeDescription);
        RegularCollection newCollection = new RegularCollection();
        newCollection.setId(new ObjectId());
        newCollection.setType(collType);
        newCollection.setAmount(amount);
        newCollection.setGivenBy(givenBy);
        newCollection.setTransactionDate(collectionDate);

        // add to locale account's collections and save locale account
        List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
        collections.add(newCollection);
        localeAccountRepo.save(localeAccount);

        // get target collection account and credit collection
        CollectionAccount collAccount = localeAccount.getAccount(typeDescription);
        if (collAccount == null) {
            collAccount = new CollectionAccount(typeDescription);
            localeAccount.getAccounts().add(collAccount);
            localeAccountRepo.save(localeAccount);
        }
        BigDecimal balance = collAccount.getBalance();
        collAccount.setBalance(balance.add(newCollection.getAmount()));

        localeAccountRepo.save(localeAccount);
        return "success";

    }

}