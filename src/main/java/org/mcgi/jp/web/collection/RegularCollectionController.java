package org.mcgi.jp.web.collection;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.CollectionType;
import org.mcgi.jp.model.CollectionTypeRepository;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.ProjectCollection;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.TransactionDateComparator;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.collection.CollectionAccount;
import org.mcgi.jp.model.project.Project;
import org.mcgi.jp.model.project.ProjectRepository;
import org.mcgi.jp.web.CsvFileUploadForm;
import org.mcgi.jp.web.DateFilterForm;
import org.mcgi.jp.web.MultiDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RegularCollectionController {

    private final Logger logger = LoggerFactory.getLogger(RegularCollectionController.class);

    @Autowired
    private ProjectRepository projectRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private CollectionTypeRepository collTypeRepo;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // true passed to CustomDateEditor constructor means convert empty String to null
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new MultiDateFormat(), true));
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection", method = RequestMethod.GET)
    public String collection(@PathVariable String localeId,
            @RequestParam(required=false, name="type") String typeDescFilter,
            @RequestParam(required=false, name="givenBy") String givenByUserIdFilter,
            @RequestParam(required=false, name="transactionDate") Date collectionDateFilter,
            Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("localeAccount", localeAccount);

        List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
        Collections.sort(collections, new TransactionDateComparator());

        // filter list before converting 'giveBy' to 'Last name, First name' format
        List<RegularCollection> filteredCollections = applyFilters(collections, typeDescFilter, givenByUserIdFilter,
                collectionDateFilter);
        model.addAttribute("collections", filteredCollections);

        // replace user ids with user names
        for (RegularCollection regularCollection : collections) {
            String givenBy = regularCollection.getGivenBy();
            JapanFinanceUser userInfo = userRepo.findOne(givenBy);
            if (userInfo != null) {
                regularCollection.setGivenBy(userInfo.getLastName() + ", " + userInfo.getFirstName());
            }
        }
        model.addAttribute("fileuploadForm", new CsvFileUploadForm());

        RegularCollection form = new RegularCollection();
        model.addAttribute("collectionForm", form);

        Set<String> collTypes = new HashSet<String>();
        for (RegularCollection collection : collections) {
            collTypes.add(collection.getType().getDescription());
        }
        model.addAttribute("collTypes", collTypes);

        List<JapanFinanceUser> users = userRepo.findByLocaleOrderByUsername(localeId);
        model.addAttribute("users", users);
        model.addAttribute("givenByFilter", givenByUserIdFilter);

        return "collection/index";
    }

    private List<RegularCollection> applyFilters(
            List<RegularCollection> collections,
            String typeDescFilter,
            String givenByFilter,
            Date collectionDateFilter) {

        if (typeDescFilter != null && !"".equals(typeDescFilter)) {
            collections = collections
                        .stream().filter(x -> x.getType().getDescription().equals(typeDescFilter))
                    .collect(Collectors.toList());
        }

        if (givenByFilter != null && !"".equals(givenByFilter)) {
            collections = collections
                        .stream().filter(x -> x.getGivenBy().equals(givenByFilter))
                    .collect(Collectors.toList());
        }

        if (collectionDateFilter != null && !"".equals(collectionDateFilter)) {
            collections = collections
                        .stream().filter(x -> x.getTransactionDate().equals(collectionDateFilter))
                    .collect(Collectors.toList());
        }

        return collections;
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/new", method = RequestMethod.GET)
    public String newCollection(@PathVariable String localeId, Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        RegularCollection collection = new RegularCollection();
        model.addAttribute("collectionForm", collection);

        List<CollectionType> collTypes = collTypeRepo.findAll();
        model.addAttribute("collTypes", collTypes);

        List<JapanFinanceUser> users = userRepo.findByLocaleOrderByUsername(localeId);
        model.addAttribute("users", users);

        return "collection/new";
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/new", method = RequestMethod.POST)
    public String create(@PathVariable String localeId,
            @RequestParam(name="type") String typeDescription,
            @RequestParam BigDecimal amount,
            @RequestParam String givenBy,
            @RequestParam(name="transactionDate") Date collectionDate,
            Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        /*
         * TODO: When we add a collection to a locale account, it should trigger a creation of a transaction
         *   and add to a CollectionAccount
         */
        // create new collection and save to collections table
        CollectionType collType = collTypeRepo.findByDescription(typeDescription);
        RegularCollection newCollection = new RegularCollection();
        newCollection.setId(new ObjectId());
        newCollection.setType(collType);
        newCollection.setAmount(amount);
        newCollection.setGivenBy(givenBy);
        newCollection.setTransactionDate(collectionDate);

        // Get user info and set as registered by
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        JapanFinanceUser user = ((CustomUserDetails)principal).getUser();
        newCollection.setRegisteredBy(user.getUsername());

        // add to locale account's collections and save locale account
        List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
        collections.add(newCollection);
        localeAccountRepo.save(localeAccount);

        // get target collection account and credit collection
        CollectionAccount collAccount = localeAccount.getAccount(typeDescription);
        if (collAccount == null) {
            collAccount = new CollectionAccount(typeDescription);
            localeAccount.getAccounts().add(collAccount);
            localeAccountRepo.save(localeAccount);
        }
        BigDecimal balance = collAccount.getBalance();
        collAccount.setBalance(balance.add(newCollection.getAmount()));

        localeAccountRepo.save(localeAccount);
        return "redirect:/locale_account/" + localeId + "/collection";

    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/{collectionId}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable String localeId, @PathVariable String collectionId, Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        RegularCollection targetCollection = getTargetCollection(collectionId,
                localeAccount.getProjectBook("Locale Fund").getRegularCollections());
        model.addAttribute("targetCollection", targetCollection);

        List<CollectionType> collTypes = collTypeRepo.findAll();
        model.addAttribute("collTypes", collTypes);

        List<JapanFinanceUser> users = userRepo.findByLocaleOrderByUsername(localeId);
        model.addAttribute("users", users);

        return "collection/edit";
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/{collectionId}/edit", method = RequestMethod.POST)
    public String update(
            @PathVariable String localeId,
            @PathVariable String collectionId,
            @RequestParam(name="type") String typeDescription,
            @RequestParam BigDecimal amount,
            @RequestParam(name="transactionDate") Date collectionDate,
            Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        // create new collection and save to collections table
        RegularCollection targetCollection = getTargetCollection(collectionId,
                localeAccount.getProjectBook("Locale Fund").getRegularCollections());
        targetCollection.setType(collTypeRepo.findByDescription(typeDescription));
        targetCollection.setAmount(amount);
        targetCollection.setTransactionDate(collectionDate);
        localeAccountRepo.save(localeAccount);
        return "redirect:/locale_account/" + localeId + "/collection";

    }

    private RegularCollection getTargetCollection(String collectionId, List<RegularCollection> collections) {
        RegularCollection targetCollection = null;
        for (RegularCollection collection : collections) {
            ObjectId id = collection.getId();
            if(id != null && collectionId.equals(id.toString())) {
                targetCollection = collection;
                break;
            }
        }
        return targetCollection;
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/{collectionId}/delete", method = RequestMethod.GET)
    public String delete(
            @PathVariable final String localeId,
            @PathVariable final String collectionId,
            Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
        for (int i = 0; i < collections.size(); i++) {
            RegularCollection collection = collections.get(i);
            ObjectId id = collection.getId();
            if (id != null && collectionId.equals(id.toString())) {
                collections.remove(i);
            }
        }
        localeAccountRepo.save(localeAccount);

        return "redirect:/locale_account/" + localeId + "/collection";
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/regular", method = RequestMethod.GET)
    public String collection(@PathVariable String localeId,
            @RequestParam(required=false) Date startDate,
            @RequestParam(required=false) Date endDate,
            Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("localeAccount", localeAccount);

        DateFilterForm dateFilterForm = new DateFilterForm();
        model.addAttribute("dateFilterForm", dateFilterForm);

        return "collection/index";
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/regular/new", method = RequestMethod.GET)
    public String newRegCollection(@PathVariable String localeId, Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        ProjectCollection projCollection = new ProjectCollection();
        model.addAttribute("formObj", projCollection);

        List<Project> projects = projectRepo.findAll();
        model.addAttribute("projects", projects);

        List<JapanFinanceUser> users = userRepo.findAll();
        model.addAttribute("users", users);

        return "collection/new";
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/regular/new", method = RequestMethod.POST)
    public String create(@PathVariable String localeId,
            @RequestParam(name="project") String projDescription,
            @RequestParam String givenBy,
            @RequestParam BigDecimal amount,
            @RequestParam(name="transactionDate") Date collectionDate,
            Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        localeAccountRepo.save(localeAccount);
        return "redirect:/locale_account/" + localeId + "/collection/project";

    }

}