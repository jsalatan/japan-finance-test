package org.mcgi.jp.web.collection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.ProjectCollection;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.project.Project;
import org.mcgi.jp.model.project.ProjectRepository;
import org.mcgi.jp.web.DateFilterForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProjectCollectionController {

    private final Logger logger = LoggerFactory.getLogger(ProjectCollectionController.class);

    @Autowired
    private ProjectRepository projectRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @RequestMapping(value = "/collection/project", method = RequestMethod.GET)
    public String index(Map<String, Object> model) {

        List<LocaleAccount> localeAccounts = localeAccountRepo.findAll();
        model.put("localeAccounts", localeAccounts);

        if (localeAccounts == null || localeAccounts.isEmpty()) {
            model.put("msg", "No locales regisered.");
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String url = "collection/project/locale_index";

        LocaleAccount locale = null;
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
            String localeStr = customUser.getLocale();
            locale = localeAccountRepo.findByName(localeStr);

            if (locale == null) {
                locale = localeAccountRepo.findOne(localeStr);
            }

            if (locale != null) {
                url = "redirect:/locale_account/" + locale.getId() + "/collection/project";
            }
        }

        return url;
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/project", method = RequestMethod.GET)
    public String collection(@PathVariable String localeId,
            @RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date startDate,
            @RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date endDate,
            Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("localeAccount", localeAccount);

        DateFilterForm dateFilterForm = new DateFilterForm();
        model.addAttribute("dateFilterForm", dateFilterForm);

        /* FIXME: Get project collection using project book
        List<ProjectCollection> projectCollections = localeAccount.getProjectCollections();
        if (startDate != null && endDate != null) {
            // TODO: filter collections between start and end dates
        }

        // Display projects with their equivalent total collections
        List<Project> projects = projectRepo.findAll();
        Map<String, BigDecimal> projectsMap = new HashMap<String, BigDecimal>();
        for (Project project : projects) {
            String projDesc = project.getDescription();
            // Filter collections by project
            List<ProjectCollection> filteredCollections = projectCollections.stream()
                    .filter(p -> projDesc.equals(p.getProject())).collect(Collectors.toList());

            BigDecimal totalCollection = BigDecimal.ZERO;
            for (ProjectCollection projectCollection : filteredCollections) {
                totalCollection = totalCollection.add(projectCollection.getAmount());
            }

            projectsMap.put(project.getDescription(), totalCollection);
        }

        model.addAttribute("projectsMap", projectsMap);
        */
        return "collection/project/project_index";
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/project/new", method = RequestMethod.GET)
    public String newCollection(@PathVariable String localeId, Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("locale", localeAccount);

        ProjectCollection projCollection = new ProjectCollection();
        model.addAttribute("formObj", projCollection);

        List<Project> projects = projectRepo.findAll();
        model.addAttribute("projects", projects);

        List<JapanFinanceUser> users = userRepo.findAll();
        model.addAttribute("users", users);

        return "collection/project/new";
    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/project/new", method = RequestMethod.POST)
    public String create(@PathVariable String localeId,
            @RequestParam(name="project") String projDescription, @RequestParam String givenBy, @RequestParam BigDecimal amount,
            @RequestParam(name="transactionDate") @DateTimeFormat(pattern="yyyy-MM-dd") Date collectionDate,
            Model model) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        /*
         * TODO: When we add a collection to a locale account, it should trigger a creation of a transaction
         *   and add to a CollectionAccount
         */
        /* FIXME:Update project book upon submission of new project collection
        // create new collection and save to collections table
        ProjectCollection newCollection = new ProjectCollection(new ObjectId(), projDescription, givenBy, amount, collectionDate);

        // add to locale account's collections and save locale account
        List<ProjectCollection> collections = localeAccount.getProjectCollections();
        collections.add(newCollection);
        localeAccountRepo.save(localeAccount);

        // get target collection account and credit collection
        CollectionAccount collAccount = localeAccount.getAccount(projDescription);
        if (collAccount == null) {
            collAccount = new CollectionAccount(projDescription);
            localeAccount.getAccounts().add(collAccount);
            localeAccountRepo.save(localeAccount);
        }
        BigDecimal balance = collAccount.getBalance();
        collAccount.setBalance(balance.add(newCollection.getAmount()));
        */

        localeAccountRepo.save(localeAccount);
        return "redirect:/locale_account/" + localeId + "/collection/project";

    }

    @RequestMapping(value = "/locale_account/{localeId}/collection/project/{project}", method = RequestMethod.GET)
    public String projectCollectionIndex(@PathVariable String localeId,
            @PathVariable(value="project") String projectStr,
            @RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date startDate,
            @RequestParam(required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date endDate,
            Model model) {

        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);
        model.addAttribute("localeAccount", localeAccount);

        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);

        Project project = projectRepo.findOne(projectStr);
        if (project == null) {
            project = projectRepo.findByDescription(projectStr);
        }
        model.addAttribute("project", project);

        return "collection/project/collection_index";
    }

}