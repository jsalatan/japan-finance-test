package org.mcgi.jp.enumeration;

public enum ExpenseStatus {
    PENDING,
    APPROVED
}
