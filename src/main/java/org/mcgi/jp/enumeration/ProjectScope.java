package org.mcgi.jp.enumeration;

public enum ProjectScope {
    LOCALE,
    NATIONAL,
    DISTRICT,
    DIVISION,
    INTERNATIONAL
}
