package org.mcgi.jp.service;

import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class FinanceUserService {

    private static final Logger logger = LoggerFactory.getLogger(FinanceUserService.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    public boolean hasLocale() {
        return (this.getLocale() != null);
    }

    public LocaleAccount getLocale() {
        LocaleAccount locale = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
            String localeStr = customUser.getLocale();
            locale = localeAccountRepo.findByName(localeStr);

            if (locale == null) {
                locale = localeAccountRepo.findOne(localeStr);
            }
        }

        return locale;
    }

}