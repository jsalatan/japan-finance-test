package org.mcgi.jp.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.mcgi.jp.common.util.CalendarUtil;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.RegularCommitment;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.UserRepository;
import org.mcgi.jp.model.admin.UsernameComparator;
import org.mcgi.jp.web.report.MonthlyReportRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CollectionAndCommitmentService {

    private static final Logger logger = LoggerFactory.getLogger(FinanceUserService.class);

    @Autowired
    private LocaleAccountRepository localeAccountRepo;

    @Autowired
    private UserRepository userRepo;

    public boolean hasLocale() {
        return (this.getLocale() == null);
    }

    public LocaleAccount getLocale() {
        LocaleAccount locale = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            JapanFinanceUser customUser = ((CustomUserDetails)principal).getUser();
            String localeStr = customUser.getLocale();
            locale = localeAccountRepo.findByName(localeStr);

            if (locale == null) {
                locale = localeAccountRepo.findOne(localeStr);
            }
        }

        return locale;
    }

    public List<RegularExpense> getCurrentMonthsExpenses() {
        // TODO Auto-generated method stub
        return null;
    }

    public List<MonthlyReportRecord> getMonthlyReport(String localeId) {
        LocaleAccount localeAccount = localeAccountRepo.findOne(localeId);

        List<JapanFinanceUser> users = userRepo.findByLocale(localeId);
        Collections.sort(users, new UsernameComparator());

        Date firstDay = CalendarUtil.getFirstDayOfCurrentMonth();
        List<MonthlyReportRecord> records = new ArrayList<MonthlyReportRecord>();
        /*
         * 1) Get all members of locale
         * 2) Get user commitment (for current month)
         * 3) Get user contributions
         */
        // Iterate the list of users
        for (JapanFinanceUser user : users) {
            MonthlyReportRecord rowRecord = new MonthlyReportRecord();
            String username = user.getUsername();
            rowRecord.setMemberId(username);

            // Get commitment
            List<RegularCommitment> regularCommitments = localeAccount.getProjectBook("Locale Fund").getRegularCommitments();
            List<RegularCommitment> filteredCommitment = regularCommitments.stream()
                    .filter(x -> x.getCommittedBy().equals(username)).collect(Collectors.toList());

            if (filteredCommitment != null && !filteredCommitment.isEmpty()) {
                rowRecord.setCommitment(filteredCommitment.get(0).getAmount());
            } else {
                rowRecord.setCommitment(BigDecimal.ZERO);
            }

            /*
             * Get collections, filter by date (within date range), then collect by user.
             */
            List<RegularCollection> regularCollections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();

            Date[] wk1 = CalendarUtil.getWeekDateRange(firstDay, 1);
            rowRecord.setCollectionWeek1(getTotalCollection(username, regularCollections, wk1[0], wk1[1]));

            Date[] wk2 = CalendarUtil.getWeekDateRange(firstDay, 2);
            rowRecord.setCollectionWeek2(getTotalCollection(username, regularCollections, wk2[0], wk2[1]));

            Date[] wk3 = CalendarUtil.getWeekDateRange(firstDay, 3);
            rowRecord.setCollectionWeek3(getTotalCollection(username, regularCollections, wk3[0], wk3[1]));

            Date[] wk4 = CalendarUtil.getWeekDateRange(firstDay, 4);
            rowRecord.setCollectionWeek4(getTotalCollection(username, regularCollections, wk4[0], wk4[1]));

            Date[] wk5 = CalendarUtil.getWeekDateRange(firstDay, 5);
            rowRecord.setCollectionWeek5(getTotalCollection(username, regularCollections, wk5[0], wk5[1]));

            BigDecimal totalCollection = BigDecimal.ZERO;
            if (rowRecord.getCollectionWeek1() != null) {
                totalCollection = totalCollection.add(rowRecord.getCollectionWeek1());
            }
            if (rowRecord.getCollectionWeek2() != null) {
                totalCollection = totalCollection.add(rowRecord.getCollectionWeek2());
            }
            if (rowRecord.getCollectionWeek3() != null) {
                totalCollection = totalCollection.add(rowRecord.getCollectionWeek3());
            }
            if (rowRecord.getCollectionWeek4() != null) {
                totalCollection = totalCollection.add(rowRecord.getCollectionWeek4());
            }
            if (rowRecord.getCollectionWeek5() != null) {
                totalCollection = totalCollection.add(rowRecord.getCollectionWeek5());
            }

            rowRecord.setTotalCollection(totalCollection);
            records.add(rowRecord);
        }

        // Add total row
        MonthlyReportRecord totalRow = getTotalRow(records);
        records.add(totalRow);

        return records;
    }

    private MonthlyReportRecord getTotalRow(List<MonthlyReportRecord> records) {

        BigDecimal[] totals = {
                // Commitment Total
                BigDecimal.ZERO,
                // Weekly totals
                BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
                // Total-total
                BigDecimal.ZERO
                };

        for (MonthlyReportRecord monthlyRecord : records) {
            BigDecimal[] collectionPerRow = {
                    monthlyRecord.getCommitment(),
                    monthlyRecord.getCollectionWeek1(), monthlyRecord.getCollectionWeek2(),
                    monthlyRecord.getCollectionWeek3(), monthlyRecord.getCollectionWeek4(),
                    monthlyRecord.getCollectionWeek5(),
                    monthlyRecord.getTotalCollection()};

            for (int i = 0; i < collectionPerRow.length; i++) {
                BigDecimal weeklyContribution = collectionPerRow[i];
                if (weeklyContribution != null) {
                    totals[i] = totals[i].add(weeklyContribution);
                }
            }
        }

        MonthlyReportRecord totalRow = new MonthlyReportRecord();
        totalRow.setMemberId("TOTAL");

        int i = 0;
        totalRow.setCommitment(totals[i++]);
        totalRow.setCollectionWeek1(totals[i++]);
        totalRow.setCollectionWeek2(totals[i++]);
        totalRow.setCollectionWeek3(totals[i++]);
        totalRow.setCollectionWeek4(totals[i++]);
        totalRow.setCollectionWeek5(totals[i++]);
        totalRow.setTotalCollection(totals[i]);

        return totalRow;
    }

    private BigDecimal getTotalCollection(String username, List<RegularCollection> regularCollections,
            Date weekStartDate, Date weekEndDate) {

        List<RegularCollection> filteredByDateCollections = regularCollections.stream()
                .filter(x -> x.getTransactionDate().compareTo(weekStartDate) >= 0
                        && x.getTransactionDate().compareTo(weekEndDate) <= 0)
                .collect(Collectors.toList());

        List<RegularCollection> targetCollections = new ArrayList<RegularCollection>();
        for (RegularCollection regularCollection : filteredByDateCollections) {
            String givenBy = regularCollection.getGivenBy();
            if (givenBy.equals(username)) {
                targetCollections.add(regularCollection);
            }
        }

        BigDecimal totalCollection = BigDecimal.ZERO;
        for (RegularCollection regularCollection : targetCollections) {
            totalCollection = totalCollection.add(regularCollection.getAmount());
        }

        return totalCollection;
    }

    public List<Date> getWeekendDatesofCurrentMonth() {
        Date firstDay = CalendarUtil.getFirstDayOfCurrentMonth();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);

        int maxWeekNum = cal.getActualMaximum(Calendar.WEEK_OF_MONTH);

        List<Date> weekendDates = new ArrayList<Date>();
        for (int i = 1; i <= maxWeekNum; i++) {
            Date[] wk = CalendarUtil.getWeekDateRange(firstDay, i);
            weekendDates.add(wk[1]);
        }

        return weekendDates;
    }

}
