package org.mcgi.jp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Scheduled;

public class CommitmentEmailNotifBatchService {

    private static final Logger logger = LoggerFactory.getLogger(CommitmentEmailNotifBatchService.class);

    @Autowired
    private MailSender mailSender;

    // https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm
    // Run every hour, every day at 0-minute
    // second - minutes - hours - day of month - month - day of week - year
    @Scheduled(cron = "0 * * * * ?")
    public void commitmentEmailNotification() {

        logger.debug("commitmentEmailNotification() is executed!");

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("eastmaels@gmail.com");
        message.setTo("jsalatan@gmail.com");
        message.setSubject("Test Email from Spring for Japan-Finance");
        message.setText("Mail body");
        mailSender.send(message);

    }

}