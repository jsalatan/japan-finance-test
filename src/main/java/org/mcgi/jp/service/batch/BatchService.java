package org.mcgi.jp.service.batch;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.mcgi.jp.common.util.CalendarUtil;
import org.mcgi.jp.model.LocaleAccount;
import org.mcgi.jp.model.LocaleAccountRepository;
import org.mcgi.jp.model.RegularCollection;
import org.mcgi.jp.model.RegularExpense;
import org.mcgi.jp.model.audit.Statement;
import org.mcgi.jp.model.audit.StatementRepository;
import org.mcgi.jp.model.project.ProjectBook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Scheduled;

public class BatchService {

    private static final Logger logger = LoggerFactory.getLogger(BatchService.class);

    @Autowired
    private StatementRepository statementRepo;

    @Autowired
    private LocaleAccountRepository localeRepo;

    @Autowired
    private MailSender mailSender;

    // https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm
    // run every first of the month
    @Scheduled(cron = "0 0 0 1 * ?")
    public void getDesc() {

        logger.debug("getDesc() is executed!");
        logger.info("getDesc() is executed!");

    }

    // https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm
    // Run every hour, every day at 0-minute
    // second - minutes - hours - day of month - month - day of week - year
    @Scheduled(cron = "0 * * * * ?")
    public void commitmentEmailNotification() {

        logger.debug("commitmentEmailNotification() is executed!");

        // NOTIFY All GS and LC of Locale Fund's collections vs upcoming expenses (bills) 
        List<LocaleAccount> localeAccounts = localeRepo.findAll();
        for (LocaleAccount localeAccount : localeAccounts) {
            
            ProjectBook localeFund = localeAccount.getProjectBook("Locale Fund");
            BigDecimal totalCollections = localeFund.getTotalCollection();
            BigDecimal totalPaidExpenses = localeFund.getTotalExpenses(true);
            
            BigDecimal remainingBalance = totalCollections.subtract(totalPaidExpenses);
            BigDecimal totalUnpaidExpenses = localeFund.getTotalUnpaidExpenses();
            
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("eastmaels@gmail.com");
            message.setTo("jsalatan@gmail.com");
            message.setSubject("Locale Fund Reminder : COH vs Upcoming Expenses");
            
            StringBuffer content = new StringBuffer();
            content.append("Total Cash-On-Hand: " + remainingBalance.toString() + "\n");
            content.append("Upcoming Expense Total: " + totalUnpaidExpenses.toString() + "\n");
            content.append("List Of Unpaid Expenses: \n");
            
            List<RegularExpense> unpaidExpenses = localeFund.getUnpaidExpenses();
            for (RegularExpense unpaidExpense : unpaidExpenses) {
                content.append("  " + unpaidExpense.getExpenseType().getDescription() + "\n");
                content.append("    Amount: " + unpaidExpense.getAmount() + "\n");
                content.append("    Due Date: " + unpaidExpense.getDueDate() + "\n");
            }
            
            message.setText(content.toString());
            mailSender.send(message);

        }

    }

    // run every minute
    @Scheduled(cron = "0 * * * * ?")
    public void createMonthlyStatement() {

        logger.debug("createMonthlyStatement() is executed!");

        List<LocaleAccount> locales = localeRepo.findAll();

        Calendar currCal = Calendar.getInstance();
        Date currFirstDay = CalendarUtil.getFirstDayOfMonth(currCal);
        Date currLastDay = CalendarUtil.getLastDayOfMonth(currCal);

        Calendar prevCal = Calendar.getInstance();
        prevCal.add(Calendar.MONTH, -1);

        DateFormat df = new SimpleDateFormat("yyyyMM");
        /*
         * 1. Get previous month's ending balance
         * 2. Get this month's collections
         * 3. Get this month's expenses
         * 4. Add all collections (#2 current collections + #1 previous ending balance)
         * 5. Add all expenses
         * 6. Subtract expenses from total collections (#4)
         */
        for (LocaleAccount localeAccount : locales) {

            String localeId = localeAccount.getId();
            String currentYm = df.format(currCal.getTime());

            // Check if this month's statement already exists
            Statement currMonthStatement = statementRepo.findByLocaleIdAndStatementYm(localeId, currentYm);
            if (currMonthStatement != null) {
                break;
            }

            // Instantiate this month's statement
            // Statement's composite index
            Statement newStatement = new Statement(localeId, currentYm);

            // Set this month's statement's beginning balance to previous month's ending balance
            Statement prevMonthStatement = statementRepo.findByLocaleIdAndStatementYm(localeId,
                    df.format(prevCal.getTime()));
            BigDecimal prevBalance = null;
            if (prevMonthStatement == null) {
                prevBalance = BigDecimal.ZERO;
            } else {
                prevBalance = prevMonthStatement.getEndingBalance();
            }
            newStatement.setStartingBalance(prevBalance);

            // Set this month's collections
            List<RegularCollection> collections = localeAccount.getProjectBook("Locale Fund").getRegularCollections();
            List<RegularCollection> filteredCollections = collections.stream()
                    .filter(c -> c.getTransactionDate().compareTo(currFirstDay) >= 0
                                && c.getTransactionDate().compareTo(currLastDay) <= 0)
                    .collect(Collectors.toList());
            newStatement.setCollections(filteredCollections);

            // Set this month's expenses
            List<RegularExpense> expenses = localeAccount.getProjectBook("Locale Fund").getRegularExpenses();
            List<RegularExpense> filteredExpenses = expenses.stream()
                    .filter(c -> c.getTransactionDate().compareTo(currFirstDay) >= 0
                                && c.getTransactionDate().compareTo(currLastDay) <= 0)
                    .collect(Collectors.toList());
            newStatement.setExpenses(filteredExpenses);

            // Set this month's ending balance
            BigDecimal endingBalance = computeForEndingBalance(filteredCollections, filteredExpenses);
            newStatement.setEndingBalance(endingBalance);

            statementRepo.insert(newStatement);
        }

    }

    private BigDecimal computeForEndingBalance(List<RegularCollection> collections, List<RegularExpense> expenses) {

        BigDecimal totalCollection = BigDecimal.ZERO;
        for (RegularCollection collection : collections) {
            totalCollection = totalCollection.add(collection.getAmount());
        }

        BigDecimal totalExpense = BigDecimal.ZERO;
        for (RegularExpense expense : expenses) {
            totalExpense = totalExpense.add(expense.getAmount());
        }

        return totalCollection.subtract(totalExpense);
    }

}