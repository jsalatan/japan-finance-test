package org.mcgi.jp.config.security;

import java.util.Collection;

import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.springframework.security.core.GrantedAuthority;

public class CustomUserDetails extends org.springframework.security.core.userdetails.User {

    /** auto-generated serialVersionUID */
    private static final long serialVersionUID = -4533278283358246109L;

    private JapanFinanceUser user;

    public CustomUserDetails(JapanFinanceUser user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getUsername(), user.getPassword(), authorities);
        this.user = user;
    }

    public CustomUserDetails(JapanFinanceUser user, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(user.getUsername(), user.getPassword(), enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.user = user;
    }

    public JapanFinanceUser getUser() {
        return user;
    }

}
