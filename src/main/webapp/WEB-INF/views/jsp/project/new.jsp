<%@ include file="../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
  });
</script>

<title>Add Project</title>

<%@ include file="../common/header_bottom.jsp" %>

<spring:url value="/project/new" var="submitUrl" />
<form:form method="POST" modelAttribute="project" action="${submitUrl}">
<div class="row">
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="description">Description</label>
    <form:input path="description" type="text" class="form-control" placeholder="Description" style="width: 100%;"/>
  </div>
  <div class="col-sm-1 form-group" >
    <label class="sr-only" for="scope">Scope</label>
    <form:input path="scope" type="text" class="form-control" placeholder="Scope" style="width: 100%;"
      list="scopes"/>
    <datalist id="scopes">
     <c:forEach items="${ scopes }" var="scope">
     <option value="${ scope }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="scope">Total Target</label>
    <form:input path="targetAmt" type="text" class="form-control" placeholder="Total Target" style="width: 100%;" />
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="mode">Mode</label>
    <form:input path="mode" type="text" class="form-control" placeholder="Mode" style="width: 100%;"
      list="modes"/>
    <datalist id="modes">
     <c:forEach items="${ modes }" var="mode">
     <option value="${ mode }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="startDate">Start Date</label>
    <form:input path="startDate" type="date" class="form-control" placeholder="Start Date" style="width: 100%;" />
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="endDate">End Date</label>
    <form:input path="endDate" type="date" class="form-control" placeholder="End Date" style="width: 100%;" />
  </div>
  <div class="col-sm-1">
    <input type="submit" value="Submit" class="btn btn-primary" />
  </div>
</div>
</form:form>

<%@ include file="../common/footer.jsp" %>