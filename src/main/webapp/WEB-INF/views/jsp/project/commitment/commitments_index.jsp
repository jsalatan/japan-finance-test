<%@ include file="../../common/header_top.jsp" %>

<title>Commitments</title>

<%@ include file="../../common/header_bottom.jsp" %>
  <!-- Table -->
  <table class="table">
    <thead>
    <tr>
      <th>Member ID</th>
      <th>Amount</th>
      <th>Commitment Date</th>
    </tr>
    </thead>
    <tbody>
      <c:forEach items="${ projectBook.regularCommitments }" var="commitment">
      <tr>
        <td><c:out value="${ commitment.committedBy }"/></td>
        <td>
          <spring:url value="/locale_account/${localeAccount.id}/commitment/project/${projectBook.id}/${commitment.id}/edit" var="editUrl" />
          <a href="${ editUrl }">
            <fmt:formatNumber type="CURRENCY" value="${commitment.amount}" />
          </a>
        </td>
        <td><fmt:formatDate type="date" value="${commitment.commitmentDate}" /></td>
      </tr>
      </c:forEach>
      <tr>
        <td colspan="3">
          <spring:url value="/locale_account/${localeAccount.id}/commitment/project/${projectBook.id}/new" var="newUrl" />
          <a href="${ newUrl }">Add Member Commitment</a>
        </td>
      </tr>
    </tbody>
  </table>
<%@ include file="../../common/footer.jsp" %>