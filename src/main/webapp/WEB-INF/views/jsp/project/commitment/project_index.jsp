<%@ include file="../../common/header_top.jsp" %>

<title>Project Index</title>

<%@ include file="../../common/header_bottom.jsp" %>
  <div class="row">
    <div class="col-lg-12">
      <h3>Projects</h3>
    </div>
  </div>

  <!-- Table -->
  <div class="row">
      <c:forEach items="${ projectBooks }" var="projectBook">
      <sec:authorize access="hasAnyRole('ROLE_MEMBER')">
      <div class="col-sm-6">
        <span>Project :</span>
        <spring:url value="/locale_account/${localeAccount.id}/commitment/project/${projectBook.id}/new" var="url" />
        <a href="${ url }" >
          <c:out value="${ projectBook.projectDesc }"/>
        </a>
      </div>
      </sec:authorize>

      <sec:authorize access="hasAnyRole('ROLE_LOCALE_COORDINATOR')">
      <div class="col-sm-6">
          <spring:url value="/locale_account/${localeAccount.id}/commitment/project/${projectBook.id}/new" var="url" />          
          <a href="${ url }" >
            <c:out value="${ projectBook.projectDesc }"/>
          </a>
      </div>
      </sec:authorize>

      <div class="col-sm-3">
        <span>Target :</span>
        <fmt:formatNumber type="CURRENCY" value="${ projectBook.target }" />
      </div>
      <div class="col-sm-3">
        <span>Total Commitment :</span>
        <fmt:formatNumber type="CURRENCY" value="${ projectBook.totalCommitment }" />
      </div>

      </c:forEach>
  </div>
  <sec:authorize access="hasAnyRole('ROLE_LOCALE_COORDINATOR')">
  <div class="row">
    <spring:url value="/locale_account/${localeAccount.id}/project_book" var="newUrl" />
    <a href="${ newUrl }">Subscribe to Project</a>
  </div>
  </sec:authorize>
<%@ include file="../../common/footer.jsp" %>