<%@ include file="../../common/header_top.jsp" %>

<title>Commitments</title>

<%@ include file="../../common/header_bottom.jsp" %>
  <!-- Table -->
  <table class="table">
    <thead>
    <tr>
      <th>Member ID</th>
      <th>Amount</th>
      <th>Commitment Date</th>
    </tr>
    </thead>
    <tbody>
      <c:forEach items="${ projectBook.commitments }" var="commitment">
      <tr>
        <td><c:out value="${ commitment.scope }"/></td>
        <td><fmt:formatNumber type="CURRENCY" value="${commitment.amount}" /></td>
        <td><fmt:formatDate type="date" value="${commitment.commitmentDate}" /></td>
      </tr>
      </c:forEach>
      <tr>
        <td colspan="3">
          <spring:url value="/locale_account/${localeAccount.id}/commitment/project/${project.id}/new" var="newUrl" />
          <a href="${ newUrl }">Add Commitment</a>
        </td>
      </tr>
    </tbody>
  </table>
<%@ include file="../../common/footer.jsp" %>