<%@ include file="../../common/header_top.jsp" %>

<title>New Project Commitment</title>

<%@ include file="../../common/header_bottom.jsp" %>

<div class="row">
    <div class="col-lg-12">
      <h3>New Commitment for ${projectBook.projectDesc}</h3>
    </div>
  </div>

<spring:url value="/locale_account/${localeAccount.id}/commitment/project/${projectBook.id}/new" var="submitUrl" />
<form:form method="POST" modelAttribute="commitment" action="${submitUrl}">
<div class="row">
  <sec:authorize access="hasAnyRole('ROLE_LOCALE_COORDINATOR')">
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="expenseType">Member</label>
    <form:select path="committedBy" class="form-control" placeholder="Member" style="width: 100%;">
      <c:forEach items="${ users }" var="user">
        <option value="${ user.username }">
          <c:out value="${ user.lastName }"/>, <c:out value="${ user.firstName }"/>
        </option>
      </c:forEach>
    </form:select>
  </div>
  </sec:authorize>
  <sec:authorize access="hasAnyRole('ROLE_MEMBER')">
  <div class="col-sm-2 form-group ui-widget" style="display: none">
    <form:input path="committedBy" type="text" class="form-control" placeholder="Member" style="width: 100%;" 
      value="${member.username}" readonly="true" />
  </div>
  </sec:authorize>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="amount">Amount</label>
    <form:input path="amount" type="number" class="form-control" placeholder="Amount" style="width: 100%;" />
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="commitmentDate">Commitment Date</label>
    <form:input path="commitmentDate" type="date" class="form-control" placeholder="Commitment Date" style="width: 100%;"/>
  </div>
  <div class="col-sm-2">
    <input type="submit" value="Submit" class="btn btn-primary" style="width: 100%;" />
  </div>
</div>
</form:form>
<%@ include file="../../common/footer.jsp" %>