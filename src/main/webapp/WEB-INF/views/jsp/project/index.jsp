<%@ include file="../common/header_top.jsp" %>

<title>Expenses</title>

<%@ include file="../common/header_bottom.jsp" %>
  <!-- Table -->
  <table class="table">
    <thead>
    <tr>
      <th>Description</th>
      <th>Scope</th>
      <th>Target Amount</th>
      <th>Mode</th>
      <th>Start Date</th>
      <th>End Date</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <c:forEach items="${ projects }" var="project">
      <tr>
        <td><c:out value="${ project.description }"/></td>
        <td><c:out value="${ project.scope }"/></td>
        <td><fmt:formatNumber type="CURRENCY" value="${project.targetAmt}" /></td>
        <td><c:out value="${ project.mode }"/></td>
        <td><fmt:formatDate type="date" value="${project.startDate}" /></td>
        <td><fmt:formatDate type="date" value="${project.endDate}" /></td>
        <td>
          <spring:url value="/project/${project.id}/edit" var="editUrl" />
          <a href="${ editUrl }">Edit</a>
        </td>
      </tr>
      </c:forEach>
      <tr>
        <td colspan="2">
          <spring:url value="/project/new" var="newUrl" />
          <a href="${ newUrl }">Create new project</a>
        </td>
      </tr>
    </tbody>
  </table>
<%@ include file="../common/footer.jsp" %>