<%@ include file="../../common/header_top.jsp" %>

<title>Collections</title>

  <style>
    * { 
      margin: 0; 
      padding: 0; 
    }
    body { 
      font: 14px/1.4 Georgia, Serif; 
    }
    #page-wrap {
      margin: 50px;
    }
    p {
      margin: 20px 0; 
    }
    
      /* 
      Generic Styling, for Desktops/Laptops 
      */
      table { 
        width: 100%; 
        border-collapse: collapse; 
      }
      /* Zebra striping */
      tr:nth-of-type(odd) { 
        background: #eee; 
      }
      th { 
        background: #333; 
        color: white; 
        font-weight: bold; 
      }
      td, th { 
        padding: 6px; 
        border: 1px solid #ccc; 
        text-align: left; 
      }
  </style>
  
  <!--[if !IE]><!-->
  <style>

  /*
  Max width before this PARTICULAR table gets nasty
  This query will take effect for any screen smaller than 760px
  and also iPads specifically.
  */
  @media
  only screen and (max-width: 760px),
  (min-device-width: 768px) and (max-device-width: 1024px)  {
  
    /* Force table to not be like tables anymore */
    #no-more-tables table, 
    #no-more-tables thead, 
    #no-more-tables tbody, 
    #no-more-tables th, 
    #no-more-tables td, 
    #no-more-tables tr { 
      display: block; 
    }
  
    /* Hide table headers (but not display: none;, for accessibility) */
    #no-more-tables thead tr { 
      position: absolute;
      top: -9999px;
      left: -9999px;
    }
  
    #no-more-tables tr { border: 1px solid #ccc; }
  
    #no-more-tables td { 
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee; 
      position: relative;
      padding-left: 50%; 
      white-space: normal;
      text-align:left;
    }
  
    #no-more-tables td:before { 
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%; 
      padding-right: 10px; 
      white-space: nowrap;
      text-align:left;
    }
  
    /*
    Label the data
    */
    #no-more-tables td:before { content: attr(data-title); }
  }

  /* Smartphones (portrait and landscape) ----------- */
  @media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
    body {
      padding: 0;
      margin: 0;
      width: 320px; }
    }

  /* iPads (portrait and landscape) ----------- */
  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
    body {
      width: 495px;
    }
  }

  </style>
  <!--<![endif]-->

  <script>
    $(document).ready(function () {
      var
        filters = {
          type: null,
          person: null,
          collect: null
        };
  
      function updateFilters() {
        $('.collection-row').hide().filter(function () {
          var
            self = $(this),
            result = true; // not guilty until proven guilty
  
          Object.keys(filters).forEach(function (filter) {
            if (filters[filter]) {
              result = result && (self.data(filter).toUpperCase().indexOf(filters[filter].toUpperCase()) > -1);
            }
          });
  
          return result;
        }).show();
      }
  
      function bindDropdownFilters() {
        Object.keys(filters).forEach(function (filterName) {
          $('#' + filterName + "-filter").on('keyup', function () {
            filters[filterName] = this.value;
            updateFilters();
          });
        });
      }
  
      bindDropdownFilters();
    });
  </script>

<%@ include file="../../common/header_bottom.jsp" %>
  <div class="row">
    <div class="col-md-12" id="no-more-tables">
      <!-- Table -->
      <table class="table" id="collections-table">
        <thead>
        <tr>
          <th>Date Generated</th>
          <th>Reported By</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
          <c:forEach items="${ reports }" var="report">
          <tr class="collection-row"
            data-reporter="${ report.reporter }"
            data-collect="<fmt:formatDate value='${ report.dateGenerated }' type='date' pattern='yyyy/MM/dd'/>">

            <td data-title="Reporter"><c:out value="${ report.reporter }"/></td>
            <td data-title="Date Generated"><fmt:formatDate type="date" value="${report.dateGenerated}" /></td>
            <td data-title="Action">
              <spring:url value="/collection_report/${ report.id }/edit" var="editUrl" />
              <a href="${ editUrl }">
                <i class="pe-7s-note pe-lg" ></i>
              </a>
              <spring:url value="/collection_report/${ report.id }/delete" var="url" />
              <a href="${ url }">
                <i class="pe-7s-trash pe-lg" ></i>
              </a>
            </td>
          </tr>
          </c:forEach>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row" style="margin-top: 10px;">
    <div class="col-sm-12 form-group ui-widget" >
      <spring:url value="/collection_report/new" var="newUrl" />
      <a href="${ newUrl }" class="btn btn-primary">Add Collection</a>
    </div>
  </div>
  <sec:authorize access="isAuthenticated() and hasAnyRole('ROLE_LOCALE_TREASURER')">
  <div class="row">
    <table>
      <tr>
        <td colspan="2">
          <spring:url value="/collection_report/csv_upload" var="uploadUrl" />
          <form:form modelAttribute="fileuploadForm" action="${ uploadUrl }" method="POST" enctype="multipart/form-data">
            <label for="file">File</label>
            <form:input path="file" type="file" />
            <p><button type="submit">Upload</button></p>
          </form:form>
        </td>
      </tr>
    </table>
  </div>
  </sec:authorize>
<%@ include file="../../common/footer.jsp" %>