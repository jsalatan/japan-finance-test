<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="row">
  <spring:url value="/locale_account/${locale.id}/submit_collection" var="actionUrl" />
  <form:form method="POST" modelAttribute="collection" action="${actionUrl}"
    class="navbar-form navbar-left">
  <div class="form-group">
    <form:select path="type" class="form-control">
      <form:option value="" label="--- Select ---"/>
      <form:options items="${ collTypes }"/>
    </form:select>
    <form:input path="amount" type="text" class="form-control"
            id="amount" placeholder="Amount" />
    <form:input path="remittanceDate" type="text" class="form-control"
            id="remittanceDate" placeholder="Remittance Date" />
      <button class="btn btn-default submit" type="button">add</button>
  </div>
  </form:form>
</div>