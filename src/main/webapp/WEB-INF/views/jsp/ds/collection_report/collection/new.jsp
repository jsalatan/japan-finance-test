<%@ include file="../../../common/header_top.jsp" %>

<title>Add Collection</title>

<%@ include file="../../../common/header_bottom.jsp" %>

<spring:url value="/locale_account/${locale.id}/collection/new" var="submitUrl" />
<form:form class="form-inline" method="POST" modelAttribute="collectionForm" action="${submitUrl}">
<div class="row">
  <div class="col-sm-3 form-group ui-widget" >
    <label class="sr-only" for="type">Collection Type</label>
    <form:select path="type" class="form-control" placeholder="Collection Type" style="width: 100%;">
      <c:forEach items="${ collTypes }" var="collType">
        <form:option value="${ collType.description }" />
      </c:forEach>
    </form:select>
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="amount">Amount</label>
    <form:input path="amount" type="text" class="form-control" placeholder="Amount" style="width: 100%;" />
  </div>
  <div class="col-sm-3 form-group ui-widget" >
    <label class="sr-only" for="givenBy">Given By</label>
    <form:select path="givenBy" class="form-control" placeholder="Given By" style="width: 100%;">
      <c:forEach items="${ users }" var="user">
        <option value="${ user.username }">
          <c:out value="${ user.lastName }"/>, <c:out value="${ user.firstName }"/>
        </option>
      </c:forEach>
    </form:select>
  </div>
  <div class="col-sm-3 form-group" >
    <label class="sr-only" for="transactionDate">Collection Date</label>
    <input name="transactionDate" id="transactionDate" type="date" class="form-control"
      placeholder="Collection date" />
  </div>
  <div class="col-sm-1">
    <input type="submit" value="Submit" class="btn btn-primary btn-block" disabled/>
  </div>
</div>
</form:form>

<script>
  (function($) {
    $('#collectionForm').on("change", ":input", function() {
      var allHasValues = true;
      $(this).parents('.row').find(':input').each(function() {
        if (!$(this).val()) {
          allHasValues = false;
        }
      })
      if( allHasValues ) {
        $("input[type=submit]").prop("disabled", false);
      } else {
        $("input[type=submit]").prop("disabled", true);
      }
    });
  }(jQuery));
</script>

<%@ include file="../../../common/footer.jsp" %>