<spring:url value="/collection/submit" var="actionUrl" />
<form:form method="POST" modelAttribute="collection" action="${actionUrl}">
              <tr>
                <td>
                  <form:select path="type" class="form-control">
                    <form:options items="${collTypes}"/>
                  </form:select>
                </td>
                <td>
                  <form:input path="amount" type="text" class="form-control"
                    id="amount" placeholder="Amount" />
                </td>
                <td>
                  <form:input path="remittanceDate" type="text" class="form-control"
                    id="remittanceDate" placeholder="Remittance Date" />
                </td>
              </tr>
</form:form>
