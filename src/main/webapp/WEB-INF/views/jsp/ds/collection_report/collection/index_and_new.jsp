<%@ include file="../../../common/header_top.jsp" %>

<title>Add Collection</title>

  <style>
    * { 
      margin: 0; 
      padding: 0; 
    }
    body { 
      font: 14px/1.4 Georgia, Serif; 
    }
    #page-wrap {
      margin: 50px;
    }
    p {
      margin: 20px 0; 
    }
    
      /* 
      Generic Styling, for Desktops/Laptops 
      */
      table { 
        width: 100%; 
        border-collapse: collapse; 
      }
      /* Zebra striping */
      tr:nth-of-type(odd) { 
        background: #eee; 
      }
      th { 
        background: #333; 
        color: white; 
        font-weight: bold; 
      }
      td, th { 
        padding: 6px; 
        border: 1px solid #ccc; 
        text-align: left; 
      }
  </style>
  
  <!--[if !IE]><!-->
  <style>

  /*
  Max width before this PARTICULAR table gets nasty
  This query will take effect for any screen smaller than 760px
  and also iPads specifically.
  */
  @media
  only screen and (max-width: 760px),
  (min-device-width: 768px) and (max-device-width: 1024px)  {
  
    /* Force table to not be like tables anymore */
    #no-more-tables table, 
    #no-more-tables thead, 
    #no-more-tables tbody, 
    #no-more-tables th, 
    #no-more-tables td, 
    #no-more-tables tr { 
      display: block; 
    }
  
    /* Hide table headers (but not display: none;, for accessibility) */
    #no-more-tables thead tr { 
      position: absolute;
      top: -9999px;
      left: -9999px;
    }
  
    #no-more-tables tr { border: 1px solid #ccc; }
  
    #no-more-tables td { 
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee; 
      position: relative;
      padding-left: 50%; 
      white-space: normal;
      text-align:left;
    }
  
    #no-more-tables td:before { 
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%; 
      padding-right: 10px; 
      white-space: nowrap;
      text-align:left;
    }
  
    /*
    Label the data
    */
    #no-more-tables td:before { content: attr(data-title); }
  }

  /* Smartphones (portrait and landscape) ----------- */
  @media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
    body {
      padding: 0;
      margin: 0;
      width: 320px; }
    }

  /* iPads (portrait and landscape) ----------- */
  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
    body {
      width: 495px;
    }
  }

  </style>
  <!--<![endif]-->

<%@ include file="../../../common/header_bottom.jsp" %>

<spring:url value="/collection_report/new" var="submitUrl" />
<form:form class="form-inline" method="POST" modelAttribute="reportForm" action="${submitUrl}">
<div class="row">
  <div class="col-sm-6 form-group" >
    <label class="sr-only" for="dateGenerated">Date of Generation</label>
    <input name="dateGenerated" id="dateGenerated" type="date" class="form-control"
      placeholder="Date Generated" />
  </div>
  <div class="col-sm-6 form-group" >
    <label class="sr-only" for="reporter">Reported By</label>
    <input name="reporter" id="reporter" type="date" class="form-control"
      placeholder="Reported By" />
  </div>
</div>
<div class="row" id="table-div">
  <div class="col-md-12" id="no-more-tables">
    <!-- Table -->
    <table class="table" id="collections-table">
      <thead>
      <tr>
        <th>Locale</th>
        <th>Collection Type</th>
        <th>Collection Date</th>
        <th>Amount</th>
        <th>Base Currency</th>
        <th>Exchange Rate</th>
      </tr>
      </thead>
      <tbody>
        <c:forEach items="${ reportForm.records }" var="record">
        <spring:url value="/collection_report/${reportForm.id}/collection/submit" var="actionUrl" />
        <form:form method="POST" modelAttribute="record" action="${actionUrl}"
          class="navbar-form navbar-left">
        <tr class="collection-row"
          data-locale="${ record.locale }"
          data-type="${ record.collectionType }"
          data-collect="<fmt:formatDate value='${ record.collectionDate }' type='date' pattern='yyyy/MM/dd'/>"
          data-currency="${ record.baseCurrency }"
          data-amount="${ record.amount }"
          data-rate="${ record.exchangeRate }">

          <td data-title="Locale">
            <c:out value="${ record.locale }"/>
            <form:select path="locale" class="form-control" placeholder="Locale" style="width: 100%;">
              <form:option label="-- Please select --" value="" />
              <c:forEach items="${ locals }" var="local">
                <form:option value="${ local.name}" />
              </c:forEach>
            </form:select>
          </td>
          <td data-title="Collection Type">
            <c:out value="${ record.collectionType }"/>
            <form:select path="collectionType" class="form-control" placeholder="Collection Type" style="width: 100%;">
              <form:option label="-- Please select --" value="" />
              <c:forEach items="${ collTypes }" var="collType">
                <form:option value="${ collType.description }" />
              </c:forEach>
            </form:select>
          </td>
          <td data-title="Collection Date">
            <fmt:formatDate type="date" value="${ record.collectionDate }" />
            <input name="collectionDate" id="collectionDate" type="date" class="form-control"
              placeholder="Collection date" />
          </td>
          <td data-title="Amount" class="numeric">
            <fmt:formatNumber type="CURRENCY" value="${ record.amount }" />
            <form:input path="amount" type="text" class="form-control" placeholder="Amount" style="width: 100%;" />
          </td>
          <td data-title="Base Currency">
            <c:out value="${ record.baseCurrency }"/>
            <form:input path="baseCurrency" type="text" class="form-control" placeholder="Base Currency" style="width: 100%;" />
          </td>
          <td data-title="FX Rate">
            <span class="input-group">
            <label class="sr-only" for="exchangeRate">FX Rate</label>
            <form:input path="exchangeRate" type="text" class="form-control" placeholder="FX Rate" style="width: 100%;" />
            <button type="button" class="input-group-addon" >
              <span class="pe-7s-refresh pe-va pe-fw"></span>
            </button>
            </span>
          </td>
        </tr>
        </form:form>
        </c:forEach>
        <tr>
          <td colspan="5"></td>
          <td><input type="submit" value="Add" class="btn btn-primary btn-block" /></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="col-sm-offset-8 col-sm-3 form-group">
    <input type="submit" value="Submit" class="btn btn-primary btn-block" />
  </div>
</div>
</form:form>

<script>
  (function($) {
    $('#reportForm').on("change", ":input", function() {
      var allHasValues = true;
      $(this).parents('.row').find(':input').each(function() {
        if (!$(this).val()) {
          allHasValues = false;
        }
      })
      if( allHasValues ) {
        $("input[type=submit]").prop("disabled", false);
      } else {
        $("input[type=submit]").prop("disabled", true);
      }
    });
  }(jQuery));
</script>

<%@ include file="../../../common/footer.jsp" %>