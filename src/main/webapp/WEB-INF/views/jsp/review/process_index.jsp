<%@ include file="../common/header_top.jsp" %>

<title>Process Index</title>

<%@ include file="../common/header_bottom.jsp" %>

  <div class="row">
      <div class="col-lg-12">
          <h1>Process Index</h1>
          <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Menu</a>
      </div>
  </div>

  <div class="row" id="table-div">
    <div class="form-group">
      <spring:url value="/review/expense" var="expenseUrl" />
      <a href="${ expenseUrl }">Expenses</a>
    </div>
  </div>

<%@ include file="../common/footer.jsp" %>