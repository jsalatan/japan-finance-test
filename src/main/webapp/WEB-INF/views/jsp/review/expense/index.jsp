<%@ include file="../../common/header_top.jsp" %>

<title>Review Expenses</title>

<%@ include file="../../common/header_bottom.jsp" %>
              <div class="row">
                  <div class="col-lg-12">
                      <h1>Expenses</h1>
                      <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Menu</a>
                  </div>
              </div>
        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Locale</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Source</th>
            <th>Payment Date</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ expenses }" var="expense">
            <tr>
              <td><c:out value="${ locale.name }"/></td>
              <td><c:out value="${ expense.expenseType.description }"/></td>
              <td><c:out value="${ expense.amount }"/></td>
              <td><c:out value="${ expense.collectionType.description }"/></td>
              <td><c:out value="${ expense.paymentDate }"/></td>
              <td>
                <button>Approve</button>
              </td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
<%@ include file="../../common/footer.jsp" %>