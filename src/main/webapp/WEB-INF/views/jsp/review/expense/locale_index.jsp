<%@ include file="../../common/header_top.jsp" %>
<title>Locale Index</title>
<%@ include file="../../common/header_bottom.jsp" %>

  <div class="row">
      <div class="col-lg-12">
          <h1>Expenses</h1>
          <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Menu</a>
      </div>
  </div>

  <div class="row" id="table-div">
    <c:forEach items="${localeAccounts}" var="locale">
    <div class="form-group">
      <spring:url value="/review/expense/${locale.id}" var="url" />
      <a href="${ url }"><c:out value="${ locale.name }"/></a>
    </div>
    </c:forEach>
  </div>
<%@ include file="../../common/footer.jsp" %>