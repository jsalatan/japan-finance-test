<%@ include file="../../common/header_top.jsp" %>

<title>Balance Sheet</title>

<%@ include file="../../common/header_bottom.jsp" %>

        <!-- Table -->
        <div>
          <div>Locale: ${ balancesheet.locale }</div>
          <div>Date Range: <c:out value="${ balancesheet.startCoverage }"/> to <c:out value="${ balancesheet.startCoverage }"/></div>
        </div>
        <table class="table">
          <thead>
          <tr>
            <th>Transaction Date</th>
            <th>Expense</th>
            <th>Credit</th>
            <th>Debit</th>
            <th>Balance</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ balancesheet.entries }" var="entry">
            <tr>
              <td><fmt:formatDate type="DATE" value="${entry.date}" /></td>
              <td><c:out value="${ entry.description }"/></td>
              <td><fmt:formatNumber type="CURRENCY" value="${entry.credit}" /></td>
              <td><fmt:formatNumber type="CURRENCY" value="${entry.debit}" /></td>
              <td><fmt:formatNumber type="CURRENCY" value="${entry.balance}" /></td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
<%@ include file="../../common/footer.jsp" %>