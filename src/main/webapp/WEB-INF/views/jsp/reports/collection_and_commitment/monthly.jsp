<%@ include file="../../common/header_top.jsp" %>
<title>Locale Index</title>
<%@ include file="../../common/header_bottom.jsp" %>
  <div id="no-more-tables">
    <!-- Table -->
    <table class="table">
      <thead>
      <tr>
        <th>Member</th>
        <th>Commitment</th>
        <c:forEach items="${ weekEnds }" var="weekEnd">
        <th>
          <fmt:formatDate value="${ weekEnd }" pattern="MM/dd" var="fmtDate" />
          <c:out value="${ fmtDate }"/>
        </th>
        </c:forEach>
        <th>Total Collection</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${ reportRows }" var="reportRow">
        <tr>
          <c:choose>
            <c:when test="${ reportRow.memberId == 'TOTAL'} ">
              <td style="font-weight: bold;"><c:out value="${ reportRow.memberId }"/></td>
              <td style="font-weight: bold;"><c:out value="${ reportRow.commitment }"/></td>
              <td style="font-weight: bold;"><c:out value="${ reportRow.collectionWeek1 }"/></td>
              <td style="font-weight: bold;"><c:out value="${ reportRow.collectionWeek2 }"/></td>
              <td style="font-weight: bold;"><c:out value="${ reportRow.collectionWeek3 }"/></td>
              <td style="font-weight: bold;"><c:out value="${ reportRow.collectionWeek4 }"/></td>
              <td style="font-weight: bold;"><c:out value="${ reportRow.collectionWeek5 }"/></td>
              <td style="font-weight: bold;"><c:out value="${ reportRow.totalCollection }"/></td>
            </c:when>
            <c:otherwise>
              <td><c:out value="${ reportRow.memberId }"/></td>
              <td><c:out value="${ reportRow.commitment }"/></td>
              <td><c:out value="${ reportRow.collectionWeek1 }"/></td>
              <td><c:out value="${ reportRow.collectionWeek2 }"/></td>
              <td><c:out value="${ reportRow.collectionWeek3 }"/></td>
              <td><c:out value="${ reportRow.collectionWeek4 }"/></td>
              <td><c:out value="${ reportRow.collectionWeek5 }"/></td>
              <td><c:out value="${ reportRow.totalCollection }"/></td>
            </c:otherwise>
          </c:choose>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>
<%@ include file="../../common/footer.jsp" %>