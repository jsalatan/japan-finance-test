<%@ include file="../../common/header_top.jsp" %>
<title>Locale Index</title>
<%@ include file="../../common/header_bottom.jsp" %>
  <div id="no-more-tables">
    <!-- Table -->
    <table class="table">
      <thead>
      <tr>
        <th>Type</th>
        <th>Description</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${ projects }" var="project">
        <tr>
          <td>
            <spring:url value="/report/collection_and_commitment/${project.id}" var="url" />
            <a href="${ url }"><c:out value="${ project.description }"/></a>
          </td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>
<%@ include file="../../common/footer.jsp" %>