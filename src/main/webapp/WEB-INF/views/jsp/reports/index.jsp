<%@ include file="../common/header_top.jsp" %>

<title>Process Index</title>

<style>
/*==============================================
    DASHBOARD STYLES    
    =============================================*/
.div-square {
    padding:5px;
    border:3px double #e1e1e1;
    -webkit-border-radius:8px;
   -moz-border-radius:8px;
    border-radius:8px;
    margin:5px;

}

.div-square> a,.div-square> a:hover {
    color:#808080;
     text-decoration:none;
}
</style>

<%@ include file="../common/header_bottom.jsp" %>

  <sec:authentication property="principal.user.locale" var="usrLocale" />
  <div class="row text-center pad-top">
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <h3>Reports</h3>
      </div>
  </div>

  <div class="row text-center pad-top">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="div-square">
        <spring:url value="/reports/accounts" var="url" />
        <spring:url value="/reports/accounts/${ usrLocale }" var="localeUrl" />
        <a href="${ empty usrLocale ? url : localeUrl}">
          <i class="pe-7s-portfolio pe-3x pe-va pe-fw"></i>
          <h4>Locale Accounts</h4>
        </a>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="div-square">
        <spring:url value="/collection" var="url" />
        <spring:url value="/locale_account/${ usrLocale }/collection" var="localeUrl" />
        <a href="${ empty usrLocale ? url : localeUrl}">
          <i class="pe-7s-piggy pe-3x pe-va pe-fw"></i>
          <h4>Collections</h4>
        </a>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="div-square">
        <spring:url value="/expense" var="url" />
        <spring:url value="/locale_account/${ usrLocale }/expense" var="localeUrl" />
        <a href="${ empty usrLocale ? url : localeUrl}">
          <i class="pe-7s-cash pe-3x pe-va pe-fw"></i>
          <h4>Expenses</h4>
        </a>
      </div>
    </div>
  </div>

  <div class="row text-center pad-top">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="div-square">
        <spring:url value="/reports/collection_and_commitment" var="url" />
        <spring:url value="/reports/collection_and_commitment/${ usrLocale }" var="localeUrl" />
        <a href="${ empty usrLocale ? url : localeUrl}">
          <i class="pe-7s-calculator pe-3x pe-va pe-fw"></i>
          <h4>Collections and Commitments</h4>
        </a>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="div-square">
        <spring:url value="/payable" var="url" />
        <spring:url value="/locale_account/${ usrLocale }/payable" var="localeUrl" />
        <a href="${ empty usrLocale ? url : localeUrl}">
          <i class="pe-7s-gleam pe-3x pe-va pe-fw"></i>
          <h4>Payables</h4>
        </a>
      </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <div class="div-square">
        <spring:url value="/reports/balance_sheet/" var="url" />
        <spring:url value="/reports/balance_sheet/${ usrLocale }" var="localeUrl" />
        <a href="${ empty usrLocale ? url : localeUrl}">
          <i class="pe-7s-bookmarks pe-3x pe-va pe-fw"></i>
          <h4>Balance Sheet</h4>
        </a>
      </div>
    </div>
  </div>

  <!-- Disabled 
  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <spring:url value="/collection/project" var="url" />
      <a href="${ url }">Project Collections</a>
    </div>
  </div>
   -->

  <!-- Disabled
  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <c:choose>
        <c:when test="${ empty usrLocale }">
          <spring:url value="/reports/statement" var="url" />
          <a href="${ url }">Statements</a>
        </c:when>
        <c:otherwise>
          <spring:url value="/reports/statement/${ usrLocale }" var="url" />
          <a href="${ url }">Statements</a>
        </c:otherwise>
      </c:choose>
    </div>
  </div>
  -->

  <!-- Disabled
  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
      <spring:url value="/reports/liquidation" var="liquidationUrl" />
      <a href="${ liquidationUrl }">Liquidation Report</a>
    </div>
  </div>
  -->

<%@ include file="../common/footer.jsp" %>