<%@ include file="../../common/header_top.jsp" %>
<title>Locale Index</title>
<%@ include file="../../common/header_bottom.jsp" %>
  <div class="row">
    <div class="col-lg-12">
      <h3>Liquidation Report</h3>
    </div>
  </div>
  
  <div class="row" id="table-div">
    <c:forEach items="${months}" var="month">
    <div class="form-group">
      <spring:url value="/reports/liquidation/${localeId}/${month}" var="url" />
      <a href="${ url }"><c:out value="${ month }"/></a>
    </div>
    </c:forEach>
  </div>
<%@ include file="../../common/footer.jsp" %>