<%@ include file="../../common/header_top.jsp" %>

<title>Balance Sheet</title>

<%@ include file="../../common/header_bottom.jsp" %>

        <!-- Table -->
        <div>
          <div>Locale: ${ locale.name }</div>
          <div>Statement Date: <c:out value="${ statement.statementYm }"/></div>
        </div>
        <div>
          <div>Starting Balance: ${ statement.startingBalance }</div>
        </div>
        <table class="table">
          <thead>
          <tr>
            <th>Transaction Date</th>
            <th>Description</th>
            <th>Credit</th>
            <th>Debit</th>
            <th>Balance</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ statement.collections }" var="collection">
            <tr>
              <td><c:out value="${ collection.transactionDate }"/></td>
              <td><c:out value="${ collection.type.description }"/></td>
              <td><c:out value="${ collection.amount }"/></td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
        <table class="table">
          <thead>
          <tr>
            <th>Transaction Date</th>
            <th>Description</th>
            <th>Credit</th>
            <th>Debit</th>
            <th>Balance</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ statement.expenses }" var="expense">
            <tr>
              <td><c:out value="${ expense.transactionDate }"/></td>
              <td><c:out value="${ expense.expenseType.description }"/></td>
              <td><c:out value="${ expense.amount }"/></td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
        <div>
          <div>Ending Balance: ${ statement.endingBalance }</div>
        </div>
<%@ include file="../../common/footer.jsp" %>