<%@ include file="../../common/header_top.jsp" %>
<title>Locale Index</title>
<%@ include file="../../common/header_bottom.jsp" %>
  <div class="row">
    <div class="col-lg-12">
      <h3>Liquidation Report</h3>
    </div>
  </div>
  
  <div class="row" id="table-div">
    <span><c:out value="${ msg }"/></span>
    <c:forEach items="${localeAccounts}" var="locale">
    <div class="form-group">
      <spring:url value="/reports/liquidation/${locale.id}" var="url" />
      <a href="${ url }"><c:out value="${ locale.name }"/></a>
    </div>
    </c:forEach>
  </div>
<%@ include file="../../common/footer.jsp" %>