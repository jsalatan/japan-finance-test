<%@ include file="../../common/header_top.jsp" %>

<title>Balance Sheet</title>

<%@ include file="../../common/header_bottom.jsp" %>

        <!-- Table -->
        <div>
          <div>Locale: ${ localeAccount.name }</div>
          <div>Date Range: <fmt:formatDate type="DATE" value="${start}" /> to <fmt:formatDate type="DATE" value="${end}" /></div>
        </div>
        <table class="table">
          <thead>
          <tr>
            <th>Account/Collection Type</th>
            <th>Balance</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ accounts }" var="entry">
            <tr>
              <td><c:out value="${ entry.key }"/></td>
              <td><fmt:formatNumber type="CURRENCY" value="${entry.value}" /></td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
<%@ include file="../../common/footer.jsp" %>