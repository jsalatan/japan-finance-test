<%@ include file="../common/header_top.jsp" %>
    
<title>Expenses</title>

<%@ include file="../common/header_bottom.jsp" %>
              <div class="row">
                  <div class="col-lg-12">
                      <h1>Ledger</h1>
                      <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Menu</a>
                  </div>
              </div>
			  <!-- Table -->
			  <table class="table">
			    <thead>
			    <tr>
			      <th>Kind</th>
			      <th>Amount</th>
			      <th>Transaction Date</th>
			      <th>Balance</th>
			    </tr>
			    </thead>
			    <tbody>
			      <c:forEach items="${ entries }" var="entry">
			      <tr>
			        <td><c:out value="${ entry.debitOrCredit }"/></td>
			        <td><c:out value="${ entry.amount }"/></td>
			        <td><c:out value="${ entry.transactionDate }"/></td>
			        <td><c:out value="${ entry.balance }"/></td>
			      </tr>
			      </c:forEach>
			      <tr>
			        <td colspan="2">
			          <spring:url value="/ledger/generate" var="ledgerUrl" />
			          <a href="${ ledgerUrl }">Generate</a>
			        </td>
			      </tr>
			    </tbody>
			  </table>
<%@ include file="../common/footer.jsp" %>