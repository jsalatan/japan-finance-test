<%@ include file="../../common/header_top.jsp" %>
<title>Payable : ${ payable.description }</title>

  <style>
    * { 
      margin: 0; 
      padding: 0; 
    }
    body { 
      font: 14px/1.4 Georgia, Serif; 
    }
    #page-wrap {
      margin: 50px;
    }
    p {
      margin: 20px 0; 
    }
    
      /* 
      Generic Styling, for Desktops/Laptops 
      */
      table { 
        width: 100%; 
        border-collapse: collapse; 
      }
      /* Zebra striping */
      tr:nth-of-type(odd) { 
        background: #eee; 
      }
      th { 
        background: #333; 
        color: white; 
        font-weight: bold; 
      }
      td, th { 
        padding: 6px; 
        border: 1px solid #ccc; 
        text-align: left; 
      }
  </style>
  
  <!--[if !IE]><!-->
  <style>

  /*
  Max width before this PARTICULAR table gets nasty
  This query will take effect for any screen smaller than 760px
  and also iPads specifically.
  */
  @media
  only screen and (max-width: 760px),
  (min-device-width: 768px) and (max-device-width: 1024px)  {
  
    /* Force table to not be like tables anymore */
    #no-more-tables table, 
    #no-more-tables thead, 
    #no-more-tables tbody, 
    #no-more-tables th, 
    #no-more-tables td, 
    #no-more-tables tr { 
      display: block; 
    }
  
    /* Hide table headers (but not display: none;, for accessibility) */
    #no-more-tables thead tr { 
      position: absolute;
      top: -9999px;
      left: -9999px;
    }
  
    #no-more-tables tr { border: 1px solid #ccc; }
  
    #no-more-tables td { 
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee; 
      position: relative;
      padding-left: 50%; 
      white-space: normal;
      text-align:left;
    }
  
    #no-more-tables td:before { 
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%; 
      padding-right: 10px; 
      white-space: nowrap;
      text-align:left;
    }
  
    /*
    Label the data
    */
    #no-more-tables td:before { content: attr(data-title); }
  }

  /* Smartphones (portrait and landscape) ----------- */
  @media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
    body {
      padding: 0;
      margin: 0;
      width: 320px; }
    }

  /* iPads (portrait and landscape) ----------- */
  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
    body {
      width: 495px;
    }
  }

  </style>
  <!--<![endif]-->

<%@ include file="../../common/header_bottom.jsp" %>
<div class="row">
  <div class="col-sm-2">
    Payable: 
  </div>
  <div class="col-sm-2">
    ${ payable.description }
  </div>
  <div class="col-sm-8">&nbsp;</div>
</div>
<div class="row">
  <div class="col-sm-12">
      <div id="no-more-tables">
        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>No.</th>
            <th>Payment Date</th>
            <th>Amount</th>
            <th>Received By</th>
            <th>Paid By</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ payable.payments }" var="payment" varStatus="index">
            <tr>
              <td data-title="Number"> 
                <spring:url value="/locale_account/${ locale.id }/payable/${ payable.id }/payment/${ payment.id }" var="url" />
                <a href="${ url }">${ index.count }</a>
              </td>
              <td data-title="Payment Date"><fmt:formatDate type="date" value="${ payment.paymentDate }" /></td>
              <td data-title="Amount"><fmt:formatNumber type="CURRENCY" value="${ payment.amount }" /></td>
              <td data-title="Received By"><c:out value="${ payment.receivedBy }"/></td>
              <td data-title="Paid By"><c:out value="${ payment.paidBy }"/></td>
            </tr>
            </c:forEach>
            <tr>
              <td>
                Payable Total: 
              </td>
              <td colspan="4">
                <fmt:formatNumber type="CURRENCY" value="${ payable.total }" />
              </td>
            </tr>
            <tr>
              <td>
                Payments Total: 
              </td>
              <td colspan="4">
                <fmt:formatNumber type="CURRENCY" value="${ payable.paymentsTotal }" />
              </td>
            </tr>
            <tr>
              <td>
                Balance: 
              </td>
              <td colspan="4">
                <fmt:formatNumber type="CURRENCY" value="${ payable.balance }" />
              </td>
            </tr>
            <tr>
              <td colspan="5">
                <spring:url value="/locale_account/${ locale.id }/payable/${ payable.id }/payment/new" var="newUrl" />
                <a href="${ newUrl }">Add Payment</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
</div>
<%@ include file="../../common/footer.jsp" %>