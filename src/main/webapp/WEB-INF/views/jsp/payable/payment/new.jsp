<%@ include file="../../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
  });
</script>

<title>Add Payment</title>

<%@ include file="../../common/header_bottom.jsp" %>

<spring:url value="/locale_account/${locale.id}/payable/${payable.id}/payment/new" var="submitUrl" />
<form:form method="POST" modelAttribute="formObj" action="${ submitUrl }">
<div class="row">
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="paymentDate">Payment Date</label>
    <form:input path="paymentDate" type="date" class="form-control" placeholder="Payment Date" style="width: 100%;" />
  </div>
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="amount">Amount</label>
    <form:input path="amount" type="text" class="form-control" placeholder="Amount" style="width: 100%;" />
  </div>
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="receivedBy">Received By</label>
    <form:input path="receivedBy" type="search" clas="form-control" placeholder="Received By" style="width: 100%;" 
      list="users"/>
    <datalist id="users">
     <c:forEach items="${ users }" var="user">
     <option value="${ user.username }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="paidBy">Paid By</label>
    <form:input path="paidBy" type="search" clas="form-control" placeholder="Paid By" style="width: 100%;" 
      list="paidByUsers"/>
    <datalist id="paidByUsers">
     <c:forEach items="${ users }" var="user">
     <option value="${ user.username }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-2">
    <input type="submit" value="Submit" class="btn btn-primary" style="width: 100%;" />
  </div>
</div>
</form:form>
<%@ include file="../../common/footer.jsp" %>