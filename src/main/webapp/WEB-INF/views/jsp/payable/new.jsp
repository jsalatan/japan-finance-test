<%@ include file="../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
  });
</script>

<title>Add Payable</title>

<%@ include file="../common/header_bottom.jsp" %>

<spring:url value="/locale_account/${locale.id}/payable/new" var="submitUrl" />
<form:form method="POST" modelAttribute="formObj" action="${ submitUrl }">
<div class="row">
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="description">Description</label>
    <form:input path="description" type="search" class="form-control" placeholder="Description" style="width: 100%;"/>
  </div>
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="lender">Lender</label>
    <form:input path="lender" type="search" clas="form-control" placeholder="Lender" style="width: 100%;" 
      list="users"/>
    <datalist id="users">
     <c:forEach items="${ users }" var="user">
     <option value="${ user.username }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="dateBorrowed">Date Borrowed</label>
    <form:input path="dateBorrowed" type="date" class="form-control" placeholder="Date Borrowed" style="width: 100%;" />
  </div>
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="total">Total</label>
    <form:input path="total" type="text" class="form-control" placeholder="Total" style="width: 100%;" />
  </div>
  <div class="col-sm-2">
    <input type="submit" value="Submit" class="btn btn-primary" style="width: 100%;" />
    <c:url var="url" value="/locale_account/${locale.id}/payable/new"/>
  </div>
</div>
</form:form>
<%@ include file="../common/footer.jsp" %>