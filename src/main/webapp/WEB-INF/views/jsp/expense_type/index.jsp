<%@ include file="../common/header_top.jsp" %>
<title>Expense Types</title>

<%@ include file="../common/header_bottom.jsp" %>
        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Description</th>
            <th>Default Source Of Fund</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ expenseTypes }" var="expenseType">
            <tr>
              <td><c:out value="${ expenseType.description }"/></td>
              <td><c:out value="${ expenseType.defaultSourceOfFund.description }"/></td>
            </tr>
            </c:forEach>
            <tr>
              <td colspan="2">
                <spring:url value="/expense_type/new" var="newUrl" />
                <a href="${ newUrl }">Add Expense Type</a>
              </td>
            </tr>
          </tbody>
        </table>

<%@ include file="../common/footer.jsp" %>