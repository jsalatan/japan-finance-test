<%@ include file="../common/header_top.jsp" %>
<title>Add Expense Type</title>

<%@ include file="../common/header_bottom.jsp" %>

        <!-- Table -->
        <spring:url value="/expense_type/new" var="submitUrl" />
        <form:form method="POST" modelAttribute="expenseType" action="${submitUrl}">
        <table class="table">
          <thead>
          <tr>
            <th>Description</th>
            <th>Default Source of Funds</th>
          </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <form:input path="description" type="text" class="form-control"
                  id="description" placeholder="Description" />
              </td>
              <td>
                <form:select path="defaultSourceOfFund" class="form-control">
                  <form:option value="" label="--- Select ---"/>
                  <form:options items="${collectionTypes}"/>
                </form:select>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                  <input type="submit" value="Submit"/>
              </td>
            </tr>
          </tbody>
        </table>
      </form:form>

<%@ include file="../common/footer.jsp" %>