<%@ include file="../common/header_top.jsp" %>
<title>Source Of Funds</title>

<%@ include file="../common/header_bottom.jsp" %>

        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ collectionTypes }" var="collectionType">
            <tr>
              <td><c:out value="${ collectionType.description }"/></td>
            </tr>
            </c:forEach>
            <tr>
              <td colspan="2">
                <spring:url value="/collection_type/new" var="newUrl" />
                <c:url var="newUrl" value="collection_type/new"/>

                <a href="${ newUrl }">Add Source</a>

              </td>
            </tr>
          </tbody>
        </table>

<%@ include file="../common/footer.jsp" %>