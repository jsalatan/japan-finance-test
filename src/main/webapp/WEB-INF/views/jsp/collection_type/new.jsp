<%@ include file="../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
    var availableTags = [];
    <c:forEach items="${ scopes }" var="scope">
      availableTags.push("<c:out value="${scope}" />");
    </c:forEach>
    $("input[name='scope']").autocomplete({
      source: availableTags,
      minLength: 0,
      focus: function() {            
        $(this).autocomplete("search");
      }
    });
    
  });
</script>

<title>Add Source Of Funds</title>

<%@ include file="../common/header_bottom.jsp" %>

<spring:url value="/collection_type/new" var="submitUrl" />
<form:form method="POST" modelAttribute="source" action="${submitUrl}">
<div class="row">
  <div class="col-sm-3 form-group ui-widget" >
    <label class="sr-only" for="description">Description</label>
    <form:input path="description" type="text" class="form-control" placeholder="Description" style="width: 100%;"/>
  </div>
  <div class="col-sm-3 form-group" >
    <label class="sr-only" for="scope">Scope</label>
    <form:input path="scope" type="text" class="form-control" placeholder="Scope" style="width: 100%;" />
  </div>
  <div class="col-sm-3 form-group" >
    <label class="sr-only" for="isRemittable">To be remitted?</label>
    <form:checkbox path="isRemittable" class="form-control" placeholder="To Be Remitted" style="width: 100%;" />
  </div>
  <div class="col-sm-3">
    <input type="submit" value="Submit" class="btn btn-primary" />
  </div>
</div>

</form:form>

<%@ include file="../common/footer.jsp" %>