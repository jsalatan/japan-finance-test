<%@ include file="../../common/header_top.jsp" %>

<title>Expenses</title>

<%@ include file="../../common/header_bottom.jsp" %>

        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Name</th>
            <th>Process URL</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ processes }" var="process">
            <tr>
              <td>
                <spring:url value="/admin/bpm/${ process.id }/edit" var="editUrl" />
                <a href="${ editUrl }">
                  <c:out value="${ process.name }"/>
                </a>
              </td>
              <td><c:out value="${ process.controlUrl }"/></td>
            </tr>
            </c:forEach>
            <tr>
              <td colspan="2">
                <spring:url value="/admin/bpm/new" var="newUrl" />
                <a href="${ newUrl }">Add Process</a>
              </td>
            </tr>
          </tbody>
        </table>
<%@ include file="../../common/footer.jsp" %>