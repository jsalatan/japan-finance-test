<%@ include file="../../common/header_top.jsp" %>
<title>Add Process</title>

<%@ include file="../../common/header_bottom.jsp" %>

<spring:url value="/admin/bpm/new" var="submitUrl" />
<form:form method="POST" modelAttribute="formObj" action="${submitUrl}">
<div class="row">
  <div class="col-sm-3 form-group ui-widget" >
    <label class="sr-only" for="name">Name</label>
    <form:input path="name" type="text" class="form-control" placeholder="Name" style="width: 100%;"/>
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="controlUrl">Control URL</label>
    <form:input path="controlUrl" type="text" class="form-control" placeholder="Control URL" style="width: 100%;" />
  </div>
  <div class="col-sm-1">
    <input type="submit" value="Submit" class="btn btn-primary btn-block" />
  </div>
</div>
</form:form>
<%@ include file="../../common/footer.jsp" %>