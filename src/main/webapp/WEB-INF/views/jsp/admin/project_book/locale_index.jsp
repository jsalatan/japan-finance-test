<%@ include file="../../common/header_top.jsp" %>
<title>Project Books : Locale Index</title>
<%@ include file="../../common/header_bottom.jsp" %>
  <div class="row">
    <div class="col-lg-12">
      <h3>Project Support - Locales</h3>
    </div>
  </div>
  
  <div class="row" id="table-div">
    <span><c:out value="${ msg }"/></span>
    <c:forEach items="${localeAccounts}" var="localeAccount">
    <div class="form-group">
      <spring:url value="/locale_account/${localeAccount.id}/project_book" var="url" />
      <a href="${ url }"><c:out value="${ localeAccount.name }"/></a>
    </div>
    </c:forEach>
  </div>
<%@ include file="../../common/footer.jsp" %>