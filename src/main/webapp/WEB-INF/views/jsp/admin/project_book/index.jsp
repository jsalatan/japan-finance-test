<%@ include file="../../common/header_top.jsp" %>

<title>Project Index</title>

<%@ include file="../../common/header_bottom.jsp" %>
  <div class="row">
    <div class="col-lg-12">
      <h3>Project Support</h3>
    </div>
  </div>

  <!-- Table -->
  <div class="row">
      <c:forEach items="${ projectBooks }" var="projectBook">
      <div class="col-sm-3">
        <span>Project: </span>
        <spring:url value="/locale_account/${localeAccount.id}/project_book/${projectBook.id}/edit" var="url" />
        <a href="${ url }" >
          <c:out value="${ projectBook.projectDesc }"/>
        </a>
      </div>
      <sec:authorize access="isAuthenticated() and hasAnyRole('ROLE_LOCALE_COORDINATOR')">
      <div class="col-sm-3">
        <span>Target: </span>
        <fmt:formatNumber type="CURRENCY" value="${ projectBook.target }" />
      </div>
      <div class="col-sm-3">
        <span>Total Commitment: </span>
        <spring:url value="/locale_account/${localeAccount.id}/project_book/${projectBook.id}/commitments" var="url" />
        <a href="${ url }" >
          <fmt:formatNumber type="CURRENCY" value="${ projectBook.totalCommitment }" />
        </a>
      </div>
      <div class="col-sm-3">
        <span>Total Collection: </span>
        <fmt:formatNumber type="CURRENCY" value="${ projectBook.totalCollection }" />
      </div>
      </sec:authorize>
      </c:forEach>
  </div>
  <sec:authorize access="isAuthenticated() and hasAnyRole('ROLE_LOCALE_COORDINATOR')">
  <div class="row">
    <div class="col-sm-12">
      <spring:url value="/locale_account/${localeAccount.id}/project_book/new" var="newUrl" />
      <a href="${ newUrl }">Support another project</a>
    </div>
  </div>
  </sec:authorize>

<%@ include file="../../common/footer.jsp" %>