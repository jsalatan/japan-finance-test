<%@ include file="../../common/header_top.jsp" %>

<title>New Project Support</title>

<script>
$(document).ready(function() {
  $("#share, #projectDesc").on("input", function() {
    var percentShare = $("#share").val();
    var projDesc = $("#projectDesc").val();
    var target = $(this).parents(".row").find("#projects").find("*[value='" + projDesc + "']").data('target');
    $("#totalTarget").val(target);
    
    if (target) {
      var share = target * (percentShare / 100);
      $(this).parents(".row").find("#target").val(share);
    }
  });
});
</script>

<%@ include file="../../common/header_bottom.jsp" %>

<div class="row">
  <div class="col-lg-12">
    <h3>New Project Support for <c:out value="${ localeAccount.name }"/></h3>
  </div>
</div>

<spring:url value="/locale_account/${localeAccount.id}/project_book/new" var="submitUrl" />
<form:form method="POST" modelAttribute="projectBook" action="${submitUrl}">
<div class="row">
  <div class="col-sm-3 form-group ui-widget" >
    <label class="sr-only" for="projectDesc">Project</label>
    <form:input path="projectDesc" type="search" class="form-control" placeholder="Project" style="width: 100%;"
      list="projects"/>
    <datalist id="projects">
     <c:forEach items="${ projects }" var="project">
     <option value="${ project.description }" data-target="${ project.targetAmt }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-3 form-group" >
    <input type="text" id="totalTarget" class="form-control" placeholder="Total Target" readonly />
  </div>
  <div class="col-sm-2 form-group" >
    <div class="input-group">
    <label class="sr-only" for="target">% Share</label>
    <input id="share" type="text" class="form-control" placeholder="% Share" />
    <span class="input-group-addon">%</span>
    </div>
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="target">Target</label>
    <form:input path="target" type="text" class="form-control" placeholder="Target" style="width: 100%;" />
  </div>
  <div class="col-sm-2">
    <input type="submit" value="Submit" class="btn btn-primary" style="width: 100%;" />
  </div>
</div>
</form:form>
<%@ include file="../../common/footer.jsp" %>