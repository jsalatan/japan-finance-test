<%@ include file="../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
    //$( "#paymentDate" ).datepicker();

    $('#expenseForm').on("change", ":input", function() {
      var allHasValues = true;
      $(this).parents('tbody').find(':input').each(function() {
        if (!$(this).val()) {
          allHasValues = false;
        }
      })
      if( allHasValues ) {
        $("input[type=submit]").prop("disabled", false);
      } else {
        $("input[type=submit]").prop("disabled", true);
      }
    });

    $('#expenseForm').on("change", "#collectionType", function() {
      var form = $(this).parents('form:first');

      <c:url var="url" value="/locale_account/${locale.id}/expense/check_balance"/>
      $.ajax({
        type : "GET",
        url : "${ url }",
        data: form.serialize(),
        success : function(data) {
          if (data == "success") {
            $("input[type=submit]").prop("disabled", false);
          } else {
            $("input[type=submit]").prop("disabled", true);
            $("#warning-dialog").fadeIn(1000).delay(500).fadeOut(1000);
          }
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    });

  });
</script>

<title>Add expense</title>

<%@ include file="../common/header_bottom.jsp" %>

        <spring:url value="/locale_account/${locale.id}/expense/${ expenseForm.id }/edit" var="submitUrl" />
        <form:form method="POST" modelAttribute="expenseForm" action="${submitUrl}">

          <!-- Table -->
          <table class="table">
            <thead>
            <tr>
              <th>Expense Type</th>
              <th>Amount</th>
              <th>Payment Date</th>
              <th>Source Of Funds</th>
            </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <form:select path="expenseType" class="form-control">
                    <form:option value="" label="--- Select ---"/>
                    <form:options items="${expenseTypes}"/>
                  </form:select>
                </td>
                <td>
                  <form:input path="amount" type="text" class="form-control"
                    id="amount" placeholder="Amount" />
                </td>
                <td>
                  <fmt:formatDate value="${ expenseForm.paymentDate }" pattern="MM/dd/yyyy" var="fmtDate" />
                  <input name="transactionDate" id="transactionDate" type="date" class="form-control" value="${ fmtDate }"
                    placeholder="Date Of Payment" readonly/>
                </td>
                <td>
                  <%-- will not use multiple select; if expense will use multiple collection account, create a new expense for each --%>
                  <%--  form:select path="collectionType" multiple="multiple" class="form-control" --%>
                  <form:select path="collectionType" class="form-control">
                    <form:option value="" label="--- Select ---"/>
                    <form:options items="${fundSources}"/>
                  </form:select>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                    <input type="submit" value="Submit" class="btn" />
                    <span id="warning-dialog" class="alert alert-warning" style="display: none;">Insufficient funds.</span>
                </td>
              </tr>
            </tbody>
          </table>
        </form:form>
<%@ include file="../common/footer.jsp" %>