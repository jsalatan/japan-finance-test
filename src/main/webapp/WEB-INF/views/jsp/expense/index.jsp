<%@ include file="../common/header_top.jsp" %>
<title>Expenses</title>

  <style>
    * { 
      margin: 0; 
      padding: 0; 
    }
    body { 
      font: 14px/1.4 Georgia, Serif; 
    }
    #page-wrap {
      margin: 50px;
    }
    p {
      margin: 20px 0; 
    }
    
      /* 
      Generic Styling, for Desktops/Laptops 
      */
      table { 
        width: 100%; 
        border-collapse: collapse; 
      }
      /* Zebra striping */
      tr:nth-of-type(odd) { 
        background: #eee; 
      }
      th { 
        background: #333; 
        color: white; 
        font-weight: bold; 
      }
      td, th { 
        padding: 6px; 
        border: 1px solid #ccc; 
        text-align: left; 
      }
  </style>
  
  <!--[if !IE]><!-->
  <style>

  /*
  Max width before this PARTICULAR table gets nasty
  This query will take effect for any screen smaller than 760px
  and also iPads specifically.
  */
  @media
  only screen and (max-width: 760px),
  (min-device-width: 768px) and (max-device-width: 1024px)  {
  
    /* Force table to not be like tables anymore */
    #no-more-tables table, 
    #no-more-tables thead, 
    #no-more-tables tbody, 
    #no-more-tables th, 
    #no-more-tables td, 
    #no-more-tables tr { 
      display: block; 
    }
  
    /* Hide table headers (but not display: none;, for accessibility) */
    #no-more-tables thead tr { 
      position: absolute;
      top: -9999px;
      left: -9999px;
    }
  
    #no-more-tables tr { border: 1px solid #ccc; }
  
    #no-more-tables td { 
      /* Behave  like a "row" */
      border: none;
      border-bottom: 1px solid #eee; 
      position: relative;
      padding-left: 50%; 
      white-space: normal;
      text-align:left;
    }
  
    #no-more-tables td:before { 
      /* Now like a table header */
      position: absolute;
      /* Top/left values mimic padding */
      top: 6px;
      left: 6px;
      width: 45%; 
      padding-right: 10px; 
      white-space: nowrap;
      text-align:left;
    }
  
    /*
    Label the data
    */
    #no-more-tables td:before { content: attr(data-title); }
  }

  /* Smartphones (portrait and landscape) ----------- */
  @media only screen
  and (min-device-width : 320px)
  and (max-device-width : 480px) {
    body {
      padding: 0;
      margin: 0;
      width: 320px; }
    }

  /* iPads (portrait and landscape) ----------- */
  @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
    body {
      width: 495px;
    }
  }

  </style>
  <!--<![endif]-->

<%@ include file="../common/header_bottom.jsp" %>
      <div id="no-more-tables">
        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Locale</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Source</th>
            <th>Due Date</th>
            <th>Payment Date</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ expenses }" var="expense">
            <tr>
              <td data-title="Locale"><c:out value="${ locale.name }"/></td>
              <td data-title="Expense Type"><c:out value="${ expense.expenseType.description }"/></td>
              <td data-title="Amount">
                <spring:url value="/locale_account/${locale.id}/expense/${ expense.id }/edit" var="editUrl" />
                <a href="${ editUrl }">
                  <fmt:formatNumber type="CURRENCY" value="${expense.amount}" />
                </a>
              </td>
              <td data-title="Source"><c:out value="${ expense.sourceOfFunds.description }"/></td>
              <td data-title="Due Date"><fmt:formatDate type="date" value="${expense.dueDate}" /></td>
              <td data-title="Payment Date"><fmt:formatDate type="date" value="${expense.transactionDate}" /></td>
              <td data-title="Action">
                <spring:url value="/locale_account/${locale.id}/expense/${ expense.id }/edit" var="editUrl" />
                <a href="${ editUrl }">
                  <i class="pe-7s-note pe-lg" ></i>
                </a>
                <spring:url value="/locale_account/${locale.id}/expense/${ expense.id }/delete" var="url" />
                <a href="${ url }">
                  <i class="pe-7s-trash pe-lg" ></i>
                </a>
              </td>
            </tr>
            </c:forEach>
            <tr>
              <td colspan="2">
                <spring:url value="/locale_account/${locale.id}/expense/new" var="newUrl" />
                <a href="${ newUrl }">Add Expense</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <sec:authorize access="isAuthenticated() and hasAnyRole('ROLE_LOCALE_TREASURER')">
      <div class="row">
        <table>
          <tr>
            <td colspan="2">
              <spring:url value="/locale_account/${locale.id}/expense/upload" var="uploadUrl" />
              <form:form modelAttribute="fileuploadForm" action="${ uploadUrl }" method="POST" enctype="multipart/form-data">
                <label for="file">File</label>
                <form:input path="file" type="file" />
                <p><button type="submit">Upload</button></p>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
              </form:form>
            </td>
          </tr>
        </table>
      </div>
      </sec:authorize>
<%@ include file="../common/footer.jsp" %>