<%@ include file="../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
    $("#amount").on("input", function(event) {

      var form = $(this).parents('form:first');
      var allHasValues = true;
      $(this).parents('.row').find(':input').each(function() {
        if (!$(this).val()) {
          allHasValues = false;
        }
      })
      if( allHasValues ) {
        <c:url var="url" value="/locale_account/${locale.id}/expense/check_balance"/>
        $.ajax({
          type : "GET",
          url : "${ url }",
          data: form.serialize(),
          success : function(data) {
            
            if (data == "sufficient") {
              $("input[type=submit]").prop("disabled", false);
              $("#borrow").addClass("disabled");
            } else {
              $("input[type=submit]").prop("disabled", true);
              $("#borrow").removeClass("disabled");
              $("#warning-dialog").fadeIn(1000).delay(500).fadeOut(1000);
            }
          },
          error : function(e) {
            console.log("ERROR: ", e);
          }
        });
      }
    });
    
    $('#expenseForm').on("change", "#sourceOfFunds", function() {
        var form = $(this).parents('form:first');
        
        <c:url var="url" value="/locale_account/${locale.id}/expense/check_balance"/>
        $.ajax({
          type : "GET",
          url : "${ url }",
          data: form.serialize(),
          success : function(balance) {
            
            var amount = $("#amount").val();
            $("#availableFunds").val(balance);
            if (balance >= amount) {
              $("input[type=submit]").prop("disabled", false);
              $("#borrow").addClass("disabled");
            } else {
              $("input[type=submit]").prop("disabled", true);
              $("#borrow").removeClass("disabled");
              $("#warning-dialog").fadeIn(1000).delay(500).fadeOut(1000);
            }
          },
          error : function(e) {
            console.log("ERROR: ", e);
          }
        });
    });
    
  });
</script>

<title>Add expense</title>

<%@ include file="../common/header_bottom.jsp" %>

<spring:url value="/locale_account/${locale.id}/expense/new" var="submitUrl" />
<form:form method="POST" modelAttribute="expenseForm" action="${submitUrl}">
<div class="row">
  <div class="col-sm-4 form-group ui-widget" >
    <label class="sr-only" for="expenseType">Expense Type</label>
    <form:input path="expenseType" type="search" class="form-control" placeholder="Expense Type" style="width: 100%;"
      list="expense-types"/>
    <datalist id="expense-types">
     <c:forEach items="${ expenseTypes }" var="expenseType">
     <option value="${ expenseType.description }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-4 form-group" >
    <label class="sr-only" for="amount">Amount</label>
    <form:input path="amount" type="text" class="form-control" placeholder="Amount" style="width: 100%;" />
  </div>
  <div class="col-sm-4 form-group" >
    <label class="sr-only" for="transactionDate">Due Date</label>
    <form:input path="dueDate" type="date" class="form-control" placeholder="Due Date" style="width: 100%;" />
  </div>
</div>
<div class="row">
  <div class="col-sm-4 form-group" >
    <label class="sr-only" for="transactionDate">Payment Date</label>
    <form:input path="transactionDate" type="date" class="form-control" placeholder="Payment Date" style="width: 100%;" />
  </div>
  <div class="col-sm-4 form-group ui-widget" >
    <label class="sr-only" for="remittedBy">Remitted By</label>
    <form:input path="remittedBy" type="search" class="form-control" placeholder="Remitted By" style="width: 100%;"
      list="users"/>
    <datalist id="users">
     <c:forEach items="${ users }" var="user">
     <option value="${ user.username }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-4 form-group ui-widget" >
    <label class="sr-only" for="sourceOfFunds">Source of Funds</label>
    <form:input path="sourceOfFunds" type="search" class="form-control" placeholder="Source of Funds" style="width: 100%;"
      list="collection-types"/>
    <datalist id="collection-types">
     <c:forEach items="${ fundSources }" var="fundSource">
     <option value="${ fundSource.type }"/>
     </c:forEach>
    </datalist>
  </div>
</div>
<div class="row">
  <div class="col-sm-12 form-group ui-widget" >
    <input name="availableFunds" class="form-control" id="availableFunds" style="width: 100%;" type="text" value="" readonly/>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <input type="submit" value="Submit" class="btn btn-primary" style="width: 100%;" disabled/>
    <c:url var="url" value="/locale_account/${locale.id}/borrow/new"/>
    <!-- FIXME: Disabled for 0.0.1a release
    <a href="${ url }" id="borrow" class="btn btn-primary disabled">Borrow</a>
     -->
  </div>
  <!-- 
  <div class="col-sm-2">
    <span id="warning-dialog" class="alert alert-warning" style="display: none;">Insufficient funds.</span>
  </div>
   -->
</div>
</form:form>
<%@ include file="../common/footer.jsp" %>