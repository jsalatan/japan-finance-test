<%@ include file="../common/header_top.jsp" %>
<title>Add Locale Account</title>

<%@ include file="../common/header_bottom.jsp" %>
        <!-- Table -->
        <spring:url value="/locale_account/new" var="submitUrl" />
        <form:form method="POST" modelAttribute="account" action="${submitUrl}">
        <table class="table">
          <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
          </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <form:input path="id" type="text" class="form-control"
                  id="id" placeholder="ID" />
              </td>
              <td>
                <form:input path="name" type="text" class="form-control"
                  id="name" placeholder="Description" />
              </td>
            </tr>
            <tr>
              <td colspan="2">
                  <input type="submit" value="Submit"/>
              </td>
            </tr>
          </tbody>
        </table>
      </form:form>

<%@ include file="../common/footer.jsp" %>