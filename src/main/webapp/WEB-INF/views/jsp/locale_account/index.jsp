<%@ include file="../common/header_top.jsp" %>
<title>Locale Account</title>

<%@ include file="../common/header_bottom.jsp" %>
        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Value</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ accounts }" var="account">
            <tr>
              <td><c:out value="${ account.id }"/></td>
              <td><c:out value="${ account.name }"/></td>
            </tr>
            </c:forEach>
            <tr>
              <td colspan="2">
                <spring:url value="/locale_account/new" var="newUrl" />
                <a href="${ newUrl }">Add Locale Account</a>
              </td>
            </tr>
          </tbody>
        </table>

<%@ include file="../common/footer.jsp" %>