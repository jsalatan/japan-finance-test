<%@ include file="../common/header_top.jsp" %>
<title>Source Of Funds</title>

    <!-- Menu Toggle Script -->
    <script>
    function addRow() {
      <c:url var="newUrl" value="collection/new_row"/>

      $.ajax({
        type : "GET",
        url : "${ newUrl }",
        success : function(data) {
          $(data).insertAfter($('div.row').last());
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    }

    $(document).ready(function() {
      $("#table-div").on("click", ".submit", function() {
        var form = $(this).parents('form:first');

        <c:url var="url" value="collection/submit_collection"/>
        $.ajax({
          type : "POST",
          url : "${ url }",
          data: form.serialize(),
          success : function(data) {
            console.log("SUCCESS: ", data);
          },
          error : function(e) {
            console.log("ERROR: ", e);
          }
        });
      });

      $(".delete").on("click", function() {
        var collectionId = $(this).parent().find("input[name=collectionId]").val();
        <c:url var="url" value="collection/delete"/>
        $.ajax({
          type : "POST",
          url : "${ url }",
          data: {collectionId: collectionId},
          success : function(data) {
            console.log("SUCCESS: ", data);
          },
          error : function(e) {
            console.log("ERROR: ", e);
          }
        });
      });

      //$( "#collectionDate" ).datepicker();

    });

    </script>

<%@ include file="../common/header_bottom.jsp" %>

  <div class="row" id="table-div">
    <c:forEach items="${ collections }" var="collection">
    <div class="form-group">
      <input type="hidden" name="collectionId" value="${ collection.id }" />
      <span><c:out value="${ localeAccount.name }"/></span>
      <span><c:out value="${ collection.type.description }"/></span>
      <span><c:out value="${ collection.amount }"/></span>
      <span><c:out value="${ collection.collectionDate }"/></span>
      <button class="btn btn-default delete" type="button">delete</button>
    </div>
    </c:forEach>

    <spring:url value="/locale_account/${localeAccount.id}/submit_collection" var="actionUrl" />
    <form:form method="POST" modelAttribute="collection" action="${actionUrl}"
      class="navbar-form navbar-left">
    <div class="form-group">
      <form:select path="type" class="form-control">
        <form:option value="NONE" label="--- Select ---"/>
        <form:options items="${collTypes}"/>
      </form:select>
      <form:input path="amount" type="text" class="form-control"
              id="amount" placeholder="Amount" />
      <form:input path="collectionDate" type="date" class="form-control"
              id="collectionDate" placeholder="Collection Date" readonly="true"/>
      <button class="btn btn-default submit" type="button">add</button>
    </div>
    </form:form>
  </div>
  <div>
    <a id="add-collection" onclick="addRow()" href="javascript:void(0);">Add Collection</a>
  </div>
  <div>
    <spring:url value="/locale_account/${localeAccount.id}/collection/csv_upload" var="uploadUrl" />
    <form:form modelAttribute="fileuploadForm" action="${ uploadUrl }" method="POST" enctype="multipart/form-data">
      <label for="file">File</label>
      <form:input path="file" type="file" />
      <p><button type="submit">Upload</button></p>
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form:form>
  </div>

<%@ include file="../common/footer.jsp" %>