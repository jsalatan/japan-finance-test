<%@ include file="../../common/header_top.jsp" %>

<title>Collections for <c:out value="${ project.description }"/></title>

<%@ include file="../../common/header_bottom.jsp" %>
<div class="row">
    <div class="col-lg-12">
      <h3>Collections for <c:out value="${ project.description }"/></h3>
      <h2>( between 
        <fmt:formatDate type="date" value="${ startDate }" /> 
        and 
        <fmt:formatDate type="date" value="${ endDate }" /> )
      </h2>
    </div>
  </div>
  
        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Given By</th>
            <th>Amount</th>
            <th>Collection Date</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ localeAccount.projectCollections }" var="collection">
            <tr>
              <td><c:out value="${ collection.givenBy }"/></td>
              <td><fmt:formatNumber type="CURRENCY" value="${collection.amount}" /></td>
              <td><fmt:formatDate type="date" value="${collection.transactionDate}" /></td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
<%@ include file="../../common/footer.jsp" %>