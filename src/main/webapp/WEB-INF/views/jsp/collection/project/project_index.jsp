<%@ include file="../../common/header_top.jsp" %>

<script>
  $(document).ready(function() {

    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);
    
//     $( "#startDate" ).datepicker({ 
//         dateFormat: "mm/dd/yy"
//       });
//     $( "#startDate" ).datepicker("setDate", firstDay);
//     $( "#endDate" ).datepicker({ 
//         dateFormat: "mm/dd/yy"
//       });
//     $( "#endDate" ).datepicker("setDate", lastDay);
    
    $( "#startDate" ).val(firstDay);
    $( "#endDate" ).val(lastDay);
    
    $(".projectLink").click(function(event){
      var serializedForm = $("#dateFilterForm").serialize();
      console.log(serializedForm);
      
      //$(this).attr("href", this.href + "?" + form);

      event.preventDefault();
      window.location.href = $(this).attr('href') + "?" + serializedForm;
    });
  });
  
  function newHref(link) {
    var serializedForm = $("#dateFilterForm").serialize();
    var href = $(link).data("link");
    console.log(href);
    
    event.preventDefault();
    window.location.href = href + "?" + serializedForm;
  }
</script>

<title>Project Index</title>

<%@ include file="../../common/header_bottom.jsp" %>
  <div class="row">
    <div class="col-lg-12">
      <h3>Projects</h3>
    </div>
  </div>

<div>
    <spring:url value="/locale_account/${locale.id}/collection/project" var="submitUrl" />
    <form:form method="POST" modelAttribute="dateFilterForm" action="${submitUrl}" class="form-inline">
      <div class="form-group">
        <label class="sr-only" for="startDate">Start Date</label>
        <form:input path="startDate" type="date" class="form-control datepicker"
          id="startDate" placeholder="Start"/>
      </div>
      <div class="form-group">
        <label class="sr-only" for="endDate">End Date</label>
        <form:input path="endDate" type="date" class="form-control datepicker"
          id="endDate" placeholder="End"/>
      </div>
      <input type="button" value="Create" class="btn" />
    </form:form>
</div>
  <!-- Table -->
  <table class="table">
    <thead>
    <tr>
      <th>Description</th>
      <th>Total Collections</th>
    </tr>
    </thead>
    <tbody>
      <c:forEach items="${ projectsMap }" var="project">
      <tr>
        <td>
          <spring:url value="/locale_account/${localeAccount.id}/collection/project/${project.key}" var="url" />
          <a href="javascript:void(0);" data-link="${ url }" onclick="newHref(this);">
            <c:out value="${ project.key }"/>
          </a>
        </td>
        <td><fmt:formatNumber type="CURRENCY" value="${ project.value }" /></td>
      </tr>
      </c:forEach>
    </tbody>
  </table>
<%@ include file="../../common/footer.jsp" %>