<%@ include file="../../common/header_top.jsp" %>

<script>
  $(document).ready(function() {
    //$( "#transactionDate" ).datepicker();

    $('#formObj').on("change", ":input", function() {
      var allHasValues = true;
      $(this).parents('.row').find(':input').each(function() {
        if (!$(this).val()) {
          allHasValues = false;
        }
      })
      if( allHasValues ) {
        $("input[type=submit]").prop("disabled", false);
      } else {
        $("input[type=submit]").prop("disabled", true);
      }
    });

  });
</script>

<title>Add Project Collection</title>

<%@ include file="../../common/header_bottom.jsp" %>

<spring:url value="/locale_account/${locale.id}/collection/project/new" var="submitUrl" />
<form:form class="form-inline" method="POST" modelAttribute="formObj" action="${submitUrl}">
<div class="row">
  <div class="col-sm-3 form-group ui-widget" >
    <label class="sr-only" for="type">Project</label>
    <form:input path="project" type="search" class="form-control" placeholder="Project" style="width: 100%;"
      list="projects"/>
    <datalist id="projects">
     <c:forEach items="${ projects }" var="project">
     <option value="${ project.description }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-3 form-group ui-widget" >
    <label class="sr-only" for="type">Given By</label>
    <form:input path="givenBy" type="search" class="form-control" placeholder="Given By" style="width: 100%;"
      list="users"/>
    <datalist id="users">
     <c:forEach items="${ users }" var="user">
     <option value="${ user.username }"/>
     </c:forEach>
    </datalist>
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="amount">Amount</label>
    <form:input path="amount" type="text" class="form-control" placeholder="Amount" style="width: 100%;" />
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="transactionDate">Collection Date</label>
    <form:input path="transactionDate" type="date" class="form-control" placeholder="Collection Date" style="width: 100%;" />
  </div>
  <div class="col-sm-2">
    <input type="submit" value="Submit" class="btn btn-primary btn-block" disabled/>
  </div>
</div>
</form:form>
<%@ include file="../../common/footer.jsp" %>