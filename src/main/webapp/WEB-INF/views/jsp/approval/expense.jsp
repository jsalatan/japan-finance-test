<%@ include file="../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
    $('.table').on("click", ".approval-btn", function() {
      <c:url var="url" value="/approval/expense/"/>
      var baseUrl = "${ url }";
      var localeId = $(this).data('localeid');
      var expenseId = $(this).data('expenseid');
      
      var siblings = $(this).parent().siblings();
      var status = $(siblings[5]).text().trim();
      
      var btn = $(this);
      $.ajax({
        type : "POST",
        url : baseUrl + localeId + "/" + expenseId,
        data: $.param({ status:  status }),
        success : function(data) {

          
          var btnTxt = btn.text("Approved");
          $(siblings[5]).text();
          if (data == "success") {
            
            btn.prop("disabled", true);
          }
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    });

  });
</script>

<title>Expenses</title>

<%@ include file="../common/header_bottom.jsp" %>

        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Locale</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Source</th>
            <th>Payment Date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ localeAccounts }" var="localeAccount">
              <c:forEach items="${ localeAccount.expenses }" var="expense">
              <tr>
                <td><c:out value="${ localeAccount.name }"/></td>
                <td><c:out value="${ expense.expenseType.description }"/></td>
                <td>
                  <spring:url value="/locale_account/${localeAccount.id}/expense/${ expense.id }/edit" var="editUrl" />
                  <a href="${ editUrl }">
                    <c:out value="${ expense.amount }"/>
                  </a>
                </td>
                <td><c:out value="${ expense.collectionType.description }"/></td>
                <td>
                  <fmt:formatDate type="date" value="${expense.transactionDate}" />
                </td>
                <td>
                  <c:out value="${ expense.status }"/>
                </td>
                <td>
                  <c:choose>
                    <c:when test="${expense.status != 'APPROVED'}">
                      <button class="btn btn-primary approval-btn"
                        data-expenseId="${ expense.id }"
                        data-localeId="${ localeAccount.id }">Approve</button>
                    </c:when>
                    <c:otherwise>
                      <button type="button" class="btn btn-warning approval-btn" 
                        data-expenseid="${ expense.id }"
                        data-localeid="${ localeAccount.id }">Revoke</button>
                    </c:otherwise>
                  </c:choose>
                </td>
              </tr>
              </c:forEach>
            </c:forEach>
          </tbody>
        </table>
<%@ include file="../common/footer.jsp" %>