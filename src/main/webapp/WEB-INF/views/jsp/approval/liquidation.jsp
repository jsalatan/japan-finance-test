<%@ include file="../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
    $('.table').on("click", ".approval-btn", function() {
      <c:url var="url" value="/approval/liquidation/"/>
      var baseUrl = "${ url }";
      // Get td elements to get the locale name and month for approval
      var siblings = $(this).parent().siblings();
      var localeName = $(siblings[0]).text();
      var yearMonth = $(siblings[1]).text();
      
      var btn = $(this);
      $.ajax({
        type : "POST",
        url : baseUrl + localeName + "/" + yearMonth,
        success : function(data) {
          console.log(data);
          if (data == "success") {
            btn.text("Approved");
            btn.prop("disabled", true);
          } else {
            btn.text("Approved");
            btn.prop("disabled", false);
          }
        },
        error : function(e) {
          console.log("ERROR: ", e);
        }
      });
    });

  });
</script>

<title>Expenses</title>

<%@ include file="../common/header_bottom.jsp" %>

        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Locale</th>
            <th>Month</th>
            <th>Status</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ liquidationReports }" var="liquidationReport">
            <tr>
              <td><c:out value="${ liquidationReport[0] }"/></td>
              <td>
                <c:url var="url" value="/approval/expense/${ liquidationReport[0] }/${ liquidationReport[1] }"/>
                <a href="${ url }"><c:out value="${ liquidationReport[1] }"/></a>
              </td>
              <td>
                <c:if test="${liquidationReport[2] == 'PENDING'}">
                  <button class="btn btn-primary approval-btn">Approve</button>
                </c:if>
                <c:if test="${liquidationReport[2] != 'PENDING'}">
                  <c:out value="${ liquidationReport[2] }"/>
                </c:if>
              </td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
<%@ include file="../common/footer.jsp" %>