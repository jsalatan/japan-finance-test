<%@ include file="../common/header_top.jsp" %>

<title>Process Index</title>

<%@ include file="../common/header_bottom.jsp" %>

  <div class="row">
      <div class="col-lg-12">
          <h3>Process Index</h3>
      </div>
  </div>

  <div class="row" id="table-div">
    <div class="form-group">
      <spring:url value="/approval/expense" var="url" />
      <a href="${ url }">Expenses</a>
    </div>
        <div class="form-group">
      <spring:url value="/approval/liquidation" var="url" />
      <a href="${ url }">Liquidation</a>
    </div>
  </div>

<%@ include file="../common/footer.jsp" %>