<%@ include file="../common/header_top.jsp" %>

<title>Expenses</title>

<%@ include file="../common/header_bottom.jsp" %>

        <!-- Table -->
        <table class="table">
          <thead>
          <tr>
            <th>Locale</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Source</th>
            <th>Payment Date</th>
          </tr>
          </thead>
          <tbody>
            <c:forEach items="${ expenses }" var="expense">
            <tr>
              <td><c:out value="${ locale.name }"/></td>
              <td><c:out value="${ expense.expenseType.description }"/></td>
              <td>
                <spring:url value="/locale_account/${locale.id}/expense/${ expense.id }/edit" var="editUrl" />
                <a href="${ editUrl }">
                  <fmt:formatNumber type="CURRENCY" value="${expense.amount}" />
                </a>
              </td>
              <td><c:out value="${ expense.collectionType.description }"/></td>
              <td><fmt:formatDate type="date" value="${expense.transactionDate}" /></td>
            </tr>
            </c:forEach>
            <tr>
              <td colspan="2">
                <spring:url value="/locale_account/${locale.id}/expense/new" var="newUrl" />
                <a href="${ newUrl }">Add Expense</a>
              </td>
            </tr>
          </tbody>
        </table>
<%@ include file="../common/footer.jsp" %>