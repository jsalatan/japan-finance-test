<%@ include file="../common/header_top.jsp" %>
<script>
  $(document).ready(function() {
    //$( "#transactionDate" ).datepicker();

    var availableTags = [];
    <c:forEach items="${ expenseTypes }" var="payFor">
      availableTags.push("<c:out value="${payFor.description}" />");
    </c:forEach>
    $("input[name='payFor']").autocomplete({
      source: availableTags,
      minLength: 0
    }).focus(function(){            
      $(this).autocomplete("search");
    });
    
    var availableSrcs = [];
    <c:forEach items="${ fundSources }" var="fundSource">
      availableSrcs.push("<c:out value="${fundSource.label}" />");
    </c:forEach>
    $("#borrowFrom").autocomplete({
      source: availableSrcs,
      minLength: 0
    }).focus(function(){            
      $(this).autocomplete("search");
    });
    
  });
</script>

<title>Add expense</title>

<%@ include file="../common/header_bottom.jsp" %>

<spring:url value="/locale_account/${locale.id}/borrow/new" var="submitUrl" />
<form:form method="POST" modelAttribute="formObj" action="${submitUrl}">
<div class="row">
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="borrowFrom">Borrow From</label>
    <form:input path="borrowFrom" type="text" class="form-control" placeholder="Borrow From" style="width: 100%;"/>
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="transactionDate">Date Borrowed</label>
    <form:input path="transactionDate" type="date" class="form-control" placeholder="Date Borrowed" style="width: 100%;" />
  </div>
  <div class="col-sm-2 form-group" >
    <label class="sr-only" for="amount">Amount To Borrow</label>
    <form:input path="amount" type="text" class="form-control" placeholder="Amount To Borrow" style="width: 100%;" />
  </div>
  <div class="col-sm-3 form-group ui-widget" >
    <label class="sr-only" for="payFor">Pay For Expense</label>
    <form:input path="payFor" type="text" class="form-control" placeholder="Pay For Expense" style="width: 100%;"/>
  </div>
  <div class="col-sm-2 form-group ui-widget" >
    <label class="sr-only" for="requestor">Requested By</label>
    <form:input path="requestor" type="text" class="form-control" placeholder="Requested By" style="width: 100%;"/>
  </div>
  <div class="col-sm-1">
    <input type="submit" value="Submit" class="btn btn-primary btn-block" disabled/>
  </div>
</div>
</form:form>
<%@ include file="../common/footer.jsp" %>