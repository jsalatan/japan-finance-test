package org.mcgi.jp.web.expense;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.Role;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration("file:src/main/webapp/WEB-INF/web.xml")
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/mvc-config.xml",
        "file:src/main/webapp/WEB-INF/security-config.xml",
        "file:src/main/webapp/WEB-INF/mongo-config.xml",
        "file:src/main/webapp/WEB-INF/core-config.xml"
    })
public class ExpenseCollectionControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setup() {
       MockitoAnnotations.initMocks(this);
       mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    @WithMockUser(username="yamamoto.jinky", password="1234", roles="LOCALE_TREASURER")
    public void shouldDisplayNewExpensePage() throws Exception {
        mockMvc.perform(get("/locale_account/{localeId}/expense/new", "CHB"))
            .andExpect(status().isOk())
            .andExpect(view().name("expense/new"));
    }

    @Test
    public void shouldRedirectToExpenseIndexAfterSuccessfulSubmission() throws Exception {

        List<Role> role = new ArrayList<Role>();
        role.add(new Role("ROLE_LOCALE_TREASURER", "Locale Treasurer"));
        JapanFinanceUser user = new JapanFinanceUser("yamamoto.jinky", "Jinky", "Yamamoto", "CHB", "1234", role);
        CustomUserDetails userDetail = new CustomUserDetails(user, getAuthorities(user.getRoles()));

        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(userDetail, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);

        mockMvc.perform(post("/locale_account/{localeId}/expense/new", "CHB")
                .param("expenseType", "Locale Fund")
                .param("amount", "10000")
                .param("sourceOfFunds", "Locale Fund")
                .param("transactionDate", "2017/02/16")
                .param("dueDate", "2017/02/16"))

            .andExpect(status().is3xxRedirection())
            .andExpect(view().name("redirect:/locale_account/CHB/expense"));
    }

    public List<GrantedAuthority> getAuthorities(List<Role> roles) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        for (Role role : roles) {
            authList.add(new SimpleGrantedAuthority(role.getId()));
        }
        return authList;
    }

}
