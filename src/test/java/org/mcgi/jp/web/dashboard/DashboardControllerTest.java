package org.mcgi.jp.web.dashboard;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mcgi.jp.config.security.CustomUserDetails;
import org.mcgi.jp.model.admin.JapanFinanceUser;
import org.mcgi.jp.model.admin.Role;
import org.mcgi.jp.model.admin.UserPreference;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration("file:src/main/webapp/WEB-INF/web.xml")
@ContextConfiguration(locations = {
        "file:src/main/webapp/WEB-INF/mvc-config.xml",
        "file:src/main/webapp/WEB-INF/security-config.xml",
        "file:src/main/webapp/WEB-INF/mongo-config.xml",
        "file:src/main/webapp/WEB-INF/core-config.xml"
    })
public class DashboardControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setup() {
       MockitoAnnotations.initMocks(this);
       mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void whenAccessedAsAnonymousUser_thenItShouldRedirectToLogin() throws Exception {
        mockMvc.perform(get("/dashboard").with(anonymous()))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("http://localhost/login"));
    }

    @Test
    @WithMockUser(username="admin", password="admin", roles="ADMIN")
    public void whenAccessedAsAdmin_thenItShouldRedirectToAdmin() throws Exception {
        mockMvc.perform(get("/dashboard"))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/admin"));
    }

    @Test
    public void whenAccessedAsLocaleCoordinatorWithNoPreferredDashboard_thenShouldDisplayDefaultDashboard_ContributionDashboard() throws Exception {
        List<Role> role = new ArrayList<Role>();
        role.add(new Role("ROLE_LOCALE_COORDINATOR", "Locale Coordinator"));
        JapanFinanceUser user = new JapanFinanceUser("salatan.jonathan", "Jonathan", "Salatan", "CHB", "1234", role);
        CustomUserDetails userDetail = new CustomUserDetails(user, getAuthorities(user.getRoles()));

        mockMvc.perform(get("/dashboard").with(user(userDetail)))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("dashboard/CHB/contribution"));
    }

    public List<GrantedAuthority> getAuthorities(List<Role> roles) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        for (Role role : roles) {
            authList.add(new SimpleGrantedAuthority(role.getId()));
        }
        return authList;
    }

    @Test
    public void whenAccessedAsAuthenticatedUserWithoutLocale_thenShouldDisplayDashboardIndex() throws Exception {
        List<Role> role = new ArrayList<Role>();
        role.add(new Role("ROLE_MEMBER", "Member"));
        JapanFinanceUser user = new JapanFinanceUser("macam.malou", "Malou", "Macam", null, "1234", role);
        CustomUserDetails userDetail = new CustomUserDetails(user, getAuthorities(user.getRoles()));

        mockMvc.perform(get("/dashboard").with(user(userDetail)))
            .andExpect(status().isOk())
            .andExpect(view().name("dashboard/index"));
    }

    @Test
    public void whenUserIsALocaleCoordinator_AndDashboardPreferenceIsProjectMonitoring_ThenShouldReturnProjectMonitoringDashboard() throws Exception {
        List<Role> role = new ArrayList<Role>();
        role.add(new Role("ROLE_LOCALE_COORDINATOR", "Locale Coordinator"));
        UserPreference preference = new UserPreference();
        preference.setDashboard("project_monitoring");

        JapanFinanceUser user = new JapanFinanceUser("salatan.jonathan", "Jonathan", "Salatan", "CHB", "1234", role);
        user.setPreference(preference);

        CustomUserDetails userDetail = new CustomUserDetails(user, getAuthorities(user.getRoles()));

        mockMvc.perform(get("/dashboard").with(user(userDetail)))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("dashboard/CHB/project_monitoring"));
    }

}
