Reference:
https://dzone.com/articles/spring-data-mongodb-hello

1. Open a command prompt & goto bin folder found within MongoDB root folder.
  cmd
  cd D:\MongoDB\Server\3.2\bin
2. Before starting MongoDB server, create the data directory within root folder.
  D:\MongoDB\data
3. Start the MongoDB server with command such as "mongod -dbpath <path-to-mongodb-root-folder>"
  mongod -dbpath D:\MongoDB\data
4. Open another command prompt and goto bin folder.
  cmd
  cd D:\MongoDB\Server\3.2\bin
5. Execute "mongo" command and you are all set. Access various DB commands at
  http://docs.mongodb.org/manual/reference/command/


6. Modify spring-mvc-config.xml and add the "testdb" schema / databasename
  <!-- MongoTemplate for connecting and quering the documents in the database -->
  <bean id="mongoTemplate" class="org.springframework.data.mongodb.core.MongoTemplate">
    <constructor-arg name="mongo" ref="mongo" />
    <constructor-arg name="databaseName" value="test" />
  </bean>

  <mongo:repositories base-package="org.mcgi.jp.repository" />
  <mongo:repositories base-package="org.mcgi.jp.model" />

7. create collections
  db.createCollection( "expense_type" )
8. db.runCommand(
  { insert: "expense_types", documents:
    [
      { _id: 1, value: "1", description: "Locale Rent" }
      ,{ _id: 2, value: "2", description: "Electricity Bill" }
      ,{ _id: 3, value: "3", description: "Water Bill" }
      ,{ _id: 4, value: "4", description: "Gas Bill" }
      ,{ _id: 5, value: "5", description: "Internet Bill" }
    ]
  } )